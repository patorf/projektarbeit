from unittest import TestCase
import numpy as np
from scr.model.Trafo import Trafo

__author__ = 'philippatorf'


class TestTrafo(TestCase):
    def setUp(self):
        self.xStart = [[929.580, 422.800, -0.210],
                       [575.360, 480.900, 2.370],
                       [812.370, 200.820, -0.240],
                       [396.280, 283.240, 0.410]]
        self.XZiel = [[585.435, 755.475, 102.520],
                      [553.175, 988.105, 104.190],
                      [424.045, 785.635, 106.125],
                      [394.950, 1061.700, 106.070]]

    def test_approxTrafoParameter(self):
        t = Trafo(self.XZiel, self.xStart)

        sollParameter = np.array([0.022563, -0.0030886, 1.2702, 0.65433, 141.1723, 1.25448616e+03, 1.10837736e+02])

        np.testing.assert_almost_equal(sollParameter, t.trafoParam, 4)

    def test_calcFineParameter(self):
        t = Trafo(self.XZiel, self.xStart)
        t.calcAccuracy()

        sollGenauigkeit = np.array([2.49683983e-04, 3.99587059e-04, 1.95962557e-04, 1.95941348e-04,
                                    2.06545094e-01, 2.04385712e-01, 3.50786700e-01])
        np.testing.assert_almost_equal(sollGenauigkeit, t.s_u, 5)


    def test_transform(self):
        t = Trafo(self.XZiel, self.xStart)
        t.calcAccuracy()

        Start = [929.580, 422.800, -0.210]
        tZiel = t.transform([Start])[0]

        tStart = t.transformBack(tZiel)

        np.testing.assert_almost_equal(Start, tStart)