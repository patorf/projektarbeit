from unittest import TestCase
from scr.model.Messdaten import Messdaten

__author__ = 'Philipp'


#noinspection PyPep8Naming,PyShadowingBuiltins
class TestMessdaten(TestCase):
    def setUp(self):
        self.m = Messdaten(1, 1, 5.5, 1.5, 10, 1, None)

        #noinspection PyShadowingBuiltins
    def test_checkType(self):
        type = 0
        b = Messdaten()
        self.assertEqual(type, b.checkType(type))

        #self.fail()

    def test_Messdate(self):
        pkt = 1111
        type = 1
        hz = 10.3
        v = 12.4
        d = 100
        gemessen = 3

        m = Messdaten(pkt, type, hz, v, d, gemessen)
        self.assertEqual(m.TargetNo, pkt)
        self.assertEqual(m.Type, type)
        self.assertEqual(m.Hz, hz)
        self.assertEqual(m.V, v)
        self.assertEqual(m.Dist, d)
        self.assertEqual(m.isMeasured, gemessen)

    def test_convertToPunkt(self):
        pkt = 1111
        type = 1
        hz = 0.78539816
        v = 1.57079633
        d = 141.421356
        gemessen = 3

        m = Messdaten(pkt, type, hz, v, d, gemessen)

        p = m.convertToPunkt()

        self.assertAlmostEqual(p.y, 100, 5)
        self.assertAlmostEqual(p.x, 100, 5)
        self.assertAlmostEqual(p.z, 0, 5)

    def test_convertToSecondFace(self):
        self.m.convertToSecondFace()

        self.assertAlmostEqual(self.m.Hz, 2.35840734641021)
        self.assertAlmostEqual(self.m.V, 1.64159265358979)

