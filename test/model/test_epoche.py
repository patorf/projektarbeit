from unittest import TestCase
from scr.model.Deformationsanalyse import Deformationsanalyse
from scr.model.Epoche import Epoche
from scr.model.Messdaten import Messdaten
from scr.model.Station import Station
from scr.model.Vollsatz import Vollsatz

__author__ = 'Philipp'


#noinspection PyPep8Naming
class TestEpoche(TestCase):
    def test_addStation(self):
        e = Epoche()
        s = e.addNewStation()
        self.assertTrue(isinstance(s, Station))
        #test
        st = Station()
        tst = e.addNewStation(st)
        self.assertTrue(tst == st)


    def test_getBeobachtungList(self):
        #1.Halbsatz
        b1 = Messdaten(1, 1, 1.1740, 97.2776, 2.2961, True)
        b2 = Messdaten(2, 1, 21.3764, 97.2778, 2.5658, True)
        b3 = Messdaten(3, 1, 42.72, 97.2772, 2.5735, True)

        #2.Halbsatz
        bb1 = Messdaten(1, 1, 201.1744, 302.7102, 2.2961, True)
        bb2 = Messdaten(2, 1, 221.3760, 302.7108, 2.5659, True)
        bb3 = Messdaten(3, 1, 242.7208, 302.7116, 2.5734, True)

        c1 = Messdaten(1, 1, 1.1740, 97.2776, 2.2959, True)
        c2 = Messdaten(2, 1, 21.3764, 97.2778, 2.5659, True)
        c3 = Messdaten(3, 1, 42.72, 97.2772, 2.5734, True)

        #2.Halbsatz
        cc1 = Messdaten(1, 1, 201.1744, 302.7102, 2.2961, True)
        cc2 = Messdaten(2, 1, 221.3760, 302.7108, 2.5659, True)
        cc3 = Messdaten(3, 1, 242.7208, 302.7116, 2.5736, True)

        d1 = Messdaten(1, 1, 1.1740, 97.2776, 2.2960, True)
        d2 = Messdaten(2, 1, 21.3764, 97.2778, 2.5660, True)
        d3 = Messdaten(3, 1, 42.72, 97.2772, 2.5736, True)

        #2.Halbsatz
        dd1 = Messdaten(1, 1, 201.1744, 302.7102, 2.2961, True)
        dd2 = Messdaten(2, 1, 221.3760, 302.7108, 2.5659, True)
        dd3 = Messdaten(3, 1, 242.7208, 302.7116, 2.5733, True)

        v1 = Vollsatz([b1, b3, b2], [bb1, bb2, bb3])
        v2 = Vollsatz([c1, c2, c3], [cc1, cc2, cc3])
        v3 = Vollsatz([d1, d2, d3], [dd1, dd2, dd3])

        e = Epoche()
        s = e.addNewStation()

        s.addNewVollsatz(v1)
        s.addNewVollsatz(v2)
        s.addNewVollsatz(v3)

        beobachtungen = e.getBeobachtungList()

        self.assertTrue(len(beobachtungen) == 9)

    def test_getKoordinaten(self):
        #Standpunkt 1
        b1 = Messdaten(1, 1, 4.729336302, 1.570796327, 2.95042, True)
        b2 = Messdaten(2, 1, 4.732793625, 1.570796327, 2.45051, True)
        b3 = Messdaten(3, 1, 5.084097511, 1.570796327, 3.16623, True)
        b4 = Messdaten(4, 2, 5.151249054, 1.570796327, 2.70647, True)

        b11 = Messdaten(1, 1, 4.7293363, 4.712388980, 2.9504, True)
        b22 = Messdaten(2, 1, 4.7327936, 4.712388980, 2.4505, True)
        b33 = Messdaten(3, 1, 5.0840975, 4.712388980, 3.1662, True)
        b44 = Messdaten(4, 2, 5.1512490, 4.712388980, 2.7064, True)

        s1V = Vollsatz([b1, b2, b3, b4], [b11, b22, b33, b44])

        c1 = Messdaten(1, 1, 4.34068045, 1.570796327, 3.16623, True)
        c2 = Messdaten(2, 1, 4.273528907, 1.570796327, 2.70647, True)
        c3 = Messdaten(3, 1, 4.695441659, 1.570796327, 2.95042, True)
        c4 = Messdaten(4, 1, 4.691984336, 1.570796327, 2.45051, True)

        c11 = Messdaten(1, 1, 4.340680, 4.712388980, 3.1662, True)
        c22 = Messdaten(2, 1, 4.2735289, 4.712388980, 2.7064, True)
        c33 = Messdaten(3, 1, 4.6954416, 4.7123889807, 2.9504, True)
        c44 = Messdaten(4, 1, 4.6919843, 4.7123889807, 2.4505, True)

        s2V = Vollsatz([c1, c2, c3, c4], [c11, c22, c33, c44])

        e = Epoche()
        sss = Station()
        ss2 = Station()

        s1 = e.addNewStation()
        s2 = e.addNewStation()

        s1.addNewVollsatz(s1V)
        s2.addNewVollsatz(s2V)

        koors = e.getKoordinaten()

        self.assertTrue(len(koors) == 6)
        self.assertTrue(koors[-2].x == 0)
        self.assertTrue(koors[-2].y == 0)
        self.assertTrue(koors[-2].z == 0)

    def test_realData(self):
        #Epoche 1
        m1 = Messdaten('1', '0', 5.1587476335, 1.68255986505, 2.8957, '7', True)
        m2 = Messdaten('2', '0', 5.38751838246, 1.76400400325, 2.9289, '7', True)
        m3 = Messdaten('3', '0', 5.43686907248, 1.68293217723, 2.8642, '7', True)

        mm1 = Messdaten('1', '0', 2.01760434499, 4.60080901805, 2.8955, '7', True)
        mm2 = Messdaten('2', '0', 2.2463636788, 4.51947437384, 2.9284, '7', True)
        mm3 = Messdaten('3', '0', 2.29570599209, 4.60039853273, 2.8642, '7', True)

        #Staton 0 eigentlich 2
        #Satz 1


        m2_1 = Messdaten('1', '0', 0.235180185412, 1.65241748138, 3.9917, '7', True)
        m2_2 = Messdaten('2', '0', 0.372688873969, 1.72771118105, 3.6194, '7', True)
        m2_3 = Messdaten('3', '0', 0.397739763517, 1.66381361393, 3.4776, '7', True)

        mm2_1 = Messdaten('1', '0', 3.37674065164, 4.63079854752, 3.9921, '7', True)
        mm2_2 = Messdaten('2', '0', 3.51429478978, 4.55552995593, 3.6198, '7', True)
        mm2_3 = Messdaten('3', '0', 3.53930586878, 4.6194024662, 3.4773, '7', True)


        #Epoche2
        versatz = 0.1
        zneu = 1.68293217723 - (  versatz / 2.8642)
        z2neu = 4.60039853273 + (versatz / 2.8642)

        e2_m1 = Messdaten('1', '0', 5.1587476335, 1.68255986505, 2.8957, '7', True)
        e2_m2 = Messdaten('2', '0', 5.38751838246, 1.76400400325, 2.9289, '7', True)
        e2_m3 = Messdaten('3', '0', 5.43686907248, zneu, 2.8642, '7', True)

        e2_mm1 = Messdaten('1', '0', 2.01760434499, 4.60080901805, 2.8955, '7', True)
        e2_mm2 = Messdaten('2', '0', 2.2463636788, 4.51947437384, 2.9284, '7', True)
        e2_mm3 = Messdaten('3', '0', 2.29570599209, z2neu, 2.8642, '7', True)

        #Staton 0 eigentlich 2
        #Satz 1

        s2Zneu = 4.6194024662 + (versatz / 3.4773)
        s2Z2neu = 1.66381361393 + (versatz / 3.4776)

        e2_m2_1 = Messdaten('1', '0', 0.235180185412, 1.65241748138, 3.9917, '7', True)
        e2_m2_2 = Messdaten('2', '0', 0.372688873969, 1.72771118105, 3.6194, '7', True)
        e2_m2_3 = Messdaten('3', '0', 0.397739763517, s2Z2neu, 3.4776, '7', True)

        e2_mm2_1 = Messdaten('1', '0', 3.37674065164, 4.63079854752, 3.9921, '7', True)
        e2_mm2_2 = Messdaten('2', '0', 3.51429478978, 4.55552995593, 3.6198, '7', True)
        e2_mm2_3 = Messdaten('3', '0', 3.53930586878, s2Zneu, 3.4773, '7', True)

        e = Epoche()
        s1 = e.addNewStation()

        v = s1.addNewVollsatz()
        v.Halbsaetze[1] = [m1, m2, m3]
        v.Halbsaetze[2] = [mm1, mm2, mm3]

        s2 = e.addNewStation()
        v2 = s2.addNewVollsatz()
        v2.Halbsaetze[1] = [m2_1, m2_2, m2_3]
        v2.Halbsaetze[2] = [mm2_1, mm2_2, mm2_3]

        e.startAusgleichung()
        e.netzausgleichung.printProtokol('jetzAber')
        e.netzausgleichung.getKonfidenzEllipsParam('1')

        s1.stationsMittelProtokoll()
        s2.stationsMittelProtokoll()

        e2 = Epoche()
        e2s1 = e2.addNewStation()

        e2v = e2s1.addNewVollsatz()
        e2v.Halbsaetze[1] = [e2_m1, e2_m2, e2_m3]
        e2v.Halbsaetze[2] = [e2_mm1, e2_mm2, e2_mm3]

        e2s2 = e2.addNewStation()
        e2v2 = e2s2.addNewVollsatz()
        e2v2.Halbsaetze[1] = [e2_m2_1, e2_m2_2, e2_m2_3]
        e2v2.Halbsaetze[2] = [e2_mm2_1, e2_mm2_2, e2_mm2_3]

        e2s1.stationsMittelProtokoll()
        e2s2.stationsMittelProtokoll()

        e2.startAusgleichung()  #e.netzausgleichung.koordinaten)

        defo = Deformationsanalyse(e, e2)

        defo.checkGrundgesamtheit()
        defo.defoAnalyse()
        defo.printProtokol('Matze123')

        self.fail()





