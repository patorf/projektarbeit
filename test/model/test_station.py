# coding=utf-8
import logging
from unittest import TestCase
from scr.model.Epoche import Epoche

from scr.model.Messdaten import Messdaten
from scr.model.Station import Station
from scr.model.Vollsatz import Vollsatz
import scr.controller.myLogger as mylogger


__author__ = 'Philipp'
mylogger.startLogger()
logger = logging.getLogger(__name__)


class TestStation(TestCase):
    def setUp(self):
        self.station = Station()

    def test_addVollsatz(self):
        self.assertTrue(True)
        # self.fail()

    def test_initStation(self):
        s1 = Station()

        s2 = Station()
        self.assertTrue(self.station.No == 1)
        self.assertTrue(s2.No == 3)
        self.assertTrue(s1.No == 2)

    def test_SatzauswertungHz(self):
        #TestDaten
        #1.Halbsatz
        b1 = Messdaten(1, 1, 0.018441149, 1.528032968, 100, True)
        b2 = Messdaten(2, 1, 0.335779706, 1.528036109, 101, True)
        b3 = Messdaten(3, 1, 0.671044191, 1.528026684, 100, True)
        b4 = Messdaten(4, 1, 1.247727505, 1.528067525, 100, True)

        #2.Halbsatz
        bb1 = Messdaten(1, 1, 3.160040086, 4.754960702, 0, True)
        bb2 = Messdaten(2, 1, 3.477366076, 4.754970127, 0, True)
        bb3 = Messdaten(3, 1, 3.812649411, 4.754982694, 0, True)
        bb4 = Messdaten(4, 1, 4.389326441, 4.756071255, 0, True)

        c1 = Messdaten(1, 1, 1.627366986, 1.570796327, 0, True)
        c2 = Messdaten(2, 1, 1.944696118, 1.570796327, 0, True)
        c3 = Messdaten(3, 1, 2.279992019, 1.570796327, 0, True)
        c4 = Messdaten(4, 1, 2.856672191, 1.570796327, 0, True)

        #2.Halbsatz
        cc1 = Messdaten(1, 1, 4.768969064, 1.570796327, 0, True)
        cc2 = Messdaten(2, 1, 5.08628563, 1.570796327, 0, True)
        cc3 = Messdaten(3, 1, 5.421594097, 1.570796327, 0, True)
        cc4 = Messdaten(4, 1, 5.998267986, 1.570796327, 0, True)

        v1 = Vollsatz([b1, b3, b2, b4], [bb1, bb2, bb4, bb3])
        v2 = Vollsatz([c1, c2, c3, c4], [cc1, cc2, cc3, cc4])
        s = Station()

        s.addNewVollsatz(v1)
        s.addNewVollsatz(v2)

        #s.addVollsatz(z_v)
        #s.addVollsatz(z_v2)
        #s.addVollsatz(z_v3)

        s.Satzauswertung()

        self.assertAlmostEqual(s.s_Hz_mittel, 6.6643243961E-06, 9)

    def test_SatzauswertungVz(self):
    #TODO auf Rad umstellen
    #TestDaten
        #1.Halbsatz
        b1 = Messdaten(1, 1, 0.018441149, 1.528032968, 1, True)
        b2 = Messdaten(1, 1, 0.335779706, 1.528036109, 1, True)
        b3 = Messdaten(1, 1, 0.671044191, 1.528026684, 1, True)
        b4 = Messdaten(4, 1, 1.247727505, 1.528067525, 1, True)

        #2.Halbsatz
        bb1 = Messdaten(1, 1, 3.160040086, 4.754960702, 1, True)
        bb2 = Messdaten(1, 1, 3.477366076, 4.754970127, 1, True)
        bb3 = Messdaten(1, 1, 3.812649411, 4.754982694, 1, True)
        bb4 = Messdaten(4, 1, 4.389326441, 4.756071255, 1, True)

        z_v = Vollsatz([b1], [bb1])
        z_v2 = Vollsatz([b2], [bb2])
        z_v3 = Vollsatz([b3], [bb3])

        #v1 = Vollsatz([b1, b3, b2, b4], [bb1, bb2, bb4, bb3])
        #v2 = Vollsatz([c1, c2, c3, c4], [cc1, cc2, cc3, cc4])
        s = Station()

        #s.addVollsatz(v1)
        #s.addVollsatz(v2)

        s.addNewVollsatz(z_v)
        s.addNewVollsatz(z_v2)
        s.addNewVollsatz(z_v3)

        s.Satzauswertung()
        anzahlBeo = s.Saetze[0].getAnzBeobachtung()

        print s.stationsMittel[0].V
        if len(s.Saetze) == 1 or anzahlBeo == 1:
            self.assertAlmostEqual(s.s_V_mittel, 0.001, 5)
        else:
            self.assertAlmostEqual(s.s_V_mittel, 0.000272, 5)


    def test_SatzauswertungS(self):
    #TestDaten
    #1 Halbsatz
    #1.Halbsatz
        b1 = Messdaten('1', 1, 1.1740, 97.2776, 2.2961, True)
        b2 = Messdaten('2', 1, 21.3764, 97.2778, 2.5658, True)
        b3 = Messdaten('3', 1, 42.72, 97.2772, 2.5735, True)

        #2.Halbsatz
        bb1 = Messdaten('1', 1, 201.1744, 302.7102, 2.2961, True)
        bb2 = Messdaten('2', 1, 221.3760, 302.7108, 2.5659, True)
        bb3 = Messdaten('3', 1, 242.7208, 302.7116, 2.5734, True)

        c1 = Messdaten('1', 1, 1.1740, 97.2776, 2.2959, True)
        c2 = Messdaten('2', 1, 21.3764, 97.2778, 2.5659, True)
        c3 = Messdaten('3', 1, 42.72, 97.2772, 2.5734, True)

        #2.Halbsatz
        cc1 = Messdaten('1', 1, 201.1744, 302.7102, 2.2961, True)
        cc2 = Messdaten('2', 1, 221.3760, 302.7108, 2.5659, True)
        cc3 = Messdaten('3', 1, 242.7208, 302.7116, 2.5736, True)

        d1 = Messdaten('1', 1, 1.1740, 97.2776, 2.2960, True)
        d2 = Messdaten('2', 1, 21.3764, 97.2778, 2.5660, True)
        d3 = Messdaten('3', 1, 42.72, 97.2772, 2.5736, True)

        #2.Halbsatz
        dd1 = Messdaten('1', 1, 201.1744, 302.7102, 2.2961, True)
        dd2 = Messdaten('2', 1, 221.3760, 302.7108, 2.5659, True)
        dd3 = Messdaten('3', 1, 242.7208, 302.7116, 2.5733, True)

        v1 = Vollsatz([b1, b3, b2], [bb1, bb2, bb3])
        v2 = Vollsatz([c1, c2, c3], [cc1, cc2, cc3])
        v3 = Vollsatz([d1, d2, d3], [dd1, dd2, dd3])

        s = Station()

        s.addNewVollsatz(v1)
        s.addNewVollsatz(v2)
        s.addNewVollsatz(v3)

        #s.addVollsatz(z_v)
        #s.addVollsatz(z_v2)
        #s.addVollsatz(z_v3)

        s.Satzauswertung()

        s.stationsMittelProtokoll()

        self.assertAlmostEqual(s.s_Dist_mittel, 0.00003118, 8)


    def test_completeFirstSet(self):
        self.station.getFirstVollsatz()
        self.station.completeFirstSet()

        self.fail()

    def test_getCountOfMeasurements(self):
        b1 = Messdaten('1', 1, 1.1740, 97.2776, 2.2961, True)
        b2 = Messdaten('2', 1, 21.3764, 97.2778, 2.5658, True)
        b3 = Messdaten('3', 1, 42.72, 97.2772, 2.5735, True)
        v1 = Vollsatz([b1, b2, b3])
        st1 = Station()
        st1.addNewVollsatz(v1)
        count = st1.getCoutOfMeasurements()
        self.assertTrue(count['1']['count'] == 1)
        self.assertTrue(count['1']['vollsatz'] == False)

        v1.addMessdaten(b1, 2)
        v1.addMessdaten(b2, 2)

        count = st1.getCoutOfMeasurements()
        self.assertTrue(count['1']['count'] == 2)
        self.assertTrue(count['1']['vollsatz'] == True)
        self.assertTrue(count['3']['count'] == 1)
        self.assertTrue(count['3']['vollsatz'] == False)


# s = Station()
# t = s.getTachy()
# t.initSerial('/dev/cu.360584TS30-SPP')
#
# s2 = Station()
# t2 = s.getTachy()
# t2.initSerial('/dev/cu.360524TS30-SPP')
#
# thread.start_new_thread(s.defineTarget)
# thread.start_new_thread(s2.defineTarget)
#
# while True:
