from unittest import TestCase
from scr.model.Deformationsanalyse import Deformationsanalyse
from scr.model.Epoche import Epoche
import numpy as np
from scr.model.Beobachtung import Beobachtung
from scr.model.Netzausgleichung import Netzausgleichung
from scr.model.Punkt import Punkt
from scr.model.Deformationsanalyse import Deformationsanalyse

__author__ = 'Marcus'


class TestDeformationsanalyse(TestCase):
    def test_kompleteAusgleichung_defo(self):
        b1 = Beobachtung(1, 10, 'Hz', 0.0, 0.00016)
        b2 = Beobachtung(1, 10, 'V', 1.15026, 0.00016)
        b3 = Beobachtung(1, 10, 'Dist', 24.47489, 0.01)

        bbb1 = Beobachtung(1, 2, 'Hz', 1.109, 0.00016)
        bbb2 = Beobachtung(1, 2, 'V', 1.57079, 0.00016)
        bbb3 = Beobachtung(1, 2, 'Dist', 40, 0.01)

        bb1 = Beobachtung(2, 10, 'Hz', 0.0, 0.00031)
        bb2 = Beobachtung(2, 10, 'V', 1.300246, 0.00031)
        bb3 = Beobachtung(2, 10, 'Dist', 37.4165738, 0.02)

        bbbb1 = Beobachtung(2, 1, 'Hz', 5.69510, 0.00031)
        bbbb2 = Beobachtung(2, 1, 'V', 1.57079, 0.00031)
        bbbb3 = Beobachtung(2, 1, 'Dist', 40, 0.02)

        p1 = Punkt(1, 10, 10, 10)
        p2 = Punkt(2, 50, 10, 10)
        p10 = Punkt(10, 20, 30, 20)
        Epoche1 = Epoche()

        station1 = Epoche1.addNewStation()
        station1.stationsMittel = [b1, b2, b3, bb1, bb2, bb3, bbb1, bbb2, bbb3, bbbb1, bbbb2, bbbb3]

        n1 = Netzausgleichung([p1, p2, p10], [b1, b2, b3, bb1, bb2, bb3, bbb1, bbb2, bbb3, bbbb1, bbbb2, bbbb3],
                              s0priori=1)

        Epoche1.netzausgleichung = n1

        n1.ausgleichung()
        n1.Fehlerrechnung()
        n1.normVerb()
        n1.Globaltest()
        n1.Redundanzanteil()
        n1.normVerb()

        b1 = Beobachtung(1, 10, 'Hz', 0.0, 0.00016)
        b2 = Beobachtung(1, 10, 'V', 1.15026, 0.00016)
        b3 = Beobachtung(1, 10, 'Dist', 24.49489, 0.01)

        bbb1 = Beobachtung(1, 2, 'Hz', 1.109, 0.00016)
        bbb2 = Beobachtung(1, 2, 'V', 1.57079, 0.00016)
        bbb3 = Beobachtung(1, 2, 'Dist', 40, 0.01)

        bb1 = Beobachtung(2, 10, 'Hz', 0.0, 0.00031)
        bb2 = Beobachtung(2, 10, 'V', 1.300246, 0.00031)
        bb3 = Beobachtung(2, 10, 'Dist', 37.4165738, 0.02)

        bbbb1 = Beobachtung(2, 1, 'Hz', 5.69510, 0.00031)
        bbbb2 = Beobachtung(2, 1, 'V', 1.57079, 0.00031)
        bbbb3 = Beobachtung(2, 1, 'Dist', 40, 0.02)

        p1 = Punkt(1, 10, 10, 10)
        p2 = Punkt(2, 50, 10, 10)
        p10 = Punkt(10, 20, 30, 20)

        n2 = Netzausgleichung([p1, p2, p10], [b1, b2, b3, bb1, bb2, bb3, bbb1, bbb2, bbb3, bbbb1, bbbb2, bbbb3],
                              s0priori=1)
        n2.ausgleichung()
        n2.Fehlerrechnung()
        n2.normVerb()
        n2.Globaltest()
        n2.Redundanzanteil()
        n2.normVerb()

        e1 = Epoche()

        e1.netzausgleichung = n1

        e2 = Epoche()
        e2.netzausgleichung = n2

        defo = Deformationsanalyse(e1, e2)

        defo.checkGrundgesamtheit()
        defo.defoAnalyse()
        defo.printProtokol('Matze123')





        # n.getKonfidenzEllipsParam(10)
        self.fail()





