# coding=utf-8
from multiprocessing import Process
from scr.model.Epoche import Epoche
from scr.model.Station import Station

__author__ = 'philippatorf'


def test_paralleCompletFirstSet():
    e = Epoche()

    s = Station()
    t = s.getTachy()
    #print t.availablePorts
    t.initSerial('/dev/cu.360524TS30-SPP')
    nextAction = 'n'
    #t.changeFace();
    #t.changeFace();
    while nextAction != 'c':
        print "Bitte Punktnummer"

        targetNo = raw_input(1)
        print "Bitte Prisma Typ angeben 0=Laica Rundprisma; 3=360 ; 7=360 mini"
        prismType = raw_input(1)
        print "Referenzpunkt= 0 oder Objektpunkt =1 "
        pointType = raw_input(1)
        print "Bitte Ziel anzielen und mit mit Enter bestätigen"
        raw_input()
        succes = s.defineTarget(targetNo, prismType, pointType)
        if succes:
            print 'Erfolgreich'
            print 'Satz beenden (c) oder weitere zielen Messen (n)'
            nextAction = raw_input(1)

    s2 = Station()
    t2 = s2.getTachy()
    #print t2.availablePorts
    t2.initSerial('/dev/cu.360584TS30-SPP')
    nextAction = 'n'

    while nextAction != 'c':
        print "Bitte Punktnummer"

        targetNo = raw_input(2)
        print "Bitte Prisma Typ angeben 0=Laica Rundprisma; 3=360 ; 7=360 mini"
        prismType = raw_input(2)
        print "Referenzpunkt= 0 oder Objektpunkt =1 "
        pointType = raw_input(2)
        print "Bitte Ziel anzielen und mit mit Enter bestätigen"
        raw_input()
        succes = s2.defineTarget(targetNo, prismType, pointType)
        if succes:
            print 'Erfolgreich'
            print 'Satz beenden (c) oder weitere zielen Messen (n)'
            nextAction = raw_input(2)

    p1 = Process(target=s.completeFirstSet)
    p1.start()

    p2 = Process(target=s2.completeFirstSet)
    p2.start()

    p1.join()
    p2.join()

    nextAction = 'n'
    while nextAction != 'c':
        p3 = Process(target=s.measureAdditionalSet)
        p3.start()

        p4 = Process(target=s2.measureAdditionalSet)
        p4.start()

        p3.join()
        p4.join()
        nextAction = raw_input("noch einen satz ? J = ja c=nein")

    print s.Saetze[0].getAnzBeobachtung()
    print s2.Saetze[0].getAnzBeobachtung()


def test_Epoche():
    e = Epoche()

    s = e.addNewStation()
    s2 = e.addNewStation()
    t = s.getTachy()
    t2 = s2.getTachy()
    print t.availablePorts
    t.initSerial('/dev/cu.360524TS30-SPP')
    nextAction = 'n'
    t.changeFace();
    while nextAction != 'c':
        print "Bitte Punktnummer"

        targetNo = raw_input(1)
        print "Bitte Prisma Typ angeben 0=Laica Rundprisma; 3=360 ; 7=360 mini"
        prismType = raw_input(1)
        print "Referenzpunkt= 0 oder Objektpunkt =1 "
        pointType = raw_input(1)
        print "Bitte Ziel anzielen und mit mit Enter bestätigen"
        raw_input()
        succes = s.defineTarget(targetNo, prismType, pointType)
        if succes:
            print 'Erfolgreich'
            print 'Satz beenden (c) oder weitere zielen Messen (n)'
            nextAction = raw_input(1)

    #print t2.availablePorts
    t2.initSerial('/dev/cu.360584TS30-SPP')
    nextAction = 'n'

    while nextAction != 'c':
        print "Bitte Punktnummer"

        targetNo = raw_input(2)
        print "Bitte Prisma Typ angeben 0=Laica Rundprisma; 3=360 ; 7=360 mini"
        prismType = raw_input(2)
        print "Referenzpunkt= 0 oder Objektpunkt =1 "
        pointType = raw_input(2)
        print "Bitte Ziel anzielen und mit mit Enter bestätigen"
        raw_input()
        succes = s2.defineTarget(targetNo, prismType, pointType)
        if succes:
            print 'Erfolgreich'
            print 'Satz beenden (c) oder weitere zielen Messen (n)'
            nextAction = raw_input(2)

    # p1 = Process(target=s.completeFirstSet)
    # p1.start()
    #
    # p2 = Process(target=s2.completeFirstSet)
    # p2.start()
    #
    # p1.join()
    # p2.join()
    #
    # p3 = Process(target=s.measureAdditionalSet)
    # p3.start()
    #
    # p4 = Process(target=s2.measureAdditionalSet)
    # p4.start()
    #
    # p3.join()
    # p4.join()
    s.completeFirstSet()
    s2.completeFirstSet()
    s.measureAdditionalSet()
    s2.measureAdditionalSet()

    e.startAusgleichung()
    s.stationsMittelProtokoll()
    s2.stationsMittelProtokoll()
    e.netzausgleichung.ausgleichung()
    #   e.netzausgleichung.Fehlerrechnung(7)
    e.netzausgleichung.normVerb()
    e.netzausgleichung.printProtokol("czommerTest")


def test_oballsKlappt():
    e = Epoche()
    s = e.addNewStation()
    #s = Station()
    t = s.getTachy()
    #print t.availablePorts
    t.initSerial('/dev/cu.360524TS30-SPP')
    nextAction = 'n'
    #t.changeFace();
    #t.changeFace();
    while nextAction != 'c':
        print "Bitte Punktnummer"

        targetNo = raw_input(1)
        print "Bitte Prisma Typ angeben 0=Laica Rundprisma; 3=360 ; 7=360 mini"
        prismType = raw_input(1)
        print "Referenzpunkt= 0 oder Objektpunkt =1 "
        pointType = raw_input(1)
        print "Bitte Ziel anzielen und mit mit Enter bestätigen"
        raw_input()
        succes = s.defineTarget(targetNo, prismType, pointType)
        if succes:
            print 'Erfolgreich'
            print 'Satz beenden (c) oder weitere zielen Messen (n)'
            nextAction = raw_input(1)

    # s2 = Station()
    # t2 = s.getTachy()
    # print t2.availablePorts
    # t2.initSerial('/dev/cu.360584TS30-SPP')
    # nextAction = 'n'
    #
    # while nextAction != 'c':
    #     print "Bitte Punktnummer"
    #
    #     targetNo = raw_input(2)
    #     print "Bitte Prisma Typ angeben 0=Laica Rundprisma; 3=360 ; 7=360 mini"
    #     prismType = raw_input(2)
    #     print "Referenzpunkt= 0 oder Objektpunkt =1 "
    #     pointType = raw_input(2)
    #     print "Bitte Ziel anzielen und mit mit Enter bestätigen"
    #     raw_input()
    #     succes = s2.defineTarget(targetNo, prismType, pointType)
    #     if succes:
    #         print 'Erfolgreich'
    #         print 'Satz beenden (c) oder weitere zielen Messen (n)'
    #         nextAction = raw_input(2)

    s.completeFirstSet()
    s.measureAdditionalSet()
    e.getBeobachtungList()
    s.stationsMittelProtokoll()
    e.startAusgleichung()


#test_oballsKlappt()
test_Epoche()
#test_paralleCompletFirstSet()