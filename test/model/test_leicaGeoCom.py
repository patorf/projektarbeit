from unittest import TestCase
from scr.model.LeicaGeoCom import LeicaGeoCom as LGC

__author__ = 'Philipp'


#noinspection PyPep8Naming
class TestLeicaGeoCom(TestCase):
    #noinspection PyPep8Naming
    def test_converResponse(self):
        respons = LGC.convertResponse('%R1P,0,0:1285,3.2,1.6,0\r\n', ["RC", "Hz", "V", "SlopeDistance"])
        self.assertTrue(respons['RC'] == 1285)
        self.assertTrue(respons['Hz'] == 3.2)
        self.assertTrue(respons['V'] == 1.6)
        self.assertTrue(respons['SlopeDistance'] == 0)

        responeWithNoParamter = LGC.convertResponse('%R1P,0,0:0\r\n', ["RC"])
        self.assertTrue(responeWithNoParamter["RC"] == 0)



