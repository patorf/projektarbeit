from unittest import TestCase
import logging

import scr.controller.myLogger as mylogger
from scr.model.LeicaGeoCom import LeicaGeoCom
from scr.model.Messdaten import Messdaten
from scr.model.Tachy import Tachy


__author__ = 'Philipp'
mylogger.startLogger()

logger = logging.getLogger(__name__)
#noinspection PyPep8Naming
class TestTachy(TestCase):
    def setUp(self):
        self.port = '/dev/cu.360584TS30-SPP'
        self.tachy = Tachy()

        logger.debug("SetUp Tachy")


    def tearDown(self):
        self.tachy.usedSerial.close()

    def test_sendTachy(self):
        self.tachy.initSerial(self.port)
        ret = self.tachy.sendTachy(LeicaGeoCom.AUS_GetUserAtrState())
        self.assertTrue(isinstance(ret, dict))


    def test_initSerial(self):
        logger.info('test_initSerial')
        self.tachy.initSerial(self.port)

        self.assertTrue(self.tachy.ready or not self.tachy.ready)

    def test_setUpTachy(self):
        self.tachy.initSerial(self.port)
        self.tachy.setUpTachy()

        self.assertTrue(True)

    def test_closeSerial(self):
        self.tachy.initSerial(self.port)
        self.tachy.closeSerial()
        self.assertTrue(not self.tachy.usedSerial.isOpen())

    def test_findTarget(self):
        logger.debug('test_findTarget')
        self.tachy.initSerial(self.port)

        m = Messdaten(2, 1, 2.68248, 1.595, 0, False)
        success = self.tachy.findTargetAt(m)
        #print success
        self.assertTrue(success)
        #self.assertFalse(success)


    def test_distMeasurement(self):
        logger.debug('test_findTarget')
        self.tachy.initSerial(self.port)

        success = self.tachy.distMeasurement();
        logger.debug(success)
        self.assertIsInstance(success, bool)

    def test_getMeasurements(self):

        logger.debug('test_getMeasurements')
        self.tachy.initSerial(self.port)

        succes, messdaten = self.tachy.getMeasurements()

        self.assertIsInstance(succes, bool)
        self.assertIsInstance(messdaten, Messdaten)
        if succes:
            self.assertTrue(messdaten.Hz != None)
            self.assertTrue(messdaten.V != None)
            self.assertTrue(messdaten.Dist != None)
            self.assertTrue(messdaten.isMeasured)
        else:
            self.assertFalse(messdaten.isMeasured)


    def test_measureAt_Hz_V(self):
        self.tachy.initSerial(self.port)
        self.tachy.setUpTachy()
        m = Messdaten(2, 1, 2.682489193493133, 1.5956, 0, False)
        success = self.tachy.measureAt_Hz_V(m)

        self.assertTrue(success)

    def test_find_and_measure_Target(self):
        self.tachy.initSerial(self.port)
        self.tachy.setUpTachy()

        success, messdaten = self.tachy.find_and_measure_Target()

        self.assertIsNotNone(messdaten.Hz)
        self.assertIsNotNone(messdaten.V)
        self.assertIsNotNone(messdaten.Dist)

    def test_messAufEinZielinZweiLagen(self):
        self.tachy.initSerial(self.port)
        self.tachy.setUpTachy()
        f = open('workfile.txt', 'a')
        i = 6
        f.write('_______')
        while i != 0:
            i -= 1
            suc, messdaten = self.tachy.find_and_measure_Target()

            f.write(str(messdaten.getHzVD_gon()))
            f.write('\n')
            self.tachy.changeFace()
        f.close()
        self.fail()


    def test_changeFace(self):
        self.tachy.initSerial(self.port)
        self.tachy.setUpTachy()
        success = self.tachy.changeFace()
        self.assertTrue(success)


    def test_TwoTachy(self):
        self.tachy.initSerial(self.port)

        tachy2 = Tachy()
        #tachy2.initSerial('')
        #TODO: Warum wirft er fehler
        # thread.start_new_thread(self.tachy.setUpTachy())
        self.tachy.setUpTachy()
        print 'Hallo '
        self.fail()
        #thread.start_new_thread(tachy2.setUpTachy())

    def test_getPrismType(self):
        self.tachy.initSerial(self.port)
        ptype = self.tachy.getPrismType()
        print(type(ptype))
        print ptype