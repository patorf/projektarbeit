from unittest import TestCase
import logging

import scr.controller.myLogger as mylogger
from scr.model.Messdaten import Messdaten
from scr.model.Vollsatz import Vollsatz


__author__ = 'philippatorf'
mylogger.startLogger()
logger = logging.getLogger(__name__)


class TestVollsatz(TestCase):
    def setUp(self):
        b1 = Messdaten(1, 1, 0.018441149, 1.528032968, 100, True)
        b2 = Messdaten(2, 1, 0.335779706, 1.528036109, 101, True)
        b3 = Messdaten(3, 1, 0.671044191, 1.528026684, 100, True)
        b4 = Messdaten(4, 1, 1.247727505, 1.528067525, 100, True)

        #2.Halbsatz
        bb1 = Messdaten(1, 1, 3.160040086, 4.754960702, 0, True)
        bb2 = Messdaten(2, 1, 3.477366076, 4.754970127, 0, True)
        bb3 = Messdaten(3, 1, 3.812649411, 4.754982694, 0, True)
        bb4 = Messdaten(4, 1, 4.389326441, 4.756071255, 0, True)

        c1 = Messdaten(1, 1, 1.627366986, 1.570796327, 0, True)
        c2 = Messdaten(2, 1, 1.944696118, 1.570796327, 0, True)
        c3 = Messdaten(3, 1, 2.279992019, 1.570796327, 0, True)
        c4 = Messdaten(4, 1, 2.856672191, 1.570796327, 0, True)

        #2.Halbsatz
        cc1 = Messdaten(1, 1, 4.768969064, 1.570796327, 0, True)
        cc2 = Messdaten(2, 1, 5.08628563, 1.570796327, 0, True)
        cc3 = Messdaten(3, 1, 5.421594097, 1.570796327, 0, True)
        cc4 = Messdaten(4, 1, 5.998267986, 1.570796327, 0, True)

        self.v1 = Vollsatz([b1, b3, b2, b4], [bb1, bb2, bb4, bb3])
        self.v2 = Vollsatz([c1, c2, c3, c4], [cc1, cc2, cc3, cc4])


    def test_getCopyOfHalbsatz1(self):
        copyOfFirstSet = self.v1.getCopyOfHalbsatz1()
        for messwert in copyOfFirstSet:
            self.assertTrue(messwert.isMeasured == False)


    def test_getReversSetOfFirstSet(self):
        reversSet = self.v1.getReversSetOfFirstSet()

        self.assertTrue(reversSet[0].TargetNo == self.v1.Halbsaetze[1][-1].TargetNo)

    def test_getAnzBeobachtung(self):
        self.fail()

    def test_postionOfTargetNo(self):
        self.fail()

    def test_addMessdaten(self):
        self.fail()

    def test_sortBeobachtung(self):
        self.fail()

    def test_Satzreduzierung(self):
        self.fail()