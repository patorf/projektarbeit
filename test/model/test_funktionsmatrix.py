from unittest import TestCase

import numpy as np
from model.Messdaten import Messdaten

from scr.model.Beobachtung import Beobachtung
from scr.model.Funktionsmatrix import Funktionsmatrix
from scr.model.Unbekannte import Unbekannte


__author__ = 'Philipp'


#noinspection PyPep8Naming,PyPep8Naming
class TestFunktionsmatrix(TestCase):
    def setUp(self):
        ay = Unbekannte(1, 'y', 90)
        ax = Unbekannte(1, 'x', 75)
        az = Unbekannte(1, 'z', 143)
        aw = Unbekannte(1, 'omega', 2.5)
        bx = Unbekannte(2, 'x', 50)
        by = Unbekannte(2, 'y', 123)
        bz = Unbekannte(2, 'z', 10)
        bw = Unbekannte(2, 'omega', 1.2)

        beoHz = Beobachtung(1, 2, 'Hz', 1.3, 0.1)
        beoV = Beobachtung(1, 2, 'V', 1, 0.1)
        beoDist = Beobachtung(1, 2, 'Dist', 10, 0.1)

        self.funkMatrix = Funktionsmatrix([ay, ax, az, aw, by, bx, bz, bw], [beoHz, beoV, beoDist])


    def test_getAbleitung(self):
        ay = Unbekannte(1, 'y', 100)
        ax = Unbekannte(1, 'x', 100)
        az = Unbekannte(1, 'z', 100)
        aw = Unbekannte(1, 'omega', 100)
        bx = Unbekannte(2, 'x', 50)
        by = Unbekannte(2, 'y', 123)
        bz = Unbekannte(2, 'z', 10)
        bw = Unbekannte(2, 'omega', 100)

        beoHz = Beobachtung(1, 2, 'Hz', 50, 0.1)
        beoV = Beobachtung(1, 2, 'V', 1, 0.1)
        beoDist = Beobachtung(1, 2, 'Dist', 10, 0.1)

        t = Funktionsmatrix([ay, ax, az, aw, by, bx, bz, bw], [beoHz, beoV, beoDist])

        referenceAMatrix = np.array([[0.01650710, 0.00759327, 0.00000000, -1, -0.01650710, -0.00759327, 0.00000000, -1],
                                     [0.00337959, -0.00734694, 0.00494531, 0, -0.00337959, 0.00734694, -0.00494531, 0],
                                     [-0.21802172, 0.47396026, 0.85312848, 0, 0.21802172, -0.47396026, -0.85312848, 0]])
        print referenceAMatrix
        print t.A_Matrix

        np.testing.assert_almost_equal(t.A_Matrix, referenceAMatrix)

    def test_getBZeile(self):
        vectorBy = self.funkMatrix.getBZeile(self.funkMatrix.unbekannten_vektor[0], 3)
        vectorBx = self.funkMatrix.getBZeile(self.funkMatrix.unbekannten_vektor[1], 3)
        vectorBz = self.funkMatrix.getBZeile(self.funkMatrix.unbekannten_vektor[2], 3)

        np.testing.assert_almost_equal(vectorBx, np.array([1, 0, 0, 0, -143, 90, 75]))
        np.testing.assert_almost_equal(vectorBy, np.array([0, 1, 0, 143, 0, -75, 90]))
        np.testing.assert_almost_equal(vectorBz, np.array([0, 0, 1, -90, 75, 0, 143]))

    def test_createB_Matrix(self):
        unV = self.funkMatrix.unbekannten_vektor
        self.funkMatrix.get_B(unV, 3)

        tempB = np.array([[0, 1, 0, 143, 0, -75, 90],
                          [1, 0, 0, 0, -143, 90, 75],
                          [0, 0, 1, -90, 75, 0, 143],
                          [0, 0, 0, 0, 0, 0, 0],
                          [0, 1, 0, 10, 0, -50, 123],
                          [1, 0, 0, 0, -10, 123, 50],
                          [0, 0, 1, -123, 50, 0, 10],
                          [0, 0, 0, 0, 0, 0, 0]])

        np.testing.assert_almost_equal(self.funkMatrix.get_B(unV, 3), tempB)

        tempB_withoutPoint2 = np.array([
            [0, 1, 0, 143, 0, -75, 90],
            [1, 0, 0, 0, -143, 90, 75],
            [0, 0, 1, -90, 75, 0, 143],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]])

        np.testing.assert_almost_equal(self.funkMatrix.get_B(unV, 3, [1]), tempB_withoutPoint2)


    def test_create_erwNorm(self):
        N_Matrix = np.matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
        B_Matrix = np.matrix([[10, 11], [12, 13], [14, 15]])

        erwNorm = self.funkMatrix.create_erwNorm(N_Matrix, B_Matrix)

        test_erwNorm = np.matrix(
            [[1, 2, 3, 10, 11], [4, 5, 6, 12, 13], [7, 8, 9, 14, 15], [10, 12, 14, 0, 0], [11, 13, 15, 0, 0]])

        np.testing.assert_equal(test_erwNorm, erwNorm)


    def test_aproxBeo(self):
    #Epoche 1
        m1 = Messdaten('1', '0', 5.1587476335, 1.68255986505, 2.8957, '7', True)
        m2 = Messdaten('2', '0', 5.38751838246, 1.76400400325, 2.9289, '7', True)
        m3 = Messdaten('3', '0', 5.43686907248, 1.68293217723, 2.8642, '7', True)

        mm1 = Messdaten('1', '0', 2.01760434499, 4.60080901805, 2.8955, '7', True)
        mm2 = Messdaten('2', '0', 2.2463636788, 4.51947437384, 2.9284, '7', True)
        mm3 = Messdaten('3', '0', 2.29570599209, 4.60039853273, 2.8642, '7', True)

        e = Epoche()
        s1 = e.addNewStation()

        self.funkMatrix.calc_aproxBeobachtung()

        print self.funkMatrix.get_l()

        self.fail()

