__author__ = 'Philipp'
#
#from Tkinter import Tk, Frame, BOTH
#
#
#class Example(Frame):
#    def __init__(self, parent):
#        Frame.__init__(self, parent, background="white")
#
#        self.parent = parent
#
#        self.initUI()
#
#    def initUI(self):
#        self.parent.title("Simple")
#        self.pack(fill=BOTH, expand=1)
#
#
#def main():
#    root = Tk()
#    root.geometry("250x150+300+300")
#    app = Example(root)
#    root.mainloop()
#
#
#if __name__ == '__main__':
#    main()

import unittest
import datetime


class DatePattern:
    def __init__(self, year, month, day):
        self.date = datetime.date(year, month, day)

    def matches(self, date):
        return self.date == date


class FooTest(unittest.TestCase):
    def testMatches(self):
        p = DatePattern(2004, 9, 28)
        d = datetime.date(2004, 9, 28)
        self.failUnless(p.matches(d))

    def testMatchesFalse(self):
        p = DatePattern(2004, 9, 28)
        d = datetime.date(2004, 9, 28)
        self.failIf(p.matches(d))


def main():
    unittest.main()


if __name__ == '__main__':
# main()
    pass

a = [1, 2]

for i in a:
    i = 10

print i