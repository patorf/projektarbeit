# coding=utf-8
import numpy.linalg as linalg
import numpy as np
import matplotlib.pyplot as plt
import math
from mpl_toolkits.mplot3d import axes3d, Axes3D  #<-- Note the capitalization!


def draw():
    #Todo Ellipsoid: Schleife schreiben fuer alle Punkte

    #Drehwinkel des Ellipsoides

    #Todo Drehwinkel berechnen

    #Vektoren der Halbachsen

    #TODO mit wahren Werten der einzelnen Punkte fuellen

    konfA = np.array([[-0.00036547], [0.00033763], [0.00296981]])
    konfB = np.array([[-0.00379214], [-0.00147546], [-0.00029893]])
    konfC = np.array([[0.00434914], [-0.01155247], [0.0018485]])

    #Einheitsvektoren im Koordinatensystem

    ky = np.array([[1], [0], [0]])
    kx = np.array([[0], [1], [0]])
    kz = np.array([[0], [0], [1]])

    #Berechnung der Drehwinkel

    #alpha

    dot = konfA.item(0) * ky.item(0) + konfA.item(1) * ky.item(1) + konfA.item(2) * ky.item(2)
    x_modulus = np.sqrt(konfA.item(0) ** 2 + konfA.item(1) ** 2 + konfA.item(2) ** 2)
    y_modulus = np.sqrt(ky.item(0) ** 2 + ky.item(1) ** 2 + ky.item(2) ** 2)
    cos_angle = dot / (x_modulus * y_modulus)
    angle = np.arccos(cos_angle)

    alpha = angle * 200 / np.pi

    dot = konfA.item(0) * kx.item(0) + konfA.item(1) * kx.item(1) + konfA.item(2) * kx.item(2)
    x_modulus = np.sqrt(konfA.item(0) ** 2 + konfA.item(1) ** 2 + konfA.item(2) ** 2)
    y_modulus = np.sqrt(kx.item(0) ** 2 + kx.item(1) ** 2 + kx.item(2) ** 2)
    cos_angle = dot / (x_modulus * y_modulus)
    angle = np.arccos(cos_angle)

    alpha1 = angle * 200 / np.pi

    dot = konfA.item(0) * kz.item(0) + konfA.item(1) * kz.item(1) + konfA.item(2) * kz.item(2)
    x_modulus = np.sqrt(konfA.item(0) ** 2 + konfA.item(1) ** 2 + konfA.item(2) ** 2)
    y_modulus = np.sqrt(kz.item(0) ** 2 + kz.item(1) ** 2 + kz.item(2) ** 2)
    cos_angle = dot / (x_modulus * y_modulus)
    angle = np.arccos(cos_angle)

    alpha2 = angle * 200 / np.pi

    #beta

    dot = konfB.item(0) * kx.item(0) + konfB.item(1) * kx.item(1) + konfB.item(2) * kx.item(2)
    x_modulus = np.sqrt(konfB.item(0) ** 2 + konfB.item(1) ** 2 + konfB.item(2) ** 2)
    y_modulus = np.sqrt(kx.item(0) ** 2 + kx.item(1) ** 2 + kx.item(2) ** 2)
    cos_angle = dot / (x_modulus * y_modulus)
    angle = np.arccos(cos_angle)

    beta = angle * 200 / np.pi

    #gamma

    dot = konfC.item(0) * kz.item(0) + konfC.item(1) * kz.item(1) + konfC.item(2) * kz.item(2)
    x_modulus = np.sqrt(konfC.item(0) ** 2 + konfC.item(1) ** 2 + konfC.item(2) ** 2)
    y_modulus = np.sqrt(kz.item(0) ** 2 + kz.item(1) ** 2 + kz.item(2) ** 2)
    cos_angle = dot / (x_modulus * y_modulus)
    angle = np.arccos(cos_angle)

    gamma = angle * 200 / np.pi




    #Halbachsenlaengen des Ellipsoides

    #Todo mit wahren werten der einzelnen Punkte fuellen

    konfA_laenge = [0]
    konfB_laenge = [10]
    konfC_laenge = [1]

    # Set of all spherical angles:
    u = np.linspace(0, 2 * np.pi, 50)
    v = np.linspace(0, np.pi, 50)


    #Berechnung des Ellipsoides

    xellX = konfA_laenge * np.outer(np.cos(u), np.sin(v))
    yellX = konfB_laenge * np.outer(np.sin(u), np.sin(v))
    zellX = konfC_laenge * np.outer(np.ones_like(u), np.cos(v))

    #Rotation des Ellipsoides

    xrotX = xellX
    yrotX = math.cos(alpha) * yellX + (-math.sin(alpha)) * zellX
    zrotX = math.sin(alpha) * yellX + (math.cos(alpha)) * zellX

    xrotY = math.cos(alpha1) * xrotX + (math.sin(alpha1)) * zrotX
    yrotY = yrotX
    zrotY = -math.sin(alpha1) * xrotX + (math.cos(alpha1)) * zrotX

    xrotZ = math.cos(alpha2) * xrotY + (-math.sin(alpha2)) * yrotY
    yrotZ = math.sin(alpha2) * xrotY + (math.cos(alpha2)) * yrotY
    zrotZ = zrotY

    xrotX1 = xellX
    yrotX1 = math.cos(alpha) * yellX + (-math.sin(alpha)) * zellX
    zrotX1 = math.sin(alpha) * yellX + (math.cos(alpha)) * zellX

    xrotY1 = math.cos(beta) * xrotX + (math.sin(beta)) * zrotX
    yrotY1 = yrotX
    zrotY1 = -math.sin(beta) * xrotX + (math.cos(beta)) * zrotX

    xrotZ1 = math.cos(gamma) * xrotY + (-math.sin(gamma)) * yrotY
    yrotZ1 = math.sin(gamma) * xrotY + (math.cos(gamma)) * yrotY
    zrotZ1 = zrotY
    #Bruecke

    #Seile

    sy1 = [2.25, 2.25, 2.25, 4.5, 2.25, 2.25, 2.25]
    sx1 = [-58.5, 0, 4.5, 42, 79.5, 84, 137.5]
    sz1 = [0, 45, 45, 19.75, 45, 45, 0]

    sy2 = [26.75, 26.75, 26.75, 24.5, 26.75, 26.75, 26.75]
    sx2 = [-58.5, 0, 4.5, 42, 79.5, 84, 137.5]
    sz2 = [0, 45, 45, 19.75, 45, 45, 0]

    #Pfeiler

    y = [0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 0, 0, 4.5, 4.5, 4.5, 4.5, 0, 0, 4.5, 4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5,
         24.5, 4.5, 4.5, 4.5]
    x = [0, 0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 0, 0, 4.5, 4.5, 4.5, 0, 0,
         0,
         4.5]
    z = [0, 0, 0, 0, 0, 45, 45, 45, 45, 45, 45, 0, 0, 45, 45, 0, 0, 18, 18, 18, 18, 18, 21.5, 21.5, 21.5, 21.5, 21.5,
         21.5]

    yy = [24.5, 29, 29, 24.5, 24.5, 24.5, 29, 29, 24.5, 24.5, 29, 29, 29, 29, 24.5, 24.5]
    xx = [0, 0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 4.5, 4.5]
    zz = [0, 0, 0, 0, 0, 45, 45, 45, 45, 45, 45, 0, 0, 45, 45, 0]

    yyy = [0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 0, 0, 4.5, 4.5, 4.5, 4.5, 0, 0, 4.5, 4.5, 24.5, 24.5, 4.5, 4.5, 4.5,
           24.5,
           24.5, 4.5, 4.5, 4.5]
    xxx = [79.75, 79.75, 84.25, 84.25, 79.75, 79.75, 79.75, 84.25, 84.25, 79.75, 79.75, 79.75, 84.25, 84.25, 84.25,
           84.25, 84.25, 84.25,
           84.25, 79.75, 79.75, 84.25, 84.25, 84.25, 79.75, 79.75, 79.75, 84.25]
    zzz = [0, 0, 0, 0, 0, 45, 45, 45, 45, 45, 45, 0, 0, 45, 45, 0, 0, 18, 18, 18, 18, 18, 21.5, 21.5, 21.5, 21.5, 21.5,
           21.5]

    yyyy = [24.5, 29, 29, 24.5, 24.5, 24.5, 29, 29, 24.5, 24.5, 29, 29, 29, 29, 24.5, 24.5]
    xxxx = [79.75, 79.75, 84.25, 84.25, 79.75, 79.75, 79.75, 84.25, 84.25, 79.75, 79.75, 79.75, 84.25, 84.25, 84.25,
            84.25]
    zzzz = [0, 0, 0, 0, 0, 45, 45, 45, 45, 45, 45, 0, 0, 45, 45, 0]


    #Fahrbahn
    #Fahrbahn

    a1 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b1 = [5, 5, 10.25, 10.25, 5, 5, 5, 5, 10.25, 10.25, 5, 10.25, 10.25, 10.25, 10.25, 5]
    c1 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a2 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b2 = [10.75, 10.75, 16, 16, 10.75, 10.75, 10.75, 10.75, 16, 16, 10.75, 16, 16, 16, 16, 10.75]
    c2 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a3 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b3 = [16.5, 16.5, 21.75, 21.75, 16.5, 16.5, 16.5, 16.5, 21.75, 21.75, 16.5, 21.75, 21.75, 21.75, 21.75, 16.5]
    c3 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a4 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b4 = [22.25, 22.25, 27.5, 27.5, 22.25, 22.25, 22.25, 22.25, 27.5, 27.5, 22.25, 27.5, 27.5, 27.5, 27.5, 22.25]
    c4 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a5 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b5 = [28, 28, 33.25, 33.25, 28, 28, 28, 28, 33.25, 33.25, 28, 33.25, 33.25, 33.25, 33.25, 28]
    c5 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a6 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b6 = [33.75, 33.75, 39, 39, 33.75, 33.75, 33.75, 33.75, 39, 39, 33.75, 39, 39, 39, 39, 33.75]
    c6 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a7 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b7 = [39.5, 39.5, 44.75, 44.75, 39.5, 39.5, 39.5, 39.5, 44.75, 44.75, 39.5, 44.75, 44.75, 44.75, 44.75, 39.5]
    c7 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a8 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b8 = [45.25, 45.25, 50.5, 50.5, 45.25, 45.25, 45.25, 45.25, 50.5, 50.5, 45.25, 50.5, 50.5, 50.5, 50.5, 45.25]
    c8 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a9 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b9 = [51, 51, 56.25, 56.25, 51, 51, 51, 51, 56.25, 56.25, 51, 56.25, 56.25, 56.25, 56.25, 51]
    c9 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a10 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b10 = [56.75, 56.75, 62, 62, 56.75, 56.75, 56.75, 56.75, 62, 62, 56.75, 62, 62, 62, 62, 56.75]
    c10 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a11 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b11 = [62.5, 62.5, 67.75, 67.75, 62.5, 62.5, 62.5, 62.5, 67.75, 67.75, 62.5, 67.75, 67.75, 67.75, 67.75, 62.5]
    c11 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a12 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b12 = [68.25, 68.25, 73.5, 73.5, 68.25, 68.25, 68.25, 68.25, 73.5, 73.5, 68.25, 73.5, 73.5, 73.5, 73.5, 68.25]
    c12 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

    a13 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
    b13 = [74, 74, 79.25, 79.25, 74, 74, 74, 74, 79.25, 79.25, 74, 79.25, 79.25, 79.25, 79.25, 74]
    c13 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]



    #Prismen

    Objektpunktey = [0, 4, 4, 4, 4, 0]
    Objektpunktex = [65.5, 12, 25, 43, 57, 2]
    Objektpunktez = [45, 11.5, 11.5, 11.5, 11.5, 45]
    #

    Referenzpunkty = []
    Referenzpunktx = []
    Referenzpunktz = []



    # plot

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(Objektpunktey, Objektpunktex, Objektpunktez)
    ax.scatter(Referenzpunkty, Referenzpunktx, Referenzpunktz)
    ax.plot(y, x, z, color='b')
    ax.plot(yy, xx, zz, color='b')
    ax.plot(yyy, xxx, zzz, color='b')
    ax.plot(yyyy, xxxx, zzzz, color='b')
    ax.plot(a1, b1, c1, color='r')
    ax.plot(a2, b2, c2, color='r')
    ax.plot(a3, b3, c3, color='r')
    ax.plot(a4, b4, c4, color='r')
    ax.plot(a5, b5, c5, color='r')
    ax.plot(a6, b6, c6, color='r')
    ax.plot(a7, b7, c7, color='r')
    ax.plot(a8, b8, c8, color='r')
    ax.plot(a9, b9, c9, color='r')
    ax.plot(a10, b10, c10, color='r')
    ax.plot(a11, b11, c11, color='r')
    ax.plot(a12, b12, c12, color='r')
    ax.plot(a13, b13, c13, color='r')
    ax.plot(sy1, sx1, sz1, color='g')
    ax.plot(sy2, sx2, sz2, color='g')


    #ax.plot_surface(xellX, yellX, zellX, rstride=4, cstride=4, color='b')
    ax.plot_surface(xrotZ, yrotZ, zrotZ, rstride=4, cstride=4, color='g')

    plt.axis('equal')
    #plt.axis('off')
    ax.autoscale_view(False, False, False)
    ax.auto_scale_xyz([0, 100], [0, 100], [0, 100])

    #plt.axes().set_aspect('equal')

    plt.show()
    plt.close(fig)
    del fig


draw()