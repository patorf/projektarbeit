from model.Tachy import Tachy
from model.LeicaGeoCom import LeicaGeoCom as LGC

myTachy = Tachy()

makePositioning_request = LGC.AUT_MakePositioning(Hz=0.3, V=3.4, POSMode=1, ATRMode=1)
#POSMode = 1 -> exact positioning mode
#ATRMode = 1 -> Positioning to a target in the environment of the hz- and v-angle.

# returntype is tuple with two elements
#requestString, responseArray =makePositioning_request
#requestString = "%R1Q,9027:0.3,3.4,1,1,0\r"
#responseArray = ["RC"]

success, responseDictionary = myTachy.sendTachy(geoComRequest=makePositioning_request)
#responseDictionary['RC']

