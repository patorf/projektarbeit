# coding=utf-8
import math

__author__ = 'Philipp'

import numpy as np


# noinspection PyCallingNonCallable
class Trafo:
    def __init__(self, zielsystem, startsystem):
        """



        @param zielsystem: array (x,y,z) mathematisches System
        @param startsystem: array (x,y,z) mathematisches System
        @type startsystem: list of list
        @type zielsystem: list of list

        """


        #convert to Numpy
        self.trafoParam = None
        self.zielsystem = np.zeros((len(zielsystem), 3))
        self.startsystem = np.zeros((len(startsystem), 3))

        self.R = None
        self.m = None
        self.t = None
        self.trafoParam = None
        self.s_u = None

        self.success = False
        for i, xyz in enumerate(zielsystem):
            self.zielsystem[i] = np.array(xyz)

        for i, xyz in enumerate(startsystem):
            self.startsystem[i] = np.array(xyz)

        if len(zielsystem) >= 3 and len(zielsystem) == len(startsystem):

            self.calcParameter()

        else:

            print 'Trafo Geht nicht'

    def calcParameter(self):

        #Calc mean
        ziel_Schwerpunkt = np.average(self.zielsystem, axis=0)
        start_Schwerpunkt = np.average(self.startsystem, axis=0)


        #Schwerpunktreduzierte Koordinaten
        ziel_SchRed = np.zeros((len(self.zielsystem), 3))
        start_SchRed = np.zeros((len(self.startsystem), 3))
        for i in range(len(self.zielsystem)):
            ziel_SchRed[i] = self.zielsystem[i] - ziel_Schwerpunkt
            start_SchRed[i] = self.startsystem[i] - start_Schwerpunkt

        N = np.zeros((4, 4))
        for i in range(len(ziel_SchRed)):
            A_i = np.zeros((4, 4))

            xi = start_SchRed[i][0]
            yi = start_SchRed[i][1]
            zi = start_SchRed[i][2]

            Xi = ziel_SchRed[i][0]
            Yi = ziel_SchRed[i][1]
            Zi = ziel_SchRed[i][2]

            A_i[0] = [0, -(xi - Xi), -(yi - Yi), -(zi - Zi)]
            A_i[1] = [(xi - Xi), 0, (zi + Zi), -(yi + Yi)]
            A_i[2] = [(yi - Yi), -(zi + Zi), 0, (xi + Xi)]
            A_i[3] = [(zi - Zi), (yi + Yi), -(xi + Xi), 0]

            A_im = np.matrix(A_i)

            N = N + A_im.T * A_im

        U, D, V = np.linalg.svd(N, full_matrices=True)

        i_minEigenwert = D.argmin()

        minEigenvector = U[:, i_minEigenwert]

        q0 = minEigenvector[0]
        q1 = minEigenvector[1]
        q2 = minEigenvector[2]
        q3 = minEigenvector[3]

        r1 = np.array(
            [(q0 ** 2 + q1 ** 2 - q2 ** 2 - q3 ** 2), ( 2 * (q1 * q2 - q0 * q3)), ( 2 * (q1 * q3 + q0 * q2))]).T
        r2 = np.array([2 * (q1 * q2 + q0 * q3), q0 ** 2 - q1 ** 2 + q2 ** 2 - q3 ** 2, 2 * (q2 * q3 - q0 * q1)]).T
        r3 = np.array([2 * (q3 * q1 - q0 * q2), 2 * (q3 * q2 + q0 * q1), q0 ** 2 - q1 ** 2 - q2 ** 2 + q3 ** 2]).T

        R = np.matrix([r1[0][0], r2[0][0], r3[0][0]])

        self.R = R

        #Maßstab
        zahler = 0
        nenner = 0

        for i in range(len(ziel_SchRed)):
            ziel_SchRed_i_M = np.matrix(ziel_SchRed[i, :])
            start_SchRed_i_M = np.matrix(start_SchRed[i, :])

            zahler = zahler + ( ziel_SchRed_i_M * R * start_SchRed_i_M.T)
            nenner = nenner + ( start_SchRed_i_M * start_SchRed_i_M.T)

        m = float(zahler / nenner)

        #Translationen
        t = np.matrix(ziel_Schwerpunkt).T - m * R * np.matrix(start_Schwerpunkt).T

        alpha = math.atan(-R[2, 1] / R[2, 2])
        beta = math.asin(R[2, 0])
        gamma = math.atan(-R[1, 0] / R[0, 0])

        self.m = m
        self.t = t
        self.trafoParam = np.array([alpha, beta, gamma, m, t[0], t[1], t[2]])
        self.success = True

    def calcAccuracy(self):
        #näherungswerte
        if not self.success:
            print 'berechnung der Trafo Genauigkeit geht nicht'
            return False
        start_ = np.zeros((len(self.startsystem), 3))

        m = self.trafoParam[3]
        t = np.array([self.trafoParam[4], self.trafoParam[5], self.trafoParam[6]])

        for i, xyz in enumerate(self.startsystem):
            xyz_Approx = np.dot(m, np.dot(self.R, xyz.T)) + t
            start_[i] = xyz_Approx
            #np.insert(start_,xyz_Approx,axis=1)

        A = np.zeros((len(self.startsystem) * 3, 7))

        for i, xyz in enumerate(start_):
            j = i * 3
            xi = xyz[0]
            yi = xyz[1]
            zi = xyz[2]

            A[j] = np.array([0, zi, -yi, xi, 1, 0, 0])
            A[j + 1] = np.array([zi, 0, xi, yi, 0, 1, 0])
            A[j + 2] = np.array([-yi, -xi, 0, zi, 0, 0, 1])

        L = self.zielsystem.flatten()

        N = np.dot(A.T, A)
        n = np.dot(A.T, L)
        u = np.dot(np.linalg.inv(N), n)
        Quu = np.linalg.inv(N)
        #Restklaffen
        v = np.dot(A, u) - L

        r = len(L) - len(u)
        s0_Post = math.sqrt(np.dot(v, v) / r)

        s_u = Quu.diagonal().copy()

        for i, s_u_i in enumerate(s_u):
            s_u[i] = s0_Post * math.sqrt(s_u[i])

        self.s_u = s_u
        print 'Restklaffen'
        print v


    def transform(self, points):
        """

        @type points: list of list
        @return:
        """
        returnlist = []
        for xyz in points:
            if not self.success:
                return False

            xyz_start = np.matrix(xyz).T

            xyz_ziel = np.dot(self.m, np.dot(self.R, xyz_start)) + self.t
            returnlist.append(np.array(xyz_ziel).reshape(-1).tolist())
        return returnlist

    def transformBack(self, point):
        if not self.success:
            return False
        xyz_ziel = np.matrix(point).T

        xyz_start = np.dot(np.linalg.inv(np.dot(self.m, self.R)), xyz_ziel - self.t)

        return np.array(xyz_start).reshape(-1).tolist()

