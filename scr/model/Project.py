import PySide

from model.Messdaten import Messdaten
from model.Epoche import Epoche


class Project(PySide.QtCore.QObject):
    """Class Project
    """
    # Attributes:


    #QT Signals
    updateName = PySide.QtCore.Signal(str)
    updateEpoch = PySide.QtCore.Signal()


    def __init__(self, name='new Project'):
        PySide.QtCore.QObject.__init__(self)

        self.epochen = []
        """:type : list of Epoche"""
        self.defoAnalyse = None
        """:type : Deformationsanalyse"""
        self.name = name



        # Operations


    def addEpoche(self):
        """

        @rtype : Epoche
        """
        newEpoche = Epoche()
        self.epochen.append(newEpoche)
        newEpoche.epochUpdated.connect(self.epochHasUpdated)

        return newEpoche

    def getCurrentEpoch(self):
        if len(self.epochen) == 0:
            return self.addEpoche()
        else:
            return self.epochen[-1]

    def epochHasUpdated(self):
        self.updateEpoch.emit()

    def removeEpoche(self, epoche):
        """


        @type epoche: Epoche
        @param epoche: Epoche to remove
        """
        if epoche in self.epochen:
            self.epochen.remove(epoche)
            self.updateEpoch.emit()

    def removeAllEpochs(self):
        del self.epochen[:]

    @staticmethod
    def getAvailablePointTypes():
        """

        @rtype : dict
        """
        return Messdaten.availableTypes


    def addNewEpochwithSetUpFromOtherEpoch(self, otherEpoch):
        """



        @param otherEpoch:
        @rtype : Epoche
        @type otherEpoch: Epoche
        """

        newEpoch = self.addEpoche()

        for otherStation in otherEpoch.stations:
            newStation = newEpoch.addNewStation()
            newStation.tachy = otherStation.getTachy()

            newStation.targetDefinition = otherStation.targetDefinition

        return newEpoch


    @property
    def name(self):
        return self._name

    # noinspection PyAttributeOutsideInit
    @name.setter
    def name(self, name):
        self._name = name

        self.updateName.emit(self._name)


    def getDemoEpoche1(self, epoche):
        #Epoche 1
        m1 = Messdaten('1', '0', 5.1587476335, 1.68255986505, 2.8957, '7', True)
        m2 = Messdaten('2', '0', 5.38751838246, 1.76400400325, 2.9289, '7', True)
        m3 = Messdaten('3', '1', 5.43686907248, 1.68293217723, 2.8642, '7', True)

        mm1 = Messdaten('1', '0', 2.01760434499, 4.60080901805, 2.8955, '7', True)
        mm2 = Messdaten('2', '0', 2.2463636788, 4.51947437384, 2.9284, '7', True)
        mm3 = Messdaten('3', '1', 2.29570599209, 4.60039853273, 2.8642, '7', True)

        #Staton 0 eigentlich 2
        #Satz 1


        m2_1 = Messdaten('1', '0', 0.235180185412, 1.65241748138, 3.9917, '7', True)
        m2_2 = Messdaten('2', '0', 0.372688873969, 1.72771118105, 3.6194, '7', True)
        m2_3 = Messdaten('3', '1', 0.397739763517, 1.66381361393, 3.4776, '7', True)

        mm2_1 = Messdaten('1', '0', 3.37674065164, 4.63079854752, 3.9921, '7', True)
        mm2_2 = Messdaten('2', '0', 3.51429478978, 4.55552995593, 3.6198, '7', True)
        mm2_3 = Messdaten('3', '1', 3.53930586878, 4.6194024662, 3.4773, '7', True)

        e = epoche
        s1 = e.addNewStation()

        v = s1.addNewVollsatz()
        v.Halbsaetze[1] = [m1, m2, m3]
        v.Halbsaetze[2] = [mm1, mm2, mm3]

        s2 = e.addNewStation()
        v2 = s2.addNewVollsatz()
        v2.Halbsaetze[1] = [m2_1, m2_2, m2_3]
        v2.Halbsaetze[2] = [mm2_1, mm2_2, mm2_3]

        return e

    def getDeomoEpcohe2(self, epoche):

        #Epoche2
        versatz = 0.003
        zneu = 1.68293217723 - (  versatz / 2.8642)
        z2neu = 4.60039853273 + (versatz / 2.8642)

        e2_m1 = Messdaten('1', '0', 5.1587476335, 1.68255986505, 2.8957, '7', True)
        e2_m2 = Messdaten('2', '0', 5.38751838246, 1.76400400325, 2.9289, '7', True)
        e2_m3 = Messdaten('3', '1', 5.43686907248, zneu, 2.8642, '7', True)

        e2_mm1 = Messdaten('1', '0', 2.01760434499, 4.60080901805, 2.8955, '7', True)
        e2_mm2 = Messdaten('2', '0', 2.2463636788, 4.51947437384, 2.9284, '7', True)
        e2_mm3 = Messdaten('3', '1', 2.29570599209, z2neu, 2.8642, '7', True)

        #Staton 0 eigentlich 2
        #Satz 1

        s2Zneu = 4.6194024662 + (versatz / 3.4773)
        s2Z2neu = 1.66381361393 - (versatz / 3.4776)

        e2_m2_1 = Messdaten('1', '0', 0.235180185412, 1.65241748138, 3.9917, '7', True)
        e2_m2_2 = Messdaten('2', '0', 0.372688873969, 1.72771118105, 3.6194, '7', True)
        e2_m2_3 = Messdaten('3', '1', 0.397739763517, s2Z2neu, 3.4776, '7', True)

        e2_mm2_1 = Messdaten('1', '0', 3.37674065164, 4.63079854752, 3.9921, '7', True)
        e2_mm2_2 = Messdaten('2', '0', 3.51429478978, 4.55552995593, 3.6198, '7', True)
        e2_mm2_3 = Messdaten('3', '1', 3.53930586878, s2Zneu, 3.4773, '7', True)

        e2 = epoche
        e2s1 = e2.addNewStation()

        e2v = e2s1.addNewVollsatz()
        e2v.Halbsaetze[1] = [e2_m1, e2_m2, e2_m3]
        e2v.Halbsaetze[2] = [e2_mm1, e2_mm2, e2_mm3]

        e2s2 = e2.addNewStation()
        e2v2 = e2s2.addNewVollsatz()
        e2v2.Halbsaetze[1] = [e2_m2_1, e2_m2_2, e2_m2_3]
        e2v2.Halbsaetze[2] = [e2_mm2_1, e2_mm2_2, e2_mm2_3]

        return e2