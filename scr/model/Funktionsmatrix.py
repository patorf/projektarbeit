# coding=utf-8
"""

"""
import math
import numpy as np
from model.Unbekannte import Unbekannte
from model.Beobachtung import Beobachtung

__author__ = 'Philipp'


#noinspection PyShadowingBuiltins
class Funktionsmatrix:
    """

    @param unbekannten_vektor:
    @param beobachtungs_vektor:
    """

    def __init__(self, unbekannten_vektor, beobachtungs_vektor):
        """


        @type beobachtungs_vektor: list of Beobachtung
        @type unbekannten_vektor: list of Unbekannte

        """
        self.unbekannten_vektor = unbekannten_vektor
        self.beobachtungs_vektor = beobachtungs_vektor
        self.aproxBeobachtung = self.calc_aproxBeobachtung()
        self.B_Matrix = None

        self.A_Matrix = np.zeros((len(beobachtungs_vektor), len(unbekannten_vektor)))

        a_list = []
        for b, beo in enumerate(beobachtungs_vektor):
            a_list.append([])
            for u, unb in enumerate(unbekannten_vektor):
                a_list[b].append(self.getAbleitung(unb, beo))
        self.A_Matrix = np.matrix(a_list)


    def get_A(self):
        return self.A_Matrix

    def calc_aproxBeobachtung(self):
        shortBeo = []

        for beo in self.beobachtungs_vektor:

            target_y = self.getUnbValue(beo.targetNo, 'y')
            target_x = self.getUnbValue(beo.targetNo, 'x')
            target_z = self.getUnbValue(beo.targetNo, 'z')
            station_y = self.getUnbValue(beo.stationNo, 'y')
            station_x = self.getUnbValue(beo.stationNo, 'x')
            station_z = self.getUnbValue(beo.stationNo, 'z')
            if beo.type == 'Hz':
                omega = self.getOmega(beo.stationNo)

                deltaY = float(target_y - station_y)
                deltaX = float(target_x - station_x)

                if deltaX == 0:
                    deltaX = 1.0e-14
                t = math.atan(deltaY / deltaX)

                if deltaY > 0 > deltaX:
                    t += math.pi
                elif deltaY < 0 and deltaX < 0:
                    t += math.pi
                elif deltaY < 0 < deltaX:
                    t += 2 * math.pi

                hz = t - omega

                if hz < 0:
                    hz += 2 * math.pi
                if hz > 2 * math.pi:
                    hz = hz - 2 * math.pi

                newBeo = Beobachtung(beo.stationNo, beo.targetNo, beo.type, hz, beo.stdAb)
                shortBeo.append(newBeo)

            elif beo.type == 'V':
                s_Hor = math.sqrt((target_x - station_x) ** 2 + (target_y - station_y) ** 2)
                h = target_z - station_z

                if h == 0:
                    h = 1.0e-14
                v = math.atan(s_Hor / math.fabs(h))

                if h < 0:
                    v = math.pi - v

                newBeo = Beobachtung(beo.stationNo, beo.targetNo, beo.type, v, beo.stdAb)
                shortBeo.append(newBeo)


            elif beo.type == 'Dist':
                dist = math.sqrt(
                    (target_x - station_x) ** 2 + (target_y - station_y) ** 2 + (target_z - station_z) ** 2)
                newBeo = Beobachtung(beo.stationNo, beo.targetNo, beo.type, dist, beo.stdAb)
                shortBeo.append(newBeo)

        return shortBeo

    def get_L(self):
        _L = []
        for beo in self.beobachtungs_vektor:
            _L.append(beo.value)
        return np.matrix([_L]).T

    def get_L0(self):
        _L0 = []
        for beo in self.aproxBeobachtung:
            _L0.append(beo.value)
        return np.matrix([_L0]).T

    def get_l(self):


        #L = self.get_L()
        #L0 = self.get_L0()
        l = self.get_L() - self.get_L0()

        #Damit die Differenz aus L-L0 nicht im bereich von +2pi oder -2pi lieg
        # muss gegucktwerden ob der Absolutwert der Differenz ca. 2pi und dann wird 2pi -abs(L-L0) gerechnet
        for i, beo in enumerate(self.beobachtungs_vektor):

            if beo.type == 'Hz':
                if int(2 * math.pi * 10 ** 1) == int(abs(l.item(i) * 10 ** 1)):
                    l[i] = 2 * math.pi - abs(l.item(i))

        return l


    def getOmega(self, pointNo):
        for unb in self.unbekannten_vektor:
            if unb.valueType == 'omega' and unb.pointNo == pointNo:
                return unb.value


    def get_B(self, unbekannten_vektor, net_dimension, datumspunkte=None):
        """


        @param unbekannten_vektor:
        @param net_dimension:
        @param datumspunkte: Array of PointNo which define the Datumspunkte. if empty -> alle Points are Datumspunke
        @type unbekannten_vektor: list of Unbekannte
        """
        if not datumspunkte: datumspunkte = []

        if net_dimension == 1:
            pass
        elif net_dimension == 2:
            pass
        elif net_dimension == 3:

            b_Matrix = np.zeros((len(unbekannten_vektor), 4))
            #print b_Matrix
            for i, zeilen in enumerate(b_Matrix):

                neueZeile = []
                if len(datumspunkte) == 0:
                    #Lagerung auf allen Punkten
                    neueZeile = self.getBZeile(unbekannten_vektor[i], net_dimension)

                elif unbekannten_vektor[i].pointNo in datumspunkte:
                    # Nur die Datumspunkte bekommen ein Zeile in der B-Matrix
                    neueZeile = self.getBZeile(unbekannten_vektor[i], net_dimension)

                # print neueZeile
                if len(neueZeile) != 0:
                    b_Matrix[i] = neueZeile

            return b_Matrix

    def getUnbValue(self, pktNr, type):

        for unb in self.unbekannten_vektor:
            if unb.pointNo == pktNr and unb.valueType == type:
                return unb.value

    @staticmethod
    def calcS(xi, yi, xk, yk):
        return math.sqrt((xi - xk) ** 2 + (yi - yk) ** 2)
        #Horizontalstrecke

    @staticmethod
    def calcD(xi, yi, zi, xk, yk, zk):

        return math.sqrt((xi - xk) ** 2 + (yi - yk) ** 2 + (zi - zk) ** 2)

    def getAbleitung(self, unbekannte, beobachtung):

        """

        @type unbekannte: Unbekannte
        """
        xi = self.getUnbValue(beobachtung.stationNo, 'x')
        yi = self.getUnbValue(beobachtung.stationNo, 'y')
        zi = self.getUnbValue(beobachtung.stationNo, 'z')
        xk = self.getUnbValue(beobachtung.targetNo, 'x')
        yk = self.getUnbValue(beobachtung.targetNo, 'y')
        zk = self.getUnbValue(beobachtung.targetNo, 'z')

        S = self.calcS(xi, yi, xk, yk)
        D = self.calcD(xi, yi, zi, xk, yk, zk)

        if beobachtung.stationNo == unbekannte.pointNo:
            #Unbekannte ist Standpunkt
            if unbekannte.valueType == 'x':
                if beobachtung.type == 'Hz':

                    return (yk - yi) / S ** 2


                elif beobachtung.type == 'V':

                    return -(xk - xi) * (zk - zi) / D ** 2 / S

                elif beobachtung.type == 'Dist':
                    return -(xk - xi) / D



            elif unbekannte.valueType == 'y':
                if beobachtung.type == 'Hz':

                    return -(xk - xi) / S ** 2

                elif beobachtung.type == 'V':
                    return -(yk - yi) * (zk - zi) / D ** 2 / S

                elif beobachtung.type == 'Dist':
                    return -(yk - yi) / D

            elif unbekannte.valueType == 'z':
                if beobachtung.type == 'Hz':
                    return 0

                elif beobachtung.type == 'V':
                    return S / D ** 2
                elif beobachtung.type == 'Dist':
                    return -(zk - zi) / D

            elif unbekannte.valueType == 'omega':
                if beobachtung.type == 'Hz':
                    return -1

                elif beobachtung.type == 'V':
                    return 0
                elif beobachtung.type == 'Dist':

                    return 0



        elif beobachtung.targetNo == unbekannte.pointNo:
            #Unbakannte ist Zielpunkt
            if unbekannte.valueType == 'x':
                if beobachtung.type == 'Hz':

                    return -(yk - yi) / S ** 2

                elif beobachtung.type == 'V':
                    return (xk - xi) * (zk - zi) / D ** 2 / S
                elif beobachtung.type == 'Dist':
                    return (xk - xi) / D


            elif unbekannte.valueType == 'y':
                if beobachtung.type == 'Hz':

                    return (xk - xi) / S ** 2

                elif beobachtung.type == 'V':
                    return (yk - yi) * (zk - zi) / D ** 2 / S
                elif beobachtung.type == 'Dist':
                    return (yk - yi) / D

            elif unbekannte.valueType == 'z':
                if beobachtung.type == 'Hz':

                    return 0
                elif beobachtung.type == 'V':
                    return -S / D ** 2
                elif beobachtung.type == 'Dist':
                    return (zk - zi) / D

            elif unbekannte.valueType == 'omega':
                if beobachtung.type == 'Hz':
                    return -1

                elif beobachtung.type == 'V':
                    return 0
                elif beobachtung.type == 'Dist':
                    return 0
        else:
            return 0


    def getBZeile(self, unbekannte, net_dimension):
        """
        return the row of the B Matrix for a given Unbekannte and a given Net Dimension

        @type net_dimension: int
        @type unbekannte: Unbekannte
        @param unbekannte:
        @param net_dimension: 1,2,3 For 1D,2D or 3D Net

        """
        xi = self.getUnbValue(unbekannte.pointNo, 'x')
        yi = self.getUnbValue(unbekannte.pointNo, 'y')
        zi = self.getUnbValue(unbekannte.pointNo, 'z')

        returnArray = []

        if net_dimension == 1:
            pass
        elif net_dimension == 2:
            pass
        elif net_dimension == 3:
            if unbekannte.valueType == 'y':
                returnArray = [0, 1, 0, zi, 0, -xi, yi]
                returnArray = [0, 1, 0, -xi]

            elif unbekannte.valueType == 'x':
                returnArray = [1, 0, 0, 0, -zi, yi, xi]
                returnArray = [1, 0, 0, yi]

            elif unbekannte.valueType == 'z':
                returnArray = [0, 0, 1, -yi, xi, 0, zi]
                returnArray = [0, 0, 1, 0]
            else:
                returnArray = [0, 0, 0, 0, 0, 0, 0]
                returnArray = [0, 0, 0, 0]

        return np.array(returnArray)


    @staticmethod
    def create_erwNorm(N_Matrix, B_Matrix):

        help_N = np.zeros((len(B_Matrix.T), len(B_Matrix.T)))
        help_1 = np.concatenate((N_Matrix, B_Matrix), axis=1)
        help_2 = np.concatenate((B_Matrix.T, help_N ), axis=1)
        erwNorm = np.concatenate((help_1, help_2), axis=0)
        return erwNorm


