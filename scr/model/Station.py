# coding=utf-8
import copy
from PySide import QtCore
import logging
import math

from model.Stationsmittel import Stationsmittel
from model.Messdaten import Messdaten
from model.Tachy import Tachy
from model.Vollsatz import Vollsatz


logger = logging.getLogger(__name__)
#noinspection PyPep8Naming
class Station(QtCore.QObject):
    """Class Standpunkt
    """
    # Attributes:
    StationCount = 1

    #QT Signals
    pointsUpdated = QtCore.Signal()
    finishedLearning = QtCore.Signal()

    # Operations
    def __init__(self):
        """




        """
        QtCore.QObject.__init__(self)

        self.Saetze = []
        """:type : list of Vollsatz"""
        self.No = 'St' + str(Station.StationCount)
        self.s_Hz_mittel = None
        self.s_V_mittel = None  # ()
        self.s_Dist_mittel = None  # ()
        self.stationsMittel = []  # ([gemittleteBeobachtung])
        """:type : list of Stationsmittel"""
        self.tachy = None
        self.learningFinished = False
        self.targetDefinition = []
        """:type : list of Messdaten"""

        Station.StationCount += 1

    @property
    def learningFinished(self):
        return self._learningFinished

    @learningFinished.setter
    def learningFinished(self, learningFinished):
        self._learningFinished = learningFinished
        if learningFinished:
            self.finishedLearning.emit()


    def pointAddedSlot(self):
        #Slot for the
        self.pointsUpdated.emit()


    def getTachy(self):
        if not self.tachy:
            self.tachy = Tachy()
        return self.tachy


    def addNewVollsatz(self, vollsatz=None):
        """


        @type vollsatz: Vollsatz
        @param vollsatz:
        @rtype : Vollsatz

        """
        if vollsatz is None:
            vollsatz = Vollsatz()

        self.Saetze.append(vollsatz)

        #SLOT For pointAdded Signal
        self.Saetze[-1].pointAdded.connect(self.pointAddedSlot)
        return self.Saetze[-1]

    def getStationsMittel(self):
        self.Satzauswertung()
        return self.stationsMittel

    def getFirstVollsatz(self):
        if len(self.Saetze) == 0:
            return self.addNewVollsatz()
        else:
            return self.Saetze[0]


    def completeFirstSet(self):

        if len(self.Saetze[0].Halbsaetze[2]) == 0:
            targetArray = self.Saetze[0].getReversSetOfFirstSet()

            succes, measuredTargets = self.tachy.measureMultipleTargets(targetArray)
            if succes:
                if len(measuredTargets) != 0:
                    self.Saetze[0].addMessdaten(measuredTargets[::-1], 2)

                self.learningFinished = True
            else:
                logger.warning('Der Zeite Halbsatz wurde zurueck gesetzt!')
                self.Saetze[0].Halbsaetze[2] = []

    def measureAdditionalSet(self, fullset=True):

        """

        @param fullset:
        """
        patternHalfSet = self.targetDefinition

        succes1, newHalfset_1 = self.tachy.measureMultipleTargets(patternHalfSet)
        if succes1:

            #newHalfset_2= self.tachy.measureMultipleTargets()
            newSet = self.addNewVollsatz()
            newSet.addMessdaten(newHalfset_1, 1)

            if fullset:
                succes2, newHalfset_2 = self.tachy.measureMultipleTargets(newSet.getReversSetOfFirstSet())
                if succes2:
                    newSet.addMessdaten(newHalfset_2[::-1], 2)
                else:
                    logger.warning('zweit Halbsatz konnte nicht gemessen werte: Station:%s', str(self.No))

        else:
            logger.warning('erster Halbsatz konnte nicht gemessen werte: Station:%s', str(self.No))

    def defineTarget(self, TargetNo=1, prismType=7, PointType=1):
        """

        @param TargetNo:
        @param prismTyepe: Laica BAP_PRISMTYPE
        @param PointType: ReferencePoint=0, ObjectPoint=1
        """

        succes, messwerte = self.tachy.find_and_measure_Target(prismType)

        #print messwerte
        if succes:
            messwerte.TargetNo = TargetNo
            messwerte.PrismType = prismType
            messwerte.Type = PointType
            firtSet = self.getFirstVollsatz()
            firtSet.addMessdaten([messwerte], 1)

            c_messwerte = copy.copy(messwerte)
            c_messwerte.isMeasured = False
            self.targetDefinition.append(messwerte)

            return succes


    def Satzauswertung(self):
        self.stationsMittel = []
        #Satzreduzierung in jedem Satz
        for vollsatz in self.Saetze:
            vollsatz.satzreduzierung()

        #Satzzusammenfuehrung
        Hz_mittel = {}
        V_mittel = {}
        Dist_mittel = {}
        prismType = {}
        targetType = {}

        i = 0
        for vollsatz in self.Saetze:

            for mittel in vollsatz.Satzmittel:
                pktNr = mittel.TargetNo

                if pktNr in Hz_mittel:
                    Hz_mittel[pktNr] = Hz_mittel[pktNr] + mittel.Hz
                else:
                    Hz_mittel[pktNr] = mittel.Hz

                if pktNr in V_mittel:
                    V_mittel[pktNr] = V_mittel[pktNr] + mittel.V
                else:
                    V_mittel[pktNr] = mittel.V

                if pktNr in Dist_mittel:
                    # print Dist_mittel
                    Dist_mittel[pktNr] = Dist_mittel[pktNr] + mittel.Dist
                else:
                    # print mittel
                    Dist_mittel[pktNr] = mittel.Dist

                prismType[pktNr] = mittel.PrismType
                targetType[pktNr] = mittel.Type

            i += 1

            #  for pkt, hz in Hz_mittel.iteritems():

            #Hz_mittel[pkt] = hz / i
        #print V_mittel
        tempStationsMittel = {}

        for pkt, s in Dist_mittel.iteritems():
            #s_mittel[pkt] = s / i

            gb = Stationsmittel()
            #TODO:Hier hab ich plus 1 rad gemacht
            gb.Hz = (Hz_mittel[pkt] / i )  #+1
            gb.V = V_mittel[pkt] / i
            gb.Dist = Dist_mittel[pkt] / i
            gb.TargetNo = pkt
            gb.PrismType = prismType[pkt]

            gb.Type = targetType[pkt]

            #  print gb.Dist
            #   print gb.Hz
            #reihenvolge von self.stationsmittel ändern (wie vollsätze)
            for ix, messdaten in enumerate(self.Saetze[0].Halbsaetze[1]):
                if messdaten.TargetNo == gb.TargetNo:
                    tempStationsMittel[ix] = gb
                    #self.stationsMittel.append(gb)

        for temp_stationsMittel in tempStationsMittel:
            self.stationsMittel.append(tempStationsMittel[temp_stationsMittel])
        #self.stationsMittel=tempStationsMittel


        #Berechene Hz D
        j = 0
        d_summe = []
        o = []
        anzahlBeo = self.Saetze[0].getAnzBeobachtung()
        for vollsatz in self.Saetze:
            d_summe.append(0)
            o.append(0)

        for vollsatz in self.Saetze:

            for i, mittel in enumerate(vollsatz.Satzmittel):
                vollsatz.Satzmittel[i].calcHzDiff(self.stationsMittel[i])
                d_summe[j] = d_summe[j] + vollsatz.Satzmittel[i].Hz_d
            j += 1

        for i, d in enumerate(d_summe):
            o[i] = d / anzahlBeo

        summeHZ_vv = 0
        summeV_vv = 0
        summeDist_vv = 0

        for i, vollsatz in enumerate(self.Saetze):

            for j, satzMittelBeo in enumerate(vollsatz.Satzmittel):
                satzMittelBeo.calcVVerbe(self.stationsMittel[j].V)

                satzMittelBeo.calcHzVerbe(o[i])
                satzMittelBeo.calcSVerbe(self.stationsMittel[j].Dist)

                summeHZ_vv += (satzMittelBeo.Hz_v * satzMittelBeo.Hz_v)
                summeV_vv += (satzMittelBeo.V_v * satzMittelBeo.V_v)
                summeDist_vv += (satzMittelBeo.Dist_v * satzMittelBeo.Dist_v)

        if True:  #len(self.Saetze) == 1 or anzahlBeo == 1:
            #TODO: Fiktiver Wert!
            s_gon = 0.008 * math.pi / 200

            self.s_Hz_mittel = s_gon
            self.s_V_mittel = s_gon
            self.s_Dist_mittel = 0.001


        else:
            print 'Realwe Werte berechnen t'
            sR = math.sqrt(summeHZ_vv / ((len(self.Saetze) - 1) * (anzahlBeo - 1)))
            sR_m = sR / math.sqrt(len(self.Saetze))
            self.s_Hz_mittel = sR_m

            #Vz
            sV = math.sqrt(summeV_vv / (anzahlBeo * (len(self.Saetze) - 1)))
            sV_m = sV / math.sqrt(len(self.Saetze))
            self.s_V_mittel = sV_m

            #D
            #TODO: ruecksprache Czommer
            sS = math.sqrt(summeDist_vv / ((len(self.Saetze) - 1) * (anzahlBeo - 1)))
            sS_m = sS / math.sqrt(len(self.Saetze))
            self.s_Dist_mittel = sS_m

        #jeder gemittelnten beobachtung die Standardaabweichungen zuweisen
        for beo in self.stationsMittel:
            beo.s_Hz_mittel = self.s_Hz_mittel
            beo.s_V_mittel = self.s_V_mittel
            beo.s_Dist_mittel = self.s_Dist_mittel


    def stationsMittelProtokoll(self, fname):
        self.Satzauswertung()
        fname += '.txt'
        f = open(fname, 'w')

        f.write(u'Station:{0:s}\n\n'.format(self.No))

        f.write("Station\tTarget\tHz\tV\tDist\ts_Hz\ts_V\ts_Dist\n")
        for sMittel in self.stationsMittel:
            hz, v, d = sMittel.getHzVD_gon()
            roh = 200 / math.pi
            s_hz = sMittel.s_Hz_mittel * roh
            s_V = sMittel.s_V_mittel * roh
            s_dist = sMittel.s_Dist_mittel
            f.write(
                u'{0:s}\t{1:s}\t{2:9.5f}\t{3:9.5f}\t{4:9.5f}\t{5:9.5f}\t{6:9.5f}\t{7:9.5f}\n'.format(str(self.No),
                                                                                                     str(
                                                                                                         sMittel.TargetNo),
                                                                                                     hz,
                                                                                                     v, d, s_hz, s_V,
                                                                                                     s_dist))

        f.close()

    def getCoutOfMeasurements(self):


        """
        returns a dict with the targetNo as Key and a other dict as value. The other dict as two keys (count, vollsatz).
        Count says how often a target is measured and vollsatz says if it is measured in two halfsets.
        {1:{'count':2,'vollsatz':True}...


        @rtype : dict
        """

        countList = {}

        firstVollsatz = self.getFirstVollsatz()
        for targets in firstVollsatz.Halbsaetze[1]:
            countList[targets.TargetNo] = {'count': 0, 'vollsatz': False}

        for vollsaetze in self.Saetze:
            for targets in vollsaetze.Halbsaetze[1]:
                countList[targets.TargetNo]['count'] += 1

            for targets in vollsaetze.Halbsaetze[2]:
                countList[targets.TargetNo]['count'] += 1
                countList[targets.TargetNo]['vollsatz'] = True

        return countList


#
#s = Station()
#s.getTachy().initSerial('/dev/cu.360584TS30-SPP')
#s.defineFirstSet()



