#noinspection PyShadowingBuiltins
class Unbekannte:
    """Class Unbekannte
    """


    # Operations
    def __init__(self, pointNo, valueType, value, pointType=0):
        """




        @type pointNo: str or int
        @param pointNo:
        @param pointType: 'x' ->North , 'y'->East, 'z'->Inclitation, 'omega'
        @param value: the value of the point
        """

        self.pointNo = pointNo
        self.valueType = valueType
        self.value = value
        self.pointType = pointType

