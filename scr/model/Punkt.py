# coding=utf-8
import math
import numpy as np


class Punkt:
    """Class Punkt
    """
    # Attributes:


    def __init__(self, pNo, y, x, z, pointtype=0):
        """

        @param y: Rechtswert
        @param x: Hochwert
        @param z: Höhe
        """
        self.No = pNo
        self.x = x
        self.y = y
        self.z = z
        #Type 0-> Referenzpunkt 1-> Objektpunkt  2->Standpunkt
        self.Type = pointtype
        self.x_stAb = None
        self.y_stAb = None
        self.z_stAb = None
        self.konfAxis_A = None
        self.konfAxis_B = None
        self.konfAxis_C = None
        self.qxx = None


    def konfA_laenge(self):
        return np.linalg.norm(self.konfAxis_A)

    def konfB_laenge(self):
        return np.linalg.norm(self.konfAxis_B)

    def konfC_laenge(self):
        return np.linalg.norm(self.konfAxis_C)

    def konf_V(self):
        return 4 / 3 * np.pi * np.linalg.norm(self.konfAxis_A) * 100 * np.linalg.norm(
            self.konfAxis_B) * 100 * np.linalg.norm(self.konfAxis_C) * 100

    def getSTABWPunkt(self):
        return math.sqrt(self.y_stAb ** 2 + self.x_stAb ** 2 + self.z_stAb ** 2)


        # Operations



