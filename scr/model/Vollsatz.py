from PySide import QtCore
import logging
import math
from model.Messdaten import Messdaten
from model.Satzmittel import Satzmittel

logger = logging.getLogger(__name__)


class Vollsatz(QtCore.QObject):
    #QT Signals
    pointAdded = QtCore.Signal()

    # noinspection PyDefaultArgument
    def __init__(self, halbsatz_1=None, halbsatz_2=None):

        """



        @type halbsatz_2: list of Messdaten
        @type halbsatz_1: list of Messdaten
        @param halbsatz_1:
        @param halbsatz_2:
        """
        if not halbsatz_2: halbsatz_2 = []
        if not halbsatz_1: halbsatz_1 = []
        QtCore.QObject.__init__(self)

        self.Halbsaetze = {1: [], 2: []}

        if isinstance(halbsatz_1, list) & isinstance(halbsatz_2, list):
            self.addMessdaten(halbsatz_1, 1)

            self.addMessdaten(halbsatz_2, 2)
            if len(halbsatz_1) != 0:
                self.sortBeobachtung()
                # self.Hz_Mittel = {}
                # self.V_Mittel = {}
                # self.s_Mittel = {}
        self.Satzmittel = []  #Array von Satzmittel (Messdaten)

    def getCopyOfHalbsatz1(self):
        """

        @rtype : list of Messdaten
        """
        returnSet = []
        for messwert in self.Halbsaetze[1]:
            newMesswert = Messdaten(Hz=messwert.Hz, V=messwert.V, prismType=messwert.PrismType, Type=messwert.Type,
                                    istGemessen=False)
            returnSet.append(newMesswert)

        return returnSet

    def getReversSetOfFirstSet(self):
        """

        @rtype : list of Messdaten
        """
        retrunSet = []
        for messwert in reversed(self.Halbsaetze[1]):
            assert isinstance(messwert, Messdaten)

            newMesswert = Messdaten(TargetNo=messwert.TargetNo, Hz=messwert.Hz, V=messwert.V,
                                    prismType=messwert.PrismType, Type=messwert.Type, istGemessen=False)
            newMesswert.convertToSecondFace()

            retrunSet.append(newMesswert)
            #depp to copy the values

        return retrunSet

    def getAnzBeobachtung(self):
        return len(self.Halbsaetze[1])

    def postionOfTargetNo(self, targetNo, withSecondSet=False):
        """


        @type targetNo: str
        """
        posInFirstSet = False
        posInSecondSet = False
        for i, messung in enumerate(self.Halbsaetze[1]):
            assert isinstance(messung, Messdaten)

            if messung.TargetNo == targetNo:
                posInFirstSet = i

        if withSecondSet:
            for i, messung in enumerate(self.Halbsaetze[2]):
                assert isinstance(messung, Messdaten)

                if messung.TargetNo == targetNo:
                    posInFirstSet = i
            return posInFirstSet, posInSecondSet
        else:
            return posInFirstSet


    def addMessdaten(self, newMessdaten, halbsatz):
        """
        append an Array of Messdaten or one Messdaten to a given halbsatz
        @type newMessdaten: list of Messdaten
        @type halbsatz: int
        @param newMessdaten:
        @param halbsatz:
        """
        if isinstance(newMessdaten, Messdaten) & isinstance(halbsatz, (int, long)):

            self.Halbsaetze[halbsatz].append(newMessdaten)
            self.pointAdded.emit()


        elif isinstance(newMessdaten, list):
            for messdaten in newMessdaten:
                self.addMessdaten(messdaten, halbsatz)
        else:
            logger.warning('can not add Messdaten to halbsatz')


    def sortBeobachtung(self):

        """
        sort the first and second halfset

        """
        dic = {}

        #convert array into dict
        for halbs in self.Halbsaetze[1]:
            dic[halbs.TargetNo] = halbs
        self.Halbsaetze[1] = []

        for value in dic:
            self.Halbsaetze[1].append(dic[value])

        dic2 = {}
        for halbs in self.Halbsaetze[2]:
            dic2[halbs.TargetNo] = halbs
        self.Halbsaetze[2] = []
        for value in dic2:
            self.Halbsaetze[2].append(dic[value])


    def satzreduzierung(self):
        self.Satzmittel = []
        pi = math.pi

        Hz_ersteRichtung = None
        for i, beo in enumerate(self.Halbsaetze[1]):
            satzmittel = Satzmittel()

            # HZ
            hz_1 = self.Halbsaetze[1][i].Hz
            hz_2 = self.Halbsaetze[2][i].Hz

            if (hz_1 < pi):
                hz_mittel = ( hz_1 + hz_2 - pi) / 2
            else:
                hz_mittel = (hz_1 + hz_2 + pi) / 2


                # if (hz_2 + pi / 2) > pi:
                #    hz_2 -= pi / 2

                # hz_mittel = (hz_2 + hz_1) / 2
            if i == 0:
                Hz_ersteRichtung = hz_mittel

            satzmittel.Hz = hz_mittel - Hz_ersteRichtung


            #Vz
            v_1 = self.Halbsaetze[1][i].V
            v_2 = self.Halbsaetze[2][i].V

            v_mittel = ((v_1 - v_2) + 2 * pi) / 2
            satzmittel.V = v_mittel

            #Strecken
            s_1 = self.Halbsaetze[1][i].Dist
            s_2 = self.Halbsaetze[2][i].Dist
            Dist_mittel = (s_1 + s_2) / 2
            satzmittel.Dist = Dist_mittel

            satzmittel.TargetNo = self.Halbsaetze[1][i].TargetNo
            satzmittel.PrismType = self.Halbsaetze[1][i].PrismType
            satzmittel.Type = self.Halbsaetze[1][i].Type

            self.Satzmittel.append(satzmittel)

