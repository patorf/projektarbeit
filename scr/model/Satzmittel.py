__author__ = 'Philipp'
from model.Messdaten import Messdaten


class Satzmittel(Messdaten):
    def __init__(self):
        Messdaten.__init__(self)

        self.Hz_d = None
        self.Hz_v = None
        self.V_v = None
        self.Dist_v = None


    def calcHzDiff(self, stationsmittelBeo):
        self.Hz_d = stationsmittelBeo.Hz - self.Hz


    def calcHzVerbe(self, o):
        self.Hz_v = self.Hz_d - o

    def calcVVerbe(self, stationsMittelV):
        self.V_v = stationsMittelV - self.V

    def calcSVerbe(self, stationsMittelS):
        self.Dist_v = stationsMittelS - self.Dist
