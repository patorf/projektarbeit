# coding=utf-8
from PySide import QtCore
from Station import Station
from model.Beobachtung import Beobachtung
from model.Netzausgleichung import Netzausgleichung
from model.Punkt import Punkt
from model.Trafo import Trafo


class Epoche(QtCore.QObject):
    epochenAnz = 0

    #QT Signal
    pointsUpdated = QtCore.Signal(str)
    epochUpdated = QtCore.Signal()

    def __init__(self):
        QtCore.QObject.__init__(self)

        self.stations = []
        """:type : list of Station"""

        self.Name = "Epoche " + str(Epoche.epochenAnz)

        self.netzausgleichung = None

        Epoche.epochenAnz += 1

    def startAusgleichung(self, s0Priori=1, NaeherungswerteDesErstenEpoche=None, winkel_genauigkeit=None,
                          strecken_genauigkeit=None):
        """

        @type NaeherungswerteDesErstenEpoche: list of Punkt
        """
        # koordinaten = NaeherungswerteDesErstenEpoche
        koordinaten = False
        if NaeherungswerteDesErstenEpoche is None:
            koordinaten = self.getKoordinaten()
        else:

            koordinaten = self.getKoordinaten()
            #replace the own Points with the from the other Epoch
            for i, thisPoints in enumerate(koordinaten):
                for otherEpochPoints in NaeherungswerteDesErstenEpoche:
                    if otherEpochPoints.No == thisPoints.No:
                        koordinaten[i] = otherEpochPoints

        if not koordinaten:
            print 'Ausgleichung geht nicht weil keine Koordrinaten gerechnet werden konnten '
            self.netzausgleichung = False
        else:
            self.netzausgleichung = Netzausgleichung(koordinaten, self.getBeobachtungList(
                streckengenauigkeit=strecken_genauigkeit, winkelgenauigkeit=winkel_genauigkeit),
                                                     s0priori=s0Priori)
            self.netzausgleichung.ausgleichung()

        self.epochUpdated.emit()

    def addNewStation(self):
        """
        @rtype : Station

        """

        newStation = Station()
        self.stations.append(newStation)

        self.epochUpdated.emit()

        return newStation


    def getBeobachtungList(self, winkelgenauigkeit=None, streckengenauigkeit=None):
        """
        return the beobachtungsVektor for this Epoche
        @param winkelgenauigkeit: in rad
        @param streckengenauigkeit: in meter
        @rtype : list of Beobachtung
        """
        returnList = []

        for stations in self.stations:
            stations.Satzauswertung()

            for stationsMittel in stations.stationsMittel:
                hz = stationsMittel.Hz
                v = stationsMittel.V
                dist = stationsMittel.Dist

                if winkelgenauigkeit is None:
                    s_v = stationsMittel.s_V_mittel

                    s_hz = stationsMittel.s_Hz_mittel
                else:
                    s_hz = winkelgenauigkeit
                    s_v = winkelgenauigkeit

                if streckengenauigkeit is None:
                    s_dist = stationsMittel.s_Dist_mittel
                else:
                    s_dist = streckengenauigkeit

                targetNo = stationsMittel.TargetNo
                b_Hz = Beobachtung(stations.No, targetNo, 'Hz', hz, s_hz)
                b_V = Beobachtung(stations.No, targetNo, 'V', v, s_v)
                b_Dist = Beobachtung(stations.No, targetNo, 'Dist', dist, s_dist)

                returnList.append(b_Hz)
                returnList.append(b_V)
                returnList.append(b_Dist)

        return returnList


    def getKoordinaten(self):
        """
        return array of Punkten im System des ersten Standpunktes

        @rtype : list of Punkt
        """
        quellSystem = []  #[x,y,z]
        zielSystem = []
        quellStationNo = None
        zielStationNo = None

        for i, stations in enumerate(self.stations):
            stations.Satzauswertung()
            for stationsMittel in stations.stationsMittel:

                p = stationsMittel.convertToPunkt()
                if i == 0:
                    zielStationNo = stations.No
                    zielSystem.append([p.y, p.x, p.z])
                elif i == 1:
                    quellStationNo = stations.No
                    quellSystem.append([p.y, p.x, p.z])

        t = Trafo(zielSystem, quellSystem)
        t.calcAccuracy()

        s2 = t.transform([[0, 0, 0]])[0]
        if not s2:
            return False
            #Standpunkt 2
        pS2 = Punkt(quellStationNo, s2[0], s2[1], s2[2], 2)

        print 'S2', pS2.y, pS2.x, pS2.z
        #Standpunkt 3
        pS1 = Punkt(zielStationNo, 0, 0, 0, 2)

        rList = []
        for i in range(len(zielSystem)):
            pNo = self.stations[0].stationsMittel[i].TargetNo
            pointtype = self.stations[0].stationsMittel[i].Type
            y = zielSystem[i][0]
            x = zielSystem[i][1]
            z = zielSystem[i][2]

            rList.append(Punkt(pNo, y, x, z, pointtype))

        rList.append(pS1)
        rList.append(pS2)

        return rList



