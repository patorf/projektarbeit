#noinspection PyShadowingBuiltins
import math


class Beobachtung:
    def __init__(self, stationNo, targetNo, valuetype, value, stdAb):
        """

        @param stationNo:
        @param targetNo:
        @param valuetype: 'Hz','V' oder 'Dist'
        @param value:
        @param stdAb:
        """
        self.stationNo = stationNo
        self.targetNo = targetNo
        self.type = valuetype
        self.v = None
        self.s_v = None
        self.s_la = None
        self.l_dach = None
        self.value = value
        self.stdAb = stdAb

        self.EV = None
        self.NV = None


    def getl_ausg(self):
        return self.value + self.v


    def getValueAsGon(self):
        return self.value * 200 / math.pi

    def getstdAbAsGon(self):
        return self.stdAb * 200 / math.pi

    def gets_vAsGon(self):
        return self.s_v * 200 / math.pi

    def gets_laAsGon(self):
        return self.s_la * 200 / math.pi

    def getvAsGon(self):
        return self.v * 200 / math.pi

    def getl_dachAsGon(self):
        return self.l_dach * 200 / math.pi

    def getl_ausgAsGon(self):
        return (self.value + self.v) * 200 / math.pi




    
