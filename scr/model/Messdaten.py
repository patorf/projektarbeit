import math

from model.Punkt import Punkt



#noinspection PyShadowingBuiltins
class Messdaten:
    """Class Messdaten

    """
    # Attributes:
    availableTypes = {0: 'Referenzpunkt', 1: 'Objektpunkt', 2: 'Standpunkt'}


    @staticmethod
    def checkType(type):
        """
        check if the type is valid
        @type type: unknown or str
        @return: return a vaild type or None
        """
        if type is None:
            return None
        type = int(type)
        types = Messdaten.availableTypes.keys()
        if type in types:
            return type
        else:
            return None


    def __init__(self, TargetNo=None, Type=None, Hz=None, V=None, Dist=None, prismType=None, istGemessen=False):
        self.TargetNo = TargetNo
        self.Type = self.checkType(Type)
        self.Hz = Hz
        self.V = V
        self.Dist = Dist
        self.isMeasured = istGemessen
        self.PrismType = prismType

    def getHzVD_gon(self):
        """

        @rtype : (float,float,float)
        """
        rho = 200 / math.pi

        return self.Hz * rho, self.V * rho, self.Dist


    def convertToPunkt(self):


        """


        @return: Punkt (y,x,z) representing this Messdaten
        """
        y = (self.Dist * (math.sin(self.V) * (math.sin(self.Hz))))

        x = (self.Dist * (math.sin(self.V) * (math.cos(self.Hz))))

        z = self.Dist * (math.cos(self.V))

        return Punkt(self.TargetNo, y, x, z, self.Type)

    def convertToSecondFace(self):
        """
        convert this Messdaten to scond face. change Hz and V

        """
        self.Hz += math.pi
        if self.Hz > 2 * math.pi:
            self.Hz -= 2 * math.pi

        self.V = 2 * math.pi - self.V











