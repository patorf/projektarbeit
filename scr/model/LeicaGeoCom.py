import inspect

__author__ = 'Philipp'


# noinspection PyClassHasNoInit
class AUT_POSMODE():
    """

    """
    AUT_NORMAL = 0,
    """fast positioning mode"""
    AUT_PRECISE = 1
    """exact positioning mode"""
    AUT_Fast = 2
    """for TS30 / TM30 instruments, positions with
    the last valid inclination and an increased
    positioning tolerance."""


class AUT_ATRMODE:
    """ Possible modes of the target recognition"""

    AUT_POSITION = 0
    """ Positioning to the hz- and v-angle"""
    AUT_TARGET = 1
    """Positioning to a target in the environment of the hz- and v-angle."""


class TMC_MEASURE_PRG:
    TMC_STOP = 0  # Stop measurement program
    TMC_DEF_DIST = 1  # Default DIST-measurement program
    TMC_CLEAR = 3  # TMC_STOP and clear data
    TMC_SIGNAL = 4  # Signal measurement (test function)
    TMC_DO_MEASURE = 6  # (Re)start measurement task
    TMC_RTRK_DIST = 8  # Distance-TRK measurement program
    TMC_RED_TRK_DIST = 10  # Reflectorless tracking
    TMC_FREQUENCY = 11  # Frequency measurement (test)


class TMC_INCLINE_PRG:
    TMC_MEA_INC = 0  # Use sensor (apriori sigma)
    TMC_AUTO_INC = 1  # Automatic mode (sensor/plane)
    TMC_PLANE_INC = 2  # Use plane (apriori sigma)


class AUT_ADJMODE:
    AUT_NORM_MODE = 0  # // Angle tolerance
    AUT_POINT_MODE = 1  # // Point tolerance
    AUT_DEFINE_MODE = 2  # System independent positioning
    # // tolerance. Set with AUT_SetTol


class EDM_MODE:
    EDM_MODE_NOT_USED = 0  # Init value
    EDM_SINGLE_TAPE = 1  # IR Standard Reflector Tape
    EDM_SINGLE_STANDARD = 2  # IR Standard
    EDM_SINGLE_FAST = 3  # IR Fast
    EDM_SINGLE_LRANGE = 4  # LO Standard
    EDM_SINGLE_SRANGE = 5  # RL Standard
    EDM_CONT_STANDARD = 6  # Standard repeated measurement
    EDM_CONT_DYNAMIC = 7  # IR Tacking
    EDM_CONT_REFLESS = 8  # RL Tracking
    EDM_CONT_FAST = 9  # Fast repeated measurement
    EDM_AVERAGE_IR = 10  # IR Average
    EDM_AVERAGE_SR = 11  # RL Average
    EDM_AVERAGE_LR = 12  # LO Averag
    EDM_PRECISE_IR = 13  # IR Precise (TS30, TM30)
    EDM_PRECISE_TAPE = 14  # IR Precise Reflector Tape (TS30, TM30)


class BAP_PRISMTYPE:
    BAP_PRISM_ROUND = 0  # Leica Circular Prism
    BAP_PRISM_MINI = 1  # Leica Mini Prism
    BAP_PRISM_TAPE = 2  # Leica Reflector Tape
    BAP_PRISM_360 = 3  # Leica 360o Prism
    BAP_PRISM_USER1 = 4
    BAP_PRISM_USER2 = 5
    BAP_PRISM_USER3 = 6
    BAP_PRISM_360_MINI = 7  # Leica Mini 360o Prism
    BAP_PRISM_MINI_ZERO = 8  # Leica Mini Zero Prism
    BAP_PRISM_USER = 9  # User Defined Prism
    BAP_PRISM_NDS_TAPE = 10  # Leica HDS Target


class CAM_ID_TYPE:
    CAM_ID_OCV = 0  # Overview Camera
    CAM_ID_OAC = 1  # Telescope Camera


class CAM_ZOOM_FACTOR_TYPE:
    CAM_ZOOM_1X = 1  # Zoo0ming disabled
    CAM_ZOOM_2X = 2  # 200% (field of view is reduced to one fourth)
    CAM_ZOOM_4X = 4
    CAM_ZOOM_8X = 8


class CAM_RESOLUTION_TYPE:
    CAM_RES_2560x1920 = 0
    CAM_RES_2048x1536 = 1
    CAM_RES_1600x1200 = 2
    CAM_RES_1280x960 = 3
    CAM_RES_640x480 = 4
    CAM_RES_320x240 = 5


class CAM_COMPRESSION_TYPE:
    CAM_COMP_JPEG = 0
    CAM_COMP_RAW = 1


class CAM_WHITE_BALANCE_TYPE:
    CAM_WB_AUTO = 0
    CAM_WB_INDOOR = 1
    CAM_WB_OUTDOOR = 2


class CAM_JPEG_COMPR_QUALITY_TYPE:
    CAM_JPEQ_STANDARD = 0
    CAM_JPEQ_BEST = 1
    CAM_JPEQ_IGNORE = 2



    @classmethod
    def getPrismaDict(cls):
        # print cls.__dict__
        retDict = {}
        clsAttributes = inspect.getmembers(cls)
        for a, b in clsAttributes:

            if a[0:3] == 'BAP':
                retDict[b] = a
        return retDict

        # print (BAP_PRISMTYPE.__dict__.values())
        # print getattr(BAP_PRISMTYPE,"BAP_PRISM_360")


class LeicaGeoCom():
    FTR_DEVICE_INTERNAL = 0
    FTR_DEVICE_PCPARD = 1
    FTR_DEVICE_SDCARD = 4
    FTR_DEVICE_USB_MEMORY = 5
    FTR_DEVICE_VOLATILERAM = 6


    @classmethod
    def convertResponse(cls, responseString, resArray):
        """

        @rtype : dict
        """
        responseString = responseString.replace("\r", '')
        responseString = responseString.replace("\n", '')

        resArg = responseString.partition(':')[2].split(',')
        returnDict = {}
        for i, keys in enumerate(resArray):
            returnDict[keys] = cls.num(resArg[i])
        return returnDict

    @classmethod
    def num(cls, s):
        try:
            return int(s)
        except ValueError:
            return float(s)

    returnCodes = {
        0: ['GRC_OK', 'Function successfully completed.'],
        1: ['GRC_UNDEFINED', 'Unknown error, result unspecified.'],
        2: ['GRC_IVPARAM', 'Invalid parameter detected. Result unspecified.'],
        3: ['GRC_IVRESULT', 'Invalid result.'],
        4: ['GRC_FATAL', 'Fatal error.'],
        5: ['GRC_NOT_IMPL', 'Not implemented yet.'],
        6: ['GRC_TIME_OUT', 'Function execution timed out. Result unspecified.'],
        7: ['GRC_SET_INCOMPL', 'Parameter setup for subsystem is incomplete.'],
        8: ['GRC_ABORT', 'Function execution has been aborted.'],
        9: ['GRC_NOMEMORY', 'Fatal error - not enough memory.'],
        10: ['GRC_NOTINIT', 'Fatal error - subsystem not initialized.'],
        12: ['GRC_SHUT_DOWN', 'Subsystem is down.'],
        13: ['GRC_SYSBUSY', 'System busy/already in use of another process. Cannot execute function.'],
        14: ['GRC_HWFAILURE', 'Fatal error - hardware failure.'],
        15: ['GRC_ABORT_APPL', 'Execution of application has been aborted'],
        16: ['GRC_LOW_POWER', 'Operation aborted - insufficient power supply level.'],
        17: ['GRC_IVVERSION', 'Invalid version of file, ...'],
        18: ['GRC_BATT_EMPTY', 'Battery empty'],
        20: ['GRC_NO_EVENT', 'no event pending.'],
        21: ['GRC_OUT_OF_TEMP', 'out of temperature range'],
        22: ['GRC_INSTRUMENT_TILT', 'instrument tilting out of range'],
        23: ['GRC_COM_SETTING', 'communication error'],
        24: ['GRC_NO_ACTION', 'GRC_TYPE Input "do no action"'],
        25: ['GRC_SLEEP_MODE', 'Instr. run into the sleep mode'],
        26: ['GRC_NOTOK', 'Function not successfully completed.'],
        27: ['GRC_NA', 'Not available'],
        28: ['GRC_OVERFLOW', 'Overflow error'],
        29: ['GRC_STOPPED', 'System or subsystem has been stopped'],
        257: ['GRC_ANG_ERROR', 'Angles and Inclinations not valid'],
        258: ['GRC_ANG_INCL_ERROR', 'inclinations not valid'],
        259: ['GRC_ANG_BAD_ACC', 'value accuracies not reached'],
        260: ['GRC_ANG_BAD_ANGLE_ACC', 'angle-accuracies not reached'],
        261: ['GRC_ANG_BAD_INCLIN_ACC', 'inclination accuracies not reached'],
        266: ['GRC_ANG_WRITE_PROTECTED', 'no write access allowed'],
        267: ['GRC_ANG_OUT_OF_RANGE', 'value out of range'],
        268: ['GRC_ANG_IR_OCCURED', 'function aborted due to interrupt'],
        269: ['GRC_ANG_HZ_MOVED', 'hz moved during incline measurement'],
        270: ['GRC_ANG_OS_ERROR', 'troubles with operation system'],
        271: ['GRC_ANG_DATA_ERROR', 'overflow at parameter values'],
        272: ['GRC_ANG_PEAK_CNT_UFL', 'too less peaks'],
        273: ['GRC_ANG_TIME_OUT', 'reading timeout'],
        274: ['GRC_ANG_TOO_MANY_EXPOS', 'too many exposures wanted'],
        275: ['GRC_ANG_PIX_CTRL_ERR', 'picture height out of range'],
        276: ['GRC_ANG_MAX_POS_SKIP', 'positive exposure dynamic overflow'],
        277: ['GRC_ANG_MAX_NEG_SKIP', 'negative exposure dynamic overflow'],
        278: ['GRC_ANG_EXP_LIMIT', 'exposure time overflow'],
        279: ['GRC_ANG_UNDER_EXPOSURE', 'picture underexposured'],
        280: ['GRC_ANG_OVER_EXPOSURE', 'picture overexposured'],
        300: ['GRC_ANG_TMANY_PEAKS', 'too many peaks detected'],
        301: ['GRC_ANG_TLESS_PEAKS', 'too less peaks detected'],
        302: ['GRC_ANG_PEAK_TOO_SLIM', 'peak too slim'],
        303: ['GRC_ANG_PEAK_TOO_WIDE', 'peak to wide'],
        304: ['GRC_ANG_BAD_PEAKDIFF', 'bad peak difference'],
        305: ['GRC_ANG_UNDER_EXP_PICT', 'too less peak amplitude'],
        306: ['GRC_ANG_PEAKS_INHOMOGEN', 'inhomogeneous peak amplitudes'],
        307: ['GRC_ANG_NO_DECOD_POSS', 'no peak decoding possible'],
        308: ['GRC_ANG_UNSTABLE_DECOD', 'peak decoding not stable'],
        309: ['GRC_ANG_TLESS_FPEAKS', 'too less valid finepeaks'],
        512: ['GRC_ATA_NOT_READY', 'ATR-System is not ready.'],
        513: ['GRC_ATA_NO_RESULT', 'Result isn\'t available yet.'],
        514: ['GRC_ATA_SEVERAL_TARGETS', 'Several Targets detected.'],
        515: ['GRC_ATA_BIG_SPOT', 'Spot is too big for analyse.'],
        516: ['GRC_ATA_BACKGROUND', 'Background is too bright.'],
        517: ['GRC_ATA_NO_TARGETS', 'No targets detected.'],
        518: ['GRC_ATA_NOT_ACCURAT', 'Accuracy worse than asked for.'],
        519: ['GRC_ATA_SPOT_ON_EDGE', 'Spot is on the edge of the sensing area.'],
        522: ['GRC_ATA_BLOOMING', 'Blooming or spot on edge detected.'],
        523: ['GRC_ATA_NOT_BUSY', 'ATR isn\'t in a continuous mode.'],
        524: ['GRC_ATA_STRANGE_LIGHT', 'Not the spot of the own target illuminator.'],
        525: ['GRC_ATA_V24_FAIL', 'Communication error to sensor (ATR).'],
        526: ['GRC_ATA_DECODE_ERROR', 'Received Arguments cannot be decoded'],
        527: ['GRC_ATA_HZ_FAIL', 'No Spot detected in Hz-direction.'],
        528: ['GRC_ATA_V_FAIL', 'No Spot detected in V-direction.'],
        529: ['GRC_ATA_HZ_STRANGE_L', 'Strange light in Hz-direction.'],
        530: ['GRC_ATA_V_STRANGE_L', 'Strange light in V-direction.'],
        531: ['GRC_ATA_SLDR_TRANSFER_PENDING', 'On multiple ATA_SLDR_OpenTransfer.'],
        532: ['GRC_ATA_SLDR_TRANSFER_ILLEGAL', 'No ATA_SLDR_OpenTransfer happened.'],
        533: ['GRC_ATA_SLDR_DATA_ERROR', 'Unexpected data format received.'],
        534: ['GRC_ATA_SLDR_CHK_SUM_ERROR', 'Checksum error in transmitted data.'],
        535: ['GRC_ATA_SLDR_ADDRESS_ERROR', 'Address out of valid range.'],
        536: ['GRC_ATA_SLDR_INV_LOADFILE', 'Firmware file has invalid format.'],
        537: ['GRC_ATA_SLDR_UNSUPPORTED', 'Current (loaded) firmware doesn\'t support upload.'],
        538: ['GRC_ATA_PS_NOT_READY', 'PS-System is not ready.'],
        539: ['GRC_ATA_ATR_SYSTEM_ERR', 'ATR system error'],
        769: ['GRC_EDM_SYSTEM_ERR',
              'Fatal EDM sensor error. See for the exact reason the original EDM sensor error number.In the most cases a service problem.'],
        770: ['GRC_EDM_INVALID_COMMAND', 'Invalid command or unknown command, see command syntax. '],
        771: ['GRC_EDM_BOOM_ERR', 'Boomerang error.'],
        772: ['GRC_EDM_SIGN_LOW_ERR',
              'Received signal to low, prism to far away, or natural barrier, bad environment, etc.'],
        773: ['GRC_EDM_DIL_ERR', 'obsolete'],
        774: ['GRC_EDM_SIGN_HIGH_ERR', 'Received signal to strong, prism  to near, stranger light effect.'],
        775: ['GRC_EDM_TIMEOUT', 'Timeout, measuring time exceeded (signal too weak, beam interrupted,..)'],
        776: ['GRC_EDM_FLUKT_ERR', 'to much turbulences or distractions'],
        777: ['GRC_EDM_FMOT_ERR', 'filter motordefective'],
        778: ['GRC_EDM_DEV_NOT_INSTALLED', 'Device like EGL, DL is not installed.'],
        779: ['GRC_EDM_NOT_FOUND',
              'Search result invalid. For the exact explanation, see in the description of the called function.'],
        780: ['GRC_EDM_ERROR_RECEIVED', 'Communication ok, but an error reported fromt he EDM sensor.'],
        781: ['GRC_EDM_MISSING_SRVPWD', 'No service password is set.'],
        782: ['GRC_EDM_INVALID_ANSWER', 'Communication ok, but an unexpected answer received.'],
        783: ['GRC_EDM_SEND_ERR', 'Data send error, sending buffer is full.'],
        784: ['GRC_EDM_RECEIVE_ERR', 'Data receive error, like parity buffer overflow.'],
        785: ['GRC_EDM_INTERNAL_ERR', 'Internal EDM subsystem error.'],
        786: ['GRC_EDM_BUSY', 'Sensor is working already, abort current measuring first.'],
        787: ['GRC_EDM_NO_MEASACTIVITY', 'No measurement activity started.'],
        788: ['GRC_EDM_CHKSUM_ERR',
              'Calculated checksum, resp. received data wrong (only in binary communication mode possible).'],
        789: ['GRC_EDM_INIT_OR_STOP_ERR',
              'During start up or shut down phase an error occured. It is saved in the DEL buffer.'],
        790: ['GRC_EDM_SRL_NOT_AVAILABLE', 'Red laser not available on this sensor HW.'],
        791: ['GRC_EDM_MEAS_ABORTED', 'Measurement will be aborted (will be used for the laser security)'],
        798: ['GRC_EDM_SLDR_TRANSFER_PENDING', 'Multiple OpenTransfer calls.'],
        799: ['GRC_EDM_SLDR_TRANSFER_ILLEGAL', 'No open transfer happened.'],
        800: ['GRC_EDM_SLDR_DATA_ERROR', 'Unexpected data format received.'],
        801: ['GRC_EDM_SLDR_CHK_SUM_ERROR', 'Checksum error in transmitted data.'],
        802: ['GRC_EDM_SLDR_ADDR_ERROR', 'Address out of valid range.'],
        803: ['GRC_EDM_SLDR_INV_LOADFILE', 'Firmware file has invalid format.'],
        804: ['GRC_EDM_SLDR_UNSUPPORTED', 'Current (loaded) firmware doesn\'t support upload.'],
        808: ['GRC_EDM_UNKNOW_ERR', 'Undocumented error from the EDM sensor, should not occur.'],
        818: ['GRC_EDM_DISTRANGE_ERR', 'Out of distance range (dist too small or large)'],
        819: ['GRC_EDM_SIGNTONOISE_ERR', 'Signal to noise ratio too small'],
        820: ['GRC_EDM_NOISEHIGH_ERR', 'Noise to high'],
        821: ['GRC_EDM_PWD_NOTSET', 'Password is not set'],
        822: ['GRC_EDM_ACTION_NO_MORE_VALID',
              'Elapsed time between prepare und start fast measurement for ATR to long'],
        823: ['GRC_EDM_MULTRG_ERR', 'Possibly more than one target (also a sensor  	error) '],
        1283: ['GRC_TMC_NO_FULL_CORRECTION', 'Warning: measurement without full correction'],
        1284: ['GRC_TMC_ACCURACY_GUARANTEE', 'Info: accuracy can not be guarantee'],
        1285: ['GRC_TMC_ANGLE_OK', 'Warning: only angle measurement valid'],
        1288: ['GRC_TMC_ANGLE_NOT_FULL_CORR',
               'Warning: only angle measurement valid but without full correction'],
        1289: ['GRC_TMC_ANGLE_NO_ACC_GUARANTY',
               'Info: only angle measurement valid but accuracy can not be guarantee '],
        1290: ['GRC_TMC_ANGLE_ERROR', 'Error: no angle measurement'],
        1291: ['GRC_TMC_DIST_PPM', 'Error: wrong setting of PPM or MM on EDM'],
        1292: ['GRC_TMC_DIST_ERROR', 'Error: distance measurement not done (no aim, etc.)'],
        1293: ['GRC_TMC_BUSY', 'Error: system is busy (no measurement done)'],
        1294: ['GRC_TMC_SIGNAL_ERROR', 'Error: no signal on EDM (only in signal mode)'],
        2305: ['GRC_BMM_XFER_PENDING', 'Loading process already opened'],
        2306: ['GRC_BMM_NO_XFER_OPEN', 'Transfer not opened'],
        2307: ['GRC_BMM_UNKNOWN_CHARSET', 'Unknown character set'],
        2308: ['GRC_BMM_NOT_INSTALLED', 'Display module not present'],
        2309: ['GRC_BMM_ALREADY_EXIST', 'Character set already exists'],
        2310: ['GRC_BMM_CANT_DELETE', 'Character set cannot be deleted'],
        2311: ['GRC_BMM_MEM_ERROR', 'Memory cannot be allocated'],
        2312: ['GRC_BMM_CHARSET_USED', 'Character set still used'],
        2313: ['GRC_BMM_CHARSET_SAVED', 'Charset cannot be deleted or is protected'],
        2314: ['GRC_BMM_INVALID_ADR', 'Attempt to copy a character block outside the allocated memory'],
        2315: ['GRC_BMM_CANCELANDADR_ERROR', 'Error during release of allocated memory'],
        2316: ['GRC_BMM_INVALID_SIZE', 'Number of bytes specified in header does not match the bytes read'],
        2317: ['GRC_BMM_CANCELANDINVSIZE_ERROR', 'Allocated memory could not be released'],
        2318: ['GRC_BMM_ALL_GROUP_OCC', 'Max. number of character sets already loaded'],
        2319: ['GRC_BMM_CANT_DEL_LAYERS', 'Layer cannot be deleted'],
        2320: ['GRC_BMM_UNKNOWN_LAYER', 'Required layer does not exist'],
        2321: ['GRC_BMM_INVALID_LAYERLEN', 'Layer length exceeds maximum'],
        3072: ['GRC_COM_ERO', 'Initiate Extended Runtime Operation (ERO).'],
        3073: ['GRC_COM_CANT_ENCODE', 'Cannot encode arguments in client.'],
        3074: ['GRC_COM_CANT_DECODE', 'Cannot decode results in client.'],
        3075: ['GRC_COM_CANT_SEND', 'Hardware error while sending.'],
        3076: ['GRC_COM_CANT_RECV', 'Hardware error while receiving.'],
        3077: ['GRC_COM_TIMEDOUT', 'Request timed out.'],
        3078: ['GRC_COM_WRONG_FORMAT', 'Packet format error.'],
        3079: ['GRC_COM_VER_MISMATCH', 'Version mismatch between client and server.'],
        3080: ['GRC_COM_CANT_DECODE_REQ', 'Cannot decode arguments in server.'],
        3081: ['GRC_COM_PROC_UNAVAIL', 'Unknown RPC, procedure ID invalid.'],
        3082: ['GRC_COM_CANT_ENCODE_REP', 'Cannot encode results in server.'],
        3083: ['GRC_COM_SYSTEM_ERR', 'Unspecified generic system error.'],
        3085: ['GRC_COM_FAILED', 'Unspecified error.'],
        3086: ['GRC_COM_NO_BINARY', 'Binary protocol not available.'],
        3087: ['GRC_COM_INTR', 'Call interrupted.'],
        3090: ['GRC_COM_REQUIRES_8DBITS', 'Protocol needs 8bit encoded characters.'],
        3093: ['GRC_COM_TR_ID_MISMATCH', 'TRANSACTIONS ID mismatch error.'],
        3094: ['GRC_COM_NOT_GEOCOM', 'Protocol not recognizable.'],
        3095: ['GRC_COM_UNKNOWN_PORT', '(WIN) Invalid port address.'],
        3099: ['GRC_COM_ERO_END', 'ERO is terminating.'],
        3100: ['GRC_COM_OVERRUN', 'Internal error: data buffer overflow.'],
        3101: ['GRC_COM_SRVR_RX_CHECKSUM_ERRR', 'Invalid checksum on server side received.'],
        3102: ['GRC_COM_CLNT_RX_CHECKSUM_ERRR', 'Invalid checksum on client side received.'],
        3103: ['GRC_COM_PORT_NOT_AVAILABLE', '(WIN) Port not available.'],
        3104: ['GRC_COM_PORT_NOT_OPEN', '(WIN) Port not opened.'],
        3105: ['GRC_COM_NO_PARTNER', '(WIN) Unable to find TPS.'],
        3106: ['GRC_COM_ERO_NOT_STARTED', 'Extended Runtime Operation could not be started.'],
        3107: ['GRC_COM_CONS_REQ', 'Att to send cons reqs'],
        3108: ['GRC_COM_SRVR_IS_SLEEPING', 'TPS has gone to sleep. Wait and try again.'],
        3109: ['GRC_COM_SRVR_IS_OFF', 'TPS has shut down. Wait and try again.'],
        8704: ['GRC_AUT_TIMEOUT', 'Position not reached'],
        8705: ['GRC_AUT_DETENT_ERROR', 'Positioning not possible due to mounted EDM'],
        8706: ['GRC_AUT_ANGLE_ERROR', 'Angle measurement error'],
        8707: ['GRC_AUT_MOTOR_ERROR', 'Motorisation error'],
        8708: ['GRC_AUT_INCACC', 'Position not exactly reached'],
        8709: ['GRC_AUT_DEV_ERROR', 'Deviation measurement error'],
        8710: ['GRC_AUT_NO_TARGET', 'No target detected'],
        8711: ['GRC_AUT_MULTIPLE_TARGETS', 'Multiple target detected'],
        8712: ['GRC_AUT_BAD_ENVIRONMENT', 'Bad environment conditions'],
        8713: ['GRC_AUT_DETECTOR_ERROR', 'Error in target acquisition'],
        8714: ['GRC_AUT_NOT_ENABLED', 'Target acquisition not enabled'],
        8715: ['GRC_AUT_CALACC', 'ATR-Calibration failed'],
        8716: ['GRC_AUT_ACCURACY', 'Target position not exactly reached'],
        8717: ['GRC_AUT_DIST_STARTED', 'Info: dist. measurement has been started'],
        8718: ['GRC_AUT_SUPPLY_TOO_HIGH', 'external Supply voltage is too high'],
        8719: ['GRC_AUT_SUPPLY_TOO_LOW', 'int. or ext. Supply voltage is too low'],
        8720: ['GRC_AUT_NO_WORKING_AREA', 'working area not set'],
        8721: ['GRC_AUT_ARRAY_FULL', 'power search data array is filled'],
        8722: ['GRC_AUT_NO_DATA', 'no data available'],
        12544: ['GRC_KDM_NOT_AVAILABLE', 'KDM device is not available.'],
        13056: ['GRC_FTR_FILEACCESS', 'File access error'],
        13057: ['GRC_FTR_WRONGFILEBLOCKNUMBER', 'block number was not the expected one'],
        13058: ['GRC_FTR_NOTENOUGHSPACE', 'not enough space on device to proceed uploading'],
        13059: ['GRC_FTR_INVALIDINPUT', 'Rename of file failed.'],
        13060: ['GRC_FTR_MISSINGSETUP', 'invalid parameter as input']
    }

    @classmethod
    def COM_GetDoublePrecision(cls):
        request = "%R1Q,108:\r"
        responseArray = ["RC", " nDigits"]

        return request, responseArray

    @classmethod
    def COM_SetDoublePrecision(cls, nDigits):
        request = "%R1Q,107:" + str(nDigits) + "\r"
        responseArray = ["RC"]

        return request, responseArray

    @classmethod
    def AUS_GetUserAtrState(cls):
        request = "%R1Q,18006:\r"
        responseArray = ["RC", "OnOff "]
        return request, responseArray

    @classmethod
    def AUS_SetUserAtrState(cls, OnOff):
        request = "%R1Q,18005:" + str(OnOff) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUS_GetUserLockState(cls):
        request = "%R1Q,18008:+\r"
        responseArray = ["RC", " OnOff "]
        return request, responseArray

    @classmethod
    def AUS_SetUserLockState(cls, OnOff):
        request = "%R1Q,18007:" + str(OnOff) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_ReadTol(cls):
        request = "%R1Q,9008:+\r"
        responseArray = ["RC", "Tolerance Hz", "Tolerance V "]
        return request, responseArray

    @classmethod
    def AUT_SetTol(cls, TolPar):
        request = "%R1Q,9007:" + str(TolPar) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_ReadTimeout(cls):
        request = "%R1Q,9012:+\r"
        responseArray = ["RC", " TimeoutHz", " TimeoutV "]
        return request, responseArray

    @classmethod
    def AUT_SetTimeout(cls, TimeoutPar):
        request = "%R1Q,9011:" + str(TimeoutPar) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_MakePositioning(cls, Hz, V, POSMode, ATRMode, bDummy=0):
        request = "%R1Q,9027:" + str(Hz) + "," + str(V) + "," + str(POSMode) + "," + str(ATRMode) + "," + str(
            bDummy) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_ChangeFace(cls, PosMode, ATRMode, bDummy=0):
        request = "%R1Q,9028:" + str(PosMode) + "," + str(ATRMode) + "," + str(bDummy) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_FineAdjust(cls, dSrchHz, dSrchV, bDummy=0):
        request = "%R1Q,9037:" + str(dSrchHz) + "," + str(dSrchV) + "," + str(bDummy) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_Search(cls, Hz_Area, V_Area, bDummy=0):
        request = "%R1Q,9029:" + str(Hz_Area) + "," + str(V_Area) + "," + str(bDummy) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_GetFineAdjustMode(cls, rAdjMode):
        request = "%R1Q,9030:" + str(rAdjMode) + "\r"
        responseArray = ["RC", "AdjMode "]
        return request, responseArray

    @classmethod
    def AUT_SetFineAdjustMode(cls, AdjMode):

        """


        @type AdjMode: object
        @param AdjMode: 
        @return: 
        """
        request = "%R1Q,9031:" + str(AdjMode) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_LockIn(cls):
        request = "%R1Q,9013:+\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_GetSearchArea(cls):
        request = "%R1Q,9042:+\r"
        responseArray = ["RC", " dCenterHz ", " dCenterV ", " dRangeHz ", " dRangeV ", " bEnabled  "]
        return request, responseArray

    @classmethod
    def AUT_SetSearchArea(cls, Area):
        request = "%R1Q,9043:" + str(Area) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_GetUserSpiral(cls):
        request = "%R1Q,9040:+\r"
        responseArray = ["RC", "dRangeHz", "dRangeV"]
        return request, responseArray

    @classmethod
    def AUT_SetUserSpiral(cls, SpiralDim):
        request = "%R1Q,9041:" + str(SpiralDim) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_PS_EnableRange(cls, bEnable):
        request = "%R1Q,9048:" + str(bEnable) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_PS_SetRange(cls, lMinDist, lMaxDist):
        request = "%R1Q,9047:" + str(lMinDist) + "," + str(lMaxDist) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_PS_SearchWindow(cls):
        request = "%R1Q,9052:+\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def AUT_PS_SearchNext(cls, lDirection, bSwing):
        request = "%R1Q,9051:" + str(lDirection) + "," + str(bSwing) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def BAP_GetTargetType(cls):
        request = "%R1Q,17022:+\r"
        responseArray = ["RC", " eTargetType"]
        return request, responseArray

    @classmethod
    def BAP_SetTargetType(cls, eTargetType):
        request = "%R1Q,17021:" + str(eTargetType) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def BAP_GetPrismType(cls):
        request = "%R1Q,17009:+\r"
        responseArray = ["RC", "ePrismType"]
        return request, responseArray

    @classmethod
    def BAP_SetPrismType(cls, ePrismType):
        request = "%R1Q,17008:" + str(ePrismType) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def BAP_GetPrismType2(cls):
        request = "%R1Q,17031:" + "\r"
        responseArray = ["RC", "ePrismType", "szPrismName"]
        return request, responseArray

    @classmethod
    def BAP_SetPrismType2(cls, ePrismType, szPrismName):
        request = "%R1Q,17030:" + str(ePrismType) + "," + str(szPrismName) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def BAP_GetPrism(cls):
        request = "%R1Q,17023:" + ",+\r"
        responseArray = ["RC", " Name", " dAddConst", " eReflType"]
        return request, responseArray

    @classmethod
    def BAP_GetUserPrism(cls):
        request = "%R1Q,17033:" + "\r"
        responseArray = ["RC", " rdAddConst", " reReflType", " szCreator"]
        return request, responseArray

    @classmethod
    def BAP_SetUserPrism(cls, szPrismName, dAddConst, eReflType, szCreator):
        request = "%R1Q,17032:" + str(szPrismName) + "," + str(dAddConst) + "," + str(eReflType) + "," + str(
            szCreator) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def BAP_GetMeasPrg(cls):
        request = "%R1Q,17018:+\r"
        responseArray = ["RC", " eMeasPrg"]
        return request, responseArray

    @classmethod
    def BAP_SetMeasPrg(cls, eMeasPrg):
        request = "%R1Q,17019:" + str(eMeasPrg) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def BAP_MeasDistanceAngle(cls, DistMode):
        request = "%R1Q,17017:", +DistMode + "\r"
        responseArray = ["RC", " dHz", " dV", " dDist", "DistMode"]
        return request, responseArray

    @classmethod
    def BAP_GetATRSetting(cls):
        request = "%R1Q,17034:+\r"
        responseArray = ["RC", " reATRSetting"]
        return request, responseArray

    @classmethod
    def BAP_SetATRSetting(cls, eATRSetting):
        request = "%R1Q,17035:" + str(eATRSetting) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def BAP_GetRedATRFov(cls):
        request = "%R1Q,17036:+\r"
        responseArray = ["RC", " reRedFov"]
        return request, responseArray

    @classmethod
    def BAP_SetRedATRFov(cls, eRedFov):
        request = "%R1Q,17037:" + str(eRedFov) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def BMM_BeepAlarm(cls):
        request = "%R1Q,11004:" + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def BMM_BeepNormal(cls):
        request = "%R1Q,11003:" + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def IOS_BeepOn(cls, nIntens):
        request = "%R1Q,20001:" + str(nIntens) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def IOS_BeepOff(cls):
        request = "%R1Q,20000:" + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def COM_GetSWVersion(cls):
        request = "%R1Q,110:" + "\r"
        responseArray = ["RC", " nRel", " nVer", " nSubVer"]
        return request, responseArray

    @classmethod
    def COM_SwitchOnTPS(cls, eOnMode):
        request = "%R1Q,111:" + str(eOnMode) + "\r"
        responseArray = ["5elseNothing"]
        return request, responseArray

    @classmethod
    def COM_SwitchOffTPS(cls, eOffMode):
        request = "%R1Q,112:" + str(eOffMode) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def COM_NullProc(cls):
        request = "%R1Q,0:" + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def COM_GetBinaryAvailable(cls):
        request = "%R1Q,113:+\r"
        responseArray = ["RC", " bAvailable"]
        return request, responseArray

    @classmethod
    def COM_SetBinaryAvailable(cls, bAvailable):
        request = "%R1Q,114:" + str(bAvailable) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def CSV_GetInstrumentNo(cls):
        request = "%R1Q,5003:+\r"
        responseArray = ["RC", " SerialNo"]
        return request, responseArray

    @classmethod
    def CSV_GetInstrumentName(cls):
        request = "%R1Q,5004:" + "\r"
        responseArray = ["RC", "Name"]
        return request, responseArray

    @classmethod
    def CSV_GetDeviceConfig(cls):
        request = "%R1Q,5035:+\r"
        responseArray = ["RC", ""]
        return request, responseArray

    @classmethod
    def CSV_GetReflectorlessClass(cls):
        request = "%R1Q,5100:+\r"
        responseArray = ["RC", "reRefLessClass"]
        return request, responseArray

    @classmethod
    def CSV_GetDateTime(cls):
        request = "%R1Q,5008:+\r"
        responseArray = ["RC", "Year", "Month", "Day", "Hour", "Minute", "Second"]
        return request, responseArray

    @classmethod
    def CSV_SetDateTime(cls, DateAndTime):
        request = "%R1Q,5007:" + str(DateAndTime) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def CSV_GetSWVersion2(cls):
        request = "%R1Q,5034:" + "\r"
        responseArray = ["RC", "nRelease", "nVersion", "nSubVersion"]
        return request, responseArray

    @classmethod
    def CSV_CheckPower(cls):
        request = "%R1Q,5039\r"
        responseArray = ["RC", " unCapacity", "eActivePower", "ePowerSuggest"]
        return request, responseArray

    @classmethod
    def CSV_GetIntTemp(cls):
        request = "%R1Q,5011:+\r"
        responseArray = ["RC", "Temp"]
        return request, responseArray

    @classmethod
    def CSV_GetDateTimeCentiSec(cls):
        request = "%R1Q,5117:" + "\r"
        responseArray = ["RC", "Year", "Month", "Day", "Hour", "Minute", "Second", "CentiSecond"]
        return request, responseArray

    @classmethod
    def EDM_Laserpointer(cls, eLaser):
        request = "%R1Q,1004:" + str(eLaser) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def EDM_GetEglIntensity(cls):
        request = "%R1Q,1058:" + "\r"
        responseArray = ["RC", "eIntensity"]
        return request, responseArray

    @classmethod
    def EDM_SetEglIntensity(cls, eIntensity):
        request = "%R1Q,1059:" + str(eIntensity) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def IMG_GetTccConfig(cls, eMemType, ):
        request = "%R1Q,23400:" + str(eMemType) + ",+\r"
        responseArray = ["RC", "ulImageNumber", "ulQuality", "ulSubFunctNumber", "szFileNamePrefix"]
        return request, responseArray

    @classmethod
    def IMG_SetTccConfig(cls, eMemType, Parameters):
        request = "%R1Q,23401:" + str(eMemType) + "," + str(Parameters) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def IMG_TakeTccImage(cls, eMemType, runImageNumber):
        request = "%R1Q,23402:" + str(eMemType) + "," + str(runImageNumber) + "\r"
        responseArray = ["RC", "runImageNumber"]
        return request, responseArray

    @classmethod
    def IMG_SetTCCExposureTime(cls, unExposureTime):
        request = "%R1Q,23403:" + str(unExposureTime)+ "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def MOT_ReadLockStatus(cls):
        request = "%R1Q,6021:\r"
        responseArray = ["RC", "Status"]
        return request, responseArray

    @classmethod
    def MOT_StartController(cls, ControlMode):
        request = "%R1Q,6001:" + str(ControlMode) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def MOT_StopController(cls, Mode):
        request = "%R1Q,6002:" + str(Mode) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def MOT_SetVelocity(cls, HZ_Speed, V_Speed):
        request = "%R1Q,6004:" + str(HZ_Speed) + "," + str(V_Speed) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def SUP_GetConfig(cls):
        request = "%R1Q,14001:" + "\r"
        responseArray = ["RC", " Reserved", "AutoPower", "Timeout"]
        return request, responseArray

    @classmethod
    def SUP_SetConfig(cls, Reserved, AutoPower, Timeout):
        request = "%R1Q,14002:" + str(Reserved) + "," + str(AutoPower) + "," + str(Timeout) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetCoordinate(cls):
        request = "%R1Q,2082:" + "\r"
        responseArray = ["RC", "E", "N", "H", "CoordTime", "E-Cont", "N-Cont", "H-Cont", "CoordContTime"]
        return request, responseArray

    @classmethod
    def TMC_GetSimpleMea(cls, WaitTime, Mode):
        request = "%R1Q,2108:" + str(WaitTime) + "," + str(Mode) + "\r"
        responseArray = ["RC", "Hz", "V", "SlopeDistance"]
        return request, responseArray

    @classmethod
    def TMC_GetAngle1(cls, Mode):
        request = "%R1Q,2003:," + str(Mode) + "\r"
        responseArray = ["RC", "Hz", "V", "AngleAccuracy", "AngleTime", "CrossIncline", "LengthIncline",
                         " AccuracyIncline", "InclineTime", "FaceDef"]
        return request, responseArray

    @classmethod
    def TMC_GetAngle5(cls, Mode):
        request = "%R1Q,2107:," + str(Mode) + "\r"
        responseArray = ["RC", "Hz", "V"]
        return request, responseArray

    @classmethod
    def TMC_GetFullMeas(cls, WaitTime, Mode):
        request = "%R1Q,2167:" + str(WaitTime) + "," + str(Mode) + "\r"
        responseArray = ["RC", "Hz", "V", "AccAngle", "C", "L", "AccIncl", "SlopeDist", "DistTime"]
        return request, responseArray

    @classmethod
    def TMC_DoMeasure(cls, Command, Mode):
        request = "%R1Q,2008:" + str(Command) + "," + str(Mode) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_SetHandDist(cls, SlopeDistance, HgtOffset, Mode):
        request = "%R1Q,2019:" + str(SlopeDistance) + "," + str(HgtOffset) + "," + str(Mode) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetHeight(cls):
        request = "%R1Q,2011:+\r"
        responseArray = ["RC", "Height"]
        return request, responseArray

    @classmethod
    def TMC_SetHeight(cls, Height):
        request = "%R1Q,2012:" + str(Height) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetAtmCorr(cls):
        request = "%R1Q,2029:+\r"
        responseArray = ["RC", "Lambda", "Pressure", " DryTemperature", "WetTemperature"]
        return request, responseArray

    @classmethod
    def TMC_SetAtmCorr(cls, AtmTemperature):
        request = "%R1Q,2028:" + str(AtmTemperature) + "\r"
        responseArray = ["RC", " "]
        return request, responseArray

    @classmethod
    def TMC_SetOrientation(cls, HzOrientation):
        request = "%R1Q,2113:" + str(HzOrientation) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetPrismCorr(cls):
        request = "%R1Q,2023:+\r"
        responseArray = ["RC", "PrismCorr"]
        return request, responseArray

    @classmethod
    def TMC_GetRefractiveCorr(cls):
        request = "%R1Q,2031:+\r"
        responseArray = ["RC", "RefOn", "EarthRadius", " RefractiveScale"]
        return request, responseArray

    @classmethod
    def TMC_SetRefractiveCorr(cls, Refractive):
        request = "%R1Q,2030:" + str(Refractive) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetRefractiveMethod(cls):
        request = "%R1Q,2091:" + "\r"
        responseArray = ["RC", "Method"]
        return request, responseArray

    @classmethod
    def TMC_SetRefractiveMethod(cls, Method):
        request = "%R1Q,2090:" + str(Method) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetStation(cls):
        request = "%R1Q,2009:+\r"
        responseArray = ["RC", "E0", "N0", "H0", "Hi"]
        return request, responseArray

    @classmethod
    def TMC_SetStation(cls, Station):
        request = "%R1Q,2010:" + str(Station) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetAtmPpm(cls):
        request = "%R1Q,2151:+\r"
        responseArray = ["RC", " dPpmA"]
        return request, responseArray

    @classmethod
    def TMC_SetAtmPpm(cls, dPpmA):
        request = "%R1Q,2148:" + str(dPpmA) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetGeoPpm(cls):
        request = "%R1Q,2154:" + "\r"
        responseArray = ["RC", "unGeomUseAutomatic", "dScaleFactorCentralMeridian", "dOffsetCentralMeridian",
                         "dHeightReductionPPM", "dIndividualPPM"]
        return request, responseArray

    @classmethod
    def TMC_SetGeoPpm(cls, unGeomUseAutomatic, dScaleFactorCentralMeridian, dOffsetCentralMeridian, dHeightReductionPPM,
                      dIndividualPPM):
        request = "%R1Q,2153:" + str(unGeomUseAutomatic) + "," + str(dScaleFactorCentralMeridian) + "," + str(
            dOffsetCentralMeridian) + "," + str(dHeightReductionPPM) + "," + str(dIndividualPPM) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetFace(cls):
        request = "%R1Q,2026:+\r"
        responseArray = ["RC", "Face"]
        return request, responseArray

    @classmethod
    def TMC_GetSignal(cls):
        request = "%R1Q,2022:+\r"
        responseArray = ["RC", "SignalIntensity", "Time"]
        return request, responseArray

    @classmethod
    def TMC_GetAngSwitch(cls):
        request = "%R1Q,2014:+\r"
        responseArray = ["RC", "InclineCorr", "StandAxisCorr", " CollimationCorr", "TiltAxisCorr"]
        return request, responseArray

    @classmethod
    def TMC_GetInclineSwitch(cls):
        request = "%R1Q,2007:" + "\r"
        responseArray = ["RC", "SwCorr"]
        return request, responseArray

    @classmethod
    def TMC_SetInclineSwitch(cls, SwCorr):
        request = "%R1Q,2006:" + str(SwCorr) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetEdmMode(cls):
        request = "%R1Q,2021:+\r"
        responseArray = ["RC", "Mode"]
        return request, responseArray

    @classmethod
    def TMC_SetEdmMode(cls, Mode):
        request = "%R1Q,2020:" + str(Mode) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_IfDataAzeCorrError(cls):
        request = "%R1Q,2114:\r"
        responseArray = ["RC", "bAtrCorrectionError"]
        return request, responseArray

    @classmethod
    def TMC_IfDataIncCorrError(cls):
        request = "%R1Q,2115:\r"
        responseArray = ["RC", "bIncCorrectionError"]
        return request, responseArray

    @classmethod
    def TMC_SetAngSwitch(cls, InclineCorr, StandAxisCorr, CollimationCorr, TiltAxisCorr):
        request = "%R1Q,2016:" + str(InclineCorr) + "," + str(StandAxisCorr) + "," + str(CollimationCorr) + "," + str(
            TiltAxisCorr) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def TMC_GetSlopeDistCorr(cls):
        request = "%R1Q,2126:\r"
        responseArray = ["RC", "dPpmCorr", " dPrismCorr"]
        return request, responseArray

    @classmethod
    def CAM_SetZoomFactor(cls, CamID, ZoomFactor):
        request = "%R1Q,23608:" + str(CamID) + "," + str(ZoomFactor) + "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def CAM_GetZoomFactor(cls, CamID):
        request = "%R1Q,23609:" + str(CamID) + "\r"
        responseArray = ["RC", "rZoomFactor"]
        return request, responseArray

    @classmethod
    def CAM_GetCamPos(cls, CamID):
        request = "%R1Q,23611:" + str(CamID) + "\r"
        responseArray = ["RC", "dX", "dY", "dZ"]
        return request, responseArray
    @classmethod
    def CAM_GetCamViewingDir(cls, CamID, dSlopeDistance):
        request = "%R1Q,23613:" + str(CamID) + "," + str(dSlopeDistance) + "\r"
        responseArray = ["RC", "dCamDirE", "dCamDirN", "dCamDirH"]
        return request, responseArray
    @classmethod
    def CAM_GetCameraFoV(cls, CamID, eZoomFactor):
        request = "%R1Q,23619:" + str(CamID) + "," + str(eZoomFactor) + "\r"
        responseArray = ["RC", "rFoVHz", "rFoVV"]
        return request, responseArray
    @classmethod
    def CAM_GetCameraFoV(cls, CamID, szName, lNumber):
        request = "%R1Q,23622:" + str(CamID) + "," + str(szName) + "," + str(lNumber) + "\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_TakeImage(cls, CamID):
        request = "%R1Q,23623:" + str(CamID) + "\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_OVC_GetActCameraCenter(cls):
        request = "%R1Q,23624:" + "\r"
        responseArray = ["RC", "rdXCenter", "rdYCenter"]
        return request, responseArray
    @classmethod
    def CAM_OVC_SetActDistance(cls, dDist, bFace1):
        request = "%R1Q,23625:" + str(dDist) + "," + str(bFace1) + "\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_OVC_SetWhiteBalanceMode(cls, CamID, eWhiteBalanceMode):
        request = "%R1Q,23626:" + str(CamID) + "," + str(eWhiteBalanceMode) + "\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_IsCameraReady(cls, CamID):
        request = "%R1Q,23627:" + str(CamID) + "\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_SetCameraProperties(cls, CamID, CameraResolution, CamCompression, JpegComprQuality):
        request = "%R1Q,23633:" + str(CamID) + "," + str(CameraResolution) + "," + str(CamCompression) + "," + str(
            JpegComprQuality) + "\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_GetCameraPowerSwitch(cls, CamID):
        request = "%R1Q,23636:" + str(CamID) + "\r"
        responseArray = ["RC","reSwitch"]
        return request, responseArray
    @classmethod
    def CAM_SetCameraPowerSwitch(cls, CamID, eSwitch):
        request = "%R1Q,23637:" + str(CamID)+","+str(eSwitch) + "\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_WaitForCameraReady(cls, CamID, ulTimeout):
        request = "%R1Q,23638:" + str(CamID)+","+str(ulTimeout) + "\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_AF_SetMotorPosition(cls, lMotorPosition):
        request = "%R1Q,23645:" +str(lMotorPosition) + "\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_AF_GetMotorPosition(cls):
        request = "%R1Q,23644:\r"
        responseArray = ["RC","lMotorPosition"]
        return request, responseArray
    @classmethod
    def CAM_AF_ContinuousAutofocus(cls, bStart):
        request = "%R1Q,23669:" +str(bStart)+ "\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def CAM_AF_PositFocusMotorToDist(cls, dDistance):
        request = "%R1Q,23652:"+str(dDistance)+"\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def CAM_AF_PositFocusMotorToInfinity(cls):
        request = "%R1Q,23677:\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_AF_SingleShotAutofocus(cls):
        request = "%R1Q,23662:\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_AF_FocusContrastArroundCurrent(cls, nSteps):
        request = "%R1Q,23663:"+str(nSteps)+"\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_GetChipWindowSize(cls, CamID):
        request = "%R1Q,23668:"+str(CamID)+"\r"
        responseArray = ["RC", "rChipWindowSize"]
        return request, responseArray
    @classmethod
    def CAM_OAC_GetCrossHairPos(cls,):
        request = "%R1Q,23671:"+"\r"
        responseArray = ["RC", "roCrossHairPos"]
        return request, responseArray
    @classmethod
    def CAM_StartRemoteVideo(cls, eCamID, nFrameRate, nQuality):
        request = "%R1Q,23675:"+str(eCamID)+","+str(nFrameRate)+","+str(nQuality)+"\r"
        responseArray = ["RC"]
        return request, responseArray
    @classmethod
    def CAM_StopRemoteVideo(cls):
        request = "%R1Q,23676:"+"\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def FTR_SETUPLIST(cls,eDeviceType, eFileType, szSearchPath):
        request = "%R1Q,23306:"+str(eDeviceType)+","+str(eFileType)+","+str(szSearchPath)+"\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def FTR_List(cls,bNext):
        request = "%R1Q,23307:"+str(bNext)+"\r"
        responseArray = ["RC","rbLast","szFileName","ulFileSize","ucHour","ucMinute","ucSecond","ucCentisecond","ucDay","ucMonth","ucYear"]
        return request, responseArray

    @classmethod
    def FTR_AbortList(cls):
        request = "%R1Q,23308:"+"\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def FTR_SetupDownload(cls,eDeviceType,eFileType,szFileNameSrc,unBlockSize):
        request = "%R1Q,23303:"+str(eDeviceType)+","+str(eFileType)+","+str(szFileNameSrc)+","+str(unBlockSize)+"\r"
        responseArray = ["RC","unNumOfBlocks"]
        return request, responseArray

    @classmethod
    def FTR_SetupDownload(cls,unNumOfBlocks):
        request = "%R1Q,23304:"+str(unNumOfBlocks)+ "\r"
        responseArray = ["RC","FTR_BLOCK_val","FTR_BLOCK_len"]
        return request, responseArray

    @classmethod
    def FTR_AbortDownload(cls):
        request = "%R1Q,23305:"+"\r"
        responseArray = ["RC"]
        return request, responseArray

    @classmethod
    def FTR_Delete(cls, eDeviceType , eFileType, ucDay, ucMonth, ucYear, szFileName):

        request = "%R1Q,23309:"+str(eDeviceType)+","+str(eFileType)+","+str(ucDay)+","+str(ucMonth)+","+str(ucYear)+","+str(szFileName)+"\r"
        responseArray = ["RC","unNumFilesDeleted"]
        return request, responseArray

    @classmethod
    def FTR_SetupDownloadLarge(cls, eDeviceType, eFileType, szFileNameSrc, unBlockSize):
        request = "%R1Q,23313:"+str(eDeviceType)+","+str(eFileType)+","+str(szFileNameSrc)+","+str(unBlockSize)+"\r"
        responseArray = ["RC","rulNumOfBlocks"]
        return request, responseArray

    @classmethod
    def FTR_SetupDownloadXL(cls, ulBlockNumber):
        request = "%R1Q,23314:"+str(ulBlockNumber)+"\r"
        responseArray = ["RC","FTR_BLOCK_LARGE_val","FTR_BLOCK_LARGE_len"]
        return request, responseArray


