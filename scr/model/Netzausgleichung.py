# coding=utf-8
import datetime
import logging
import math
import collections

import numpy as np
from numpy import linalg as LA
from scipy.stats import chi2
from scipy.stats import f, norm

from model.Beobachtung import Beobachtung
from model.Funktionsmatrix import Funktionsmatrix
from model.Punkt import Punkt
from model.Unbekannte import Unbekannte



# noinspection PyCallingNonCallable
logger = logging.getLogger(__name__)


class Netzausgleichung:
    """Class Netzausgleichung
	"""
    # Attributes:
    def __init__(self, koordinaten, beobachtungen, s0priori=1, datumspunkte=None):
        """




        @param s0priori:
        @param datumspunkte:
        @type beobachtungen: list of Beobachtung
        @type koordinaten: list of Punkt
        @param koordinaten:  startwerte
        @param beobachtungen: Messwerte mit genauigkeit
        """
        if not datumspunkte: datumspunkte = []

        self.datumspunkte = datumspunkte
        self.koordinaten = koordinaten
        self.beobachtungen = beobachtungen
        self.aproxUnbekannten = self.buildUnbekanntenVector()
        """:type : list of Unbekannte"""
        self.ausUnbekannten = self.buildUnbekanntenVector()
        """:type : list of Unbekannte"""

        self.datumsdefekt = 4
        self.frei = len(beobachtungen) - len(self.aproxUnbekannten) + self.datumsdefekt
        self.Qxx = None
        self.S0priori = s0priori
        self.S0post = None
        self.v = None
        self.N_Matrix = None
        self.Qvv = None
        self.P = None
        self.l_ausg = None
        self.Q_ll = None
        self.A = None

    # Operations


    def getUnbekannteAsPunkte(self):
        """
        @rtype : list of Punkt
        @return: Liste mit Unbekannten, deren Standardabweichung und Konfidenellipsoid-Achsen für jeweiligen Punkt

        """
        retrunList = []
        for kor in self.koordinaten:
            x = None
            y = None
            z = None
            staAb_y = None
            staAb_x = None
            staAb_z = None
            for i, unb in enumerate(self.ausUnbekannten):

                if kor.No == unb.pointNo:

                    if unb.valueType == 'y':
                        staAb_y = math.sqrt(self.Qxx[i, i]) * self.S0post
                        y = unb.value
                    elif unb.valueType == 'x':
                        x = unb.value
                        staAb_x = math.sqrt(self.Qxx[i, i]) * self.S0post
                    elif unb.valueType == 'z':
                        z = unb.value
                        staAb_z = math.sqrt(self.Qxx[i, i]) * self.S0post

            newP = Punkt(kor.No, y, x, z, pointtype=kor.Type)
            newP.y_stAb = staAb_y
            newP.x_stAb = staAb_x
            newP.z_stAb = staAb_z

            konfA, konfB, konfC = self.getKonfidenzEllipsParam(newP.No)
            newP.konfAxis_A = konfA
            newP.konfAxis_B = konfB
            newP.konfAxis_C = konfC

            newP.qxx = self.getQxxForPoint(self.Qxx, newP.No)

            retrunList.append(newP)
        return retrunList


    def buildUnbekanntenVector(self):
        """
        Bildung des Unbekanntenvektor
        @return: Unbekanntenvektor mit y, x, z und omega
        """
        unbekanntenvector = []
        for punkte in self.koordinaten:
            unY = Unbekannte(punkte.No, 'y', punkte.y, punkte.Type)
            unX = Unbekannte(punkte.No, 'x', punkte.x, punkte.Type)
            unZ = Unbekannte(punkte.No, 'z', punkte.z, punkte.Type)
            unbekanntenvector.append(unY)
            unbekanntenvector.append(unX)
            unbekanntenvector.append(unZ)

        stationFirstTarget = []
        """:type : list of Beobachtung"""

        #Orientierungsunbekannten berechnen
        for beobachtungen in self.beobachtungen:
            if beobachtungen.type == 'Hz':

                if len(stationFirstTarget) == 0:
                    stationFirstTarget.append(beobachtungen)
                elif stationFirstTarget[-1].stationNo != beobachtungen.stationNo:

                    stationFirstTarget.append(beobachtungen)

        stationsWhichAreAlreadyCalc = []

        for beobachtungen in stationFirstTarget:
            if not beobachtungen.stationNo in stationsWhichAreAlreadyCalc:
                stationsWhichAreAlreadyCalc.append(beobachtungen.stationNo)
                hz = beobachtungen.value
                station = self.getPunktByNo(beobachtungen.stationNo)
                target = self.getPunktByNo(beobachtungen.targetNo)
                omega = self.Abriss(hz, station, target)
                unbOmega = Unbekannte(station.No, 'omega', omega, beobachtungen.type)
                unbekanntenvector.append(unbOmega)

        return unbekanntenvector


    def getPunktByNo(self, No):
        for punkte in self.koordinaten:
            if punkte.No == No:
                return punkte

    @staticmethod
    def Abriss(hz, station, target):
        """
        Berechnung der Orientierungsunbekannten omega für Unbekanntenvektor
        @rtype : double
        @type target: Punkt
        @type station: Punkt
        @type hz: double
        @param hz:
        @param station:
        @param target:
        """
        deltaY = float(target.y - station.y)
        deltaX = float(target.x - station.x)
        if deltaX == 0:
            deltaX = 10e-14
        t = math.atan(deltaY / deltaX)

        if deltaY > 0 > deltaX:
            t += math.pi
        elif deltaY < 0 and deltaX < 0:
            t += math.pi
        elif deltaY < 0 < deltaX:
            t += 2 * math.pi

        omega = t - hz

        if omega < 0:
            omega += 2 * math.pi
        return omega

    @staticmethod
    def getQ_ll(beobachtungs_vektor, s0):
        """
        aufstellen der Kofaktormatrix Q_ll

        @type beobachtungs_vektor: list of Beobachtung
        @type s0: Genauigkeit a priori
        @param beobachtungs_vektor:
        @param s0:
        @return: Kofaktormatrix Q_ll
        """

        S_ll = np.matrix(np.zeros((len(beobachtungs_vektor), len(beobachtungs_vektor) )))

        for i in range(0, len(beobachtungs_vektor)):
            S_ll[i, i] = beobachtungs_vektor[i].stdAb ** 2

        #Kofaktormatrix
        Q_ll = np.matrix((1 / s0 ** 2) * S_ll)

        return Q_ll


    def getQxxForPoint(self, Qxx, pointNo):
        """
        get the Submatrix of the Qxx for a given PointNo

        @param pointNo: PointNo
        """
        indexOfPoint = None
        for j, unb in enumerate(self.ausUnbekannten):
            if unb.pointNo == pointNo and unb.valueType == 'y':
                indexOfPoint = j

        #get Submatrix at index  (3x3)
        QxxP = Qxx[indexOfPoint:indexOfPoint + 3, indexOfPoint:indexOfPoint + 3]
        return QxxP


    @staticmethod
    def STDABW(s0post, Q):
        s = np.zeros((len(Q), 1 ))
        for i in range(1, len(Q)):
            s[i] = s0post * np.sqrt(Q[i, i])


    def ausgleichung(self):
        """
        Durchführung der 3D-Netzausgleichung
        @return: succes, iteration
        """
        unbekannten = self.ausUnbekannten
        beobachtungen = self.beobachtungen

        refPoints = []
        for unb in unbekannten:
            if unb.pointType == 0:
                #0-> Referenzpunkt
                refPoints.append(unb.pointNo)

        refPoints = list(set(refPoints))

        success = False
        iterationen = 1
        while not success:
            funkMatrix = Funktionsmatrix(unbekannten, beobachtungen)
            A = funkMatrix.get_A()
            if len(refPoints) < 3:
                B_Matrix = funkMatrix.get_B(unbekannten, 3)
            else:
                B_Matrix = funkMatrix.get_B(unbekannten, 3, refPoints)
            l = funkMatrix.get_l()
            Q_ll = self.getQ_ll(beobachtungen, self.S0priori)
            #Gewichtsmatrix
            P = np.matrix(Q_ll.I)
            #Normalgleichungsmatrix
            N_Matrix = np.dot(np.dot(A.T, P), A)
            erwNorm = funkMatrix.create_erwNorm(N_Matrix, B_Matrix)
            Qxx_help = np.matrix(erwNorm.I)
            Qxx = Qxx_help[0:len(N_Matrix), 0:len(N_Matrix)]
            #ausgeglichener Parametervektor
            n = np.matrix(np.dot(np.dot(A.T, P), l))
            x = np.matrix(np.dot(Qxx, n))
            for i, unbe in enumerate(unbekannten):
                unbe.value = unbe.value + x.item(i)
            #Abbruchkreterium
            if np.dot(x.T, x) < 1e-15:
                success = True
            iterationen += 1
            if iterationen > 50:
                success = False
                logger.warning('Ausgleichung nicht moeglich')
        self.Qxx = Qxx
        self.P = P
        self.Q_ll = Q_ll
        self.A = A
        self.N_Matrix = N_Matrix
        #Verbesserungsvektor
        v = np.matrix(np.dot(A, x) - l)
        self.l_ausg = l + v
        self.v = v
        for i, beo in enumerate(self.beobachtungen):
            beo.v = self.v.item(i)
            beo.l_ausg = self.l_ausg.item(i)
        self.Fehlerrechnung()
        self.Globaltest()
        self.Redundanzanteil()
        self.normVerb()
        return success, iterationen


    def do_S_Transformation_to_date(self, datumspunkte):
        funkMatrix = Funktionsmatrix(self.aproxUnbekannten, self.beobachtungen)
        #BI = funkMatrix.get_B(self.ausUnbekannten,3,[])
        #BII = funkMatrix.get_B(self.aproxUnbekannten,3,datumspunkte)

        N = np.dot(np.dot(funkMatrix.get_A().T, self.P), funkMatrix.get_A())

        #pseudoN = LA.pinv(N)
        w, v = LA.eigh(N)

        G = None
        for i, eingenwert in enumerate(w):
            if abs(eingenwert) < 1e-7:
                #print v[:,i]
                if G is None:
                    G = v[:, i]

                else:
                    G = np.concatenate((G, v[:, i]), 1)

        EI = np.identity(len(self.Qxx))
        EII = np.identity(len(self.Qxx))
        for i, unbe in enumerate(self.ausUnbekannten):
            EII[i, i] = 0
            for dpunkt in datumspunkte:
                if unbe.pointNo == dpunkt:
                    EII[i, i] = 1







                    #atest = np.matrix([[3,-1,-1,-1],[-1,3,-1,-1],[-1,-1,3,-1],[-1,-1,-1,3]])
                    #eingeW , eigeV = LA.eigh(atest)

                    # N= np.matrix([[187500,-62500,-62500,-62500], [-62500,187500, -62500 ,  -62500 ],[-62500,   -62500  , 187500,   -62500] ,  [  -62500  , -62500  , -62500  , 187500 ]])
                    # pes = LA.pinv(N)

        EII = np.dot(EII, G)
        EI = np.dot(EI, G)
        Einheit = np.identity(len(self.Qxx))

        SII = Einheit - (np.dot(G, np.dot(LA.inv(np.dot(EII.T, G)), EII.T)))
        SI = Einheit - (np.dot(G, np.dot(LA.inv(np.dot(EI.T, G)), EI.T)))

        #null_ob = np.dot(SII, G)
        #SII_test = np.dot(SII, SI)
        xI_dach = []
        apros = []
        #Wichtig xI_dach muss der verschiebungsvektor X_dach - X0_dach

        for i, ausUn in enumerate(self.ausUnbekannten):
            aproUn = self.aproxUnbekannten[i].value
            apros.append(aproUn)
            xI_dach.append(ausUn.value - aproUn)

        xI_dach = np.array(xI_dach)

        xII_dach = np.dot(SII, xI_dach)

        unbekanntenVektor = []

        for i, global_unbekannte in enumerate(self.aproxUnbekannten):
            newValue = xII_dach.item(i) + global_unbekannte.value

            unbekannte = Unbekannte(global_unbekannte.pointNo, global_unbekannte.valueType, newValue)
            unbekanntenVektor.append(unbekannte)

        Qxx_II = np.matrix(np.dot(np.dot(SII, self.Qxx), SII.T))

        return Qxx_II, unbekanntenVektor


    def Fehlerrechnung(self):
        """
        Fehlerrechnung nach Ausgleichung

        @param s0post: geschätzte Varianz der Gewichtseinheit
        @param Qlldach: Varianz-Kovarianzmatrix der verbesserten Beobachtungen
        @param Qvv: Varianz-Kovarianzmatrix der Verbesserungen

        @return: s0post, Qlldach, Qvv
        """
        #(f=Freiheitsgrad n-u)
        s0post = np.sqrt((np.dot(np.dot(self.v.T, self.P), self.v)) / self.frei)
        self.S0post = s0post.item(0)

        Qlldach = np.matrix(np.dot(np.dot(self.A, self.Qxx), self.A.T))

        Qvv = np.matrix(self.P.I - np.dot(np.dot(self.A, self.Qxx), self.A.T))
        self.Qvv = Qvv

        for i, beo in enumerate(self.beobachtungen):
            #Standardabweichung der Verbesserungen
            qvvi = self.Qvv.item((i, i))
            beo.s_v = self.S0post * math.sqrt(qvvi)
            #Standardabweichung der Verbesserten Beobachtungen
            qlli = Qlldach.item((i, i))
            beo.s_la = self.S0post * math.sqrt(qlli)

            # beo.s_v=self.Q_ll.item((i,i))


        #Ev wird in den beobachtunge gespeichert
        self.Redundanzanteil()
        self.normVerb()

        return Qvv, Qlldach, s0post

    def Globaltest(self):

        """
        Globaltest: Überprüfung ob ein grober Fehler im Datenmaterial vorliegt

        @rtype : (bool,float,float)
        @return: Succes, T, chi
        """

        chi = chi2.ppf(0.95, self.frei)

        T = self.frei * self.S0post ** 2 / self.S0priori ** 2
        if T > chi:
            print "Ein grober Fehler wird im Datenmaterial vermutet!"
            return False, T, chi
        return True, T, chi

    def Redundanzanteil(self):
        """
        Berechnung der Redundanzanteile


        @return:
        """

        R = np.matrix(np.dot(self.Qvv, self.P))
        r = np.matrix(np.zeros((len(R), 1)))

        for i in range(0, len(R)):
            r[i] = R[i, i]

        EV = np.matrix(100 * r)

        for i, beo in enumerate(self.beobachtungen):
            beo.EV = EV.item((i))


    def normVerb(self):

        """
        Berechnung der normierten Verbesserungen


       @return:
        """

        NV = np.matrix(np.zeros((len(self.Qvv), 1)))

        for i in range(1, len(self.Qvv)):
            NV[i] = np.absolute(self.v[i]) / (self.S0priori * np.sqrt(self.Qvv[i, i]))

        for i, beo in enumerate(self.beobachtungen):
            beo.NV = NV.item((i))


    def getKonfidenzEllipsParam(self, PointNo):
        """
        calculate the Ellipsoidvectors for one given PointNo

        @rtype : (ndarray,ndarray,ndarray)
        @type PointNo: str
        @return: 3 Ellipsoidvectors
        """

        #Kofaktormatrix for one Point
        qxxP = self.getQxxForPoint(self.Qxx, PointNo)

        f_qunatil = f.ppf(0.999, 3, self.frei)
        #TODO:Czommer: warum mal 2

        #Kovarianzmatrix mit f_quantil multipliziert
        kovarP = qxxP * (self.S0post ** 2 * f_qunatil * 2)

        ww, vv = np.linalg.eig(kovarP)


        #w= eigenvalue ->quadrat der halbachsenlängen (deswegen noch sqrt)
        #v= eigenvectors -> richtung der halbachsen

        ellipsoidVektorA = np.array((vv[:3, 0] * math.sqrt(ww[0]))).reshape(-1, )
        ellipsoidVektorB = np.array((vv[:3, 1] * math.sqrt(ww[1]))).reshape(-1, )
        ellipsoidVektorC = np.array((vv[:3, 2] * math.sqrt(ww[2]))).reshape(-1, )
        #return ellipsoidVektorA, ellipsoidVektorB, ellipsoidVektorC

        #Vertoren nach Größe ordnen, weil beim zwichen die Sigulärwerte auch nach größe geordnet sind

        lengthA = np.linalg.norm(ellipsoidVektorA)
        lengthB = np.linalg.norm(ellipsoidVektorB)
        lengthC = np.linalg.norm(ellipsoidVektorC)

        ellipsoidVektos_dict = {lengthA: ellipsoidVektorA,
                                lengthB: ellipsoidVektorB,
                                lengthC: ellipsoidVektorC}
        od = collections.OrderedDict(sorted(ellipsoidVektos_dict.items()))

        #die werte in dem orderDict sind aufsteigend sortiert, die Ellipsoidachsen sollen aber absteigend sein
        #deshalb die indexe 2-1-0 und nicht 0-1-2
        lengthA, ellipsoidVektorA = od.items()[2]
        lengthB, ellipsoidVektorB = od.items()[1]
        lengthC, ellipsoidVektorC = od.items()[0]

        return ellipsoidVektorA, ellipsoidVektorB, ellipsoidVektorC


    def printProtokol(self, pName):
        """



        @rtype : file
        @type pName: str
        @param pName:
        """
        pName += ".txt"
        newFile = open(pName, 'w')

        newFile.write("\t\t3D Netzausgleichung\n\n")
        newFile.write("Ausgleichung nach vermittelden Beobachtungen" + "\n\n")
        a = datetime.datetime.now()

        newFile.write("\tBerechnet am: " + a.strftime("%Y-%m-%d %H:%M:%S") + "\n\n")

        newFile.write('Strecken und Standardabweichungen in   [m]\t\t\t\n')
        newFile.write('Richtungen und Standardabweichungen in [gon]\t\t\t' + "\n\n")

        newFile.write('Anzahl Unbekannte\t\t: %7d\n' % len(self.ausUnbekannten))
        newFile.write('Anzahl Beobachtungen\t: %7d\n' % len(self.beobachtungen))
        newFile.write('Datumsdefekt:\t\t\t: %7d\n' % self.datumsdefekt)
        newFile.write('Freiheitsgrad (f):\t\t\t: %7d\n' % self.frei)





        #newFile.write('Freiheitsgrad:\t\t\t: %7d\n' % self.frei + "\n\n")

        newFile.write('sigma_0 a priori (S0_priori):      %2.5f\n' % self.S0priori + "\n")
        newFile.write('sigma_0 a posteriori (S0_post):  %2.5f\n' % self.S0post + "\n\n")
        newFile.write('*** Globaltest ***\n')

        globaTestSucces, T, chi = self.Globaltest()
        newFile.write('Schrankenwert:\nChi^2(r,0.95)\t\t\t:%.3f\n\n' % chi)
        newFile.write('Testgroesse:\nf * S0_post^2/S0_priori^2\t:%.3f\n\n' % T)
        newFile.write('Bedingung: Schrankenwert >= Testgroesse\n\n')

        if globaTestSucces:
            newFile.write('-> *** Vermutlich kein grober Fehler ***\n\n')

        else:
            newFile.write('-> *** Vermutlich liegt ein grober Fehler vor ***\n\n')

        newFile.write('\t\t\t      Näherungskoordinaten\n')
        newFile.write('\t\t\t      ====================\n\n')

        newFile.write(' Punktnummer\t  Y\t        X\t\t   Z\n\n')

        for punkte in self.koordinaten:
            newFile.write(
                u'   {0:s}\t\t{1:9.5f}  {2:9.5f}  {3:9.5f}'.format(punkte.No, punkte.y, punkte.x, punkte.z)
                + '\n\n')

        newFile.write('\n\n\n\n\t\t\t\t\t\t\t\t\t\t\t\t Beobachtungen\n')
        newFile.write('\t\t\t\t\t\t\t\t\t\t\t\t =============\n\n\n')

        newFile.write('\t\t\t\t\t\t\t\t\t\t\t\t  Richtungen\n')
        newFile.write('\t\t\t\t\t\t\t\t\t\t\t\t  ----------\n\n')
        newFile.write(
            '                  \t\t L\t      Stabw.\t     v\t      Stabw.v\t    La\t     Stabw.La\t       EV\t     NV\t   KW(NV)\n')
        KWNV = norm.ppf(0.95)
        for beo in self.beobachtungen:
            if beo.type == 'Hz':
                newFile.write("\n")
                newFile.write(
                    u'von {0:s} nach {1:s}:\t\t{2:9.5f}\t{3:9.5f}\t{4:9.5f}\t{5:9.5f}\t{6:9.5f}\t{7:9.5f}\t{8:9'
                    u'.0f}\t{9:9.3f}\t{10:9.3f}'.format(
                        beo.stationNo, beo.targetNo, beo.getValueAsGon(),
                        beo.getstdAbAsGon(), beo.getvAsGon(), beo.gets_vAsGon(), beo.getl_ausgAsGon(),
                        beo.gets_laAsGon(), beo.EV, beo.NV, KWNV) + '\n')

        newFile.write('\n\n\t\t\t\t\t\t\t\t\t\t\t\t  Zenitwinkel\n')
        newFile.write('\t\t\t\t\t\t\t\t\t\t\t\t  -----------\n\n')
        newFile.write(
            '                  \t\t L\t      Stabw.\t     v\t      Stabw.v\t    La\t     Stabw.La\t       EV\t     NV\t   KW(NV)\n')

        for beo in self.beobachtungen:
            if beo.type == 'V':
                newFile.write("\n")
                newFile.write(
                    u'von {0:s} nach {1:s}:\t\t{2:9.5f}\t{3:9.5f}\t{4:9.5f}\t{5:9.5f}\t{6:9.5f}\t{7:9.5f}\t{8:9.0f}\t{9:9.3f}\t{10:9.3f}'.format(
                        beo.stationNo, beo.targetNo, beo.getValueAsGon(),
                        beo.getstdAbAsGon(), beo.getvAsGon(), beo.gets_vAsGon(), beo.getl_ausgAsGon(),
                        beo.gets_laAsGon(), beo.EV, beo.NV, KWNV) + '\n')

        newFile.write('\n\n\t\t\t\t\t\t\t\t\t\t\t\t    Strecken\n')
        newFile.write('\t\t\t\t\t\t\t\t\t\t\t\t    --------\n\n')
        newFile.write(
            '                  \t\t L\t      Stabw.\t     v\t      Stabw.v\t    La\t     Stabw.La\t       EV\t     NV\t   KW(NV)\n')

        for beo in self.beobachtungen:
            if beo.type == 'Dist':
                newFile.write("\n")
                newFile.write(
                    u'von {0:s} nach {1:s}:\t\t{2:9.5f}\t{3:9.5f}\t{4:9.5f}\t{5:9.5f}\t{6:9.5f}\t{7:9.5f}\t{8:9.0f}\t{9:9.3f}\t{10:9.3f}'.format(
                        beo.stationNo, beo.targetNo, beo.value,
                        beo.stdAb, beo.v, beo.s_v, beo.getl_ausg(), beo.s_la, beo.EV, beo.NV, KWNV) + '\n')

        newFile.write('\n\n\n\n\t\t\t\t\t\t\t\t\t\t\t\t\t   Ausgeglichene Koordinaten')
        newFile.write('\n\t\t\t\t\t\t\t\t\t\t\t\t\t   =========================\n\n\n')

        newFile.write(
            ' Punktnummer\t  Y\t\t     X\t         Z\t\t  Stabw.Y\t  Stabw.X\t  Stabw.Z\t  Stabw.P\t\t a\t\t     b\t\t\t c\t\t  V [cm³]\n\n')

        ausgPunkte = self.getUnbekannteAsPunkte()

        #TODO Stabw der ausgeglichenen Koord. in Punkt schreiben



        #bla
        for i, ausP in enumerate(ausgPunkte):
            newFile.write(
                u'   {0:s}\t\t{1:9.5f}\t{2:9.5f}\t{3:9.5f}\t{4:9.5f}\t{5:9.5f}\t{6:9.5f}\t{7:9.5f}\t{8:9.5f}\t{9:9.5f}\t{10:9.5f}\t{11:9.5f} '.format(
                    ausP.No,
                    ausP.y,
                    ausP.x,
                    ausP.z,
                    ausP.y_stAb,
                    ausP.x_stAb,
                    ausP.z_stAb,
                    ausP.getSTABWPunkt(),
                    ausP.konfA_laenge(),
                    ausP.konfB_laenge(),
                    ausP.konfC_laenge(),
                    ausP.konf_V())
                + '\n\n')

        newFile.close()

        return newFile


