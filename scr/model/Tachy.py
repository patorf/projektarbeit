# coding=utf-8
from PySide import QtCore
from time import sleep
import logging

import serial
from serial.tools import list_ports

from model.LeicaGeoCom import LeicaGeoCom as Lgc
from model.LeicaGeoCom import *
from model.Messdaten import Messdaten

logger = logging.getLogger(__name__)

#noinspection PyPep8Naming
class Tachy(QtCore.QObject):
    """
    Mit dieser Klassen können Befehle an ein Tachymeter geschickt werden, das über die Serielle Schnittstelle mit dem
    Computer verbunden ist.
    @type status: str
    """
    numOfInitTachy = 0
    usedPorts = []
    #Signals
    updateStatus = QtCore.Signal(str)

    def __init__(self, status='nicht Verbunden!'):
        """
        setzt den default Status auf 'nicht Verbunden'
        @type status: str
        """
        QtCore.QObject.__init__(self)

        logger.debug('init Tachy ' + str(Tachy.numOfInitTachy))

        self.usedSerial = None
        self.availablePorts = list_ports.comports()
        self.isWorking = False
        self.ready = None
        self.status = status

        Tachy.numOfInitTachy += 1
        #  self.sendTachy(self.ser,ATR_on)

    def __del__(self):
        if self.usedSerial is not None:
            self.closeSerial()
            #logger.debug('tachy deleted')



            # Tachy.numOfInitTachy -= 1


    @property
    def status(self):
        """
        getter of the property status

        @return:
        """
        return self._status

    # noinspection PyAttributeOutsideInit
    @status.setter
    def status(self, status):
        """
        setter of status
        emit the updateStatus Signal
        @param status:
        """
        self._status = status

        self.updateStatus.emit(self._status)


    def sendTachy(self, geoComRequest):
        """
        send a query over the serial Port and waint for the respose


        @type geoComRequest: (str,str)
        @param geoComRequest: (queryString,responseArray) is the return
        of a LeicaGeoCom interface Function
        @return : a boolean which indicates if the tachy response to the
        query. if true the second parameter is the respose of the tachy
        @rtype : (bool,dict)
        """
        if not self.ready:
            logger.warning('Tachy not ready')
            return False, {}

        if self.isWorking:
            logger.warning('Tachy is busy')
            return False, {}

        #devide the geoComRequest Tuple in the querysting (R1Q,9027:....) and the responseArray (['RC','HZ'])
        qstring, responseArray = geoComRequest

        logger.debug("SendTachy: %s : %s", str(qstring), str(responseArray))

        response = ''

        self.isWorking = True
        try:
            if not self.usedSerial.isOpen():
                self.usedSerial.open()
            self.usedSerial.write(qstring)

            while response == '':
                sleep(0.01)

                response = self.usedSerial.readline()

            self.isWorking = False
            responseDict = Lgc.convertResponse(response, responseArray)
            logger.info(responseDict)
            if responseDict['RC'] != 0:
                logger.warning("Tachy Response:%s", Lgc.returnCodes[responseDict['RC']][1])
            return True, responseDict
        except Exception as ep:
            logger.error("Connection Error")
            logger.exception(ep)

            self.isWorking = False
            self.closeSerial()
            return False, {}

    def initSerial(self, portName):
        """




        @type portName: str
        @param portName: Portname
        @return:
        """
        try:
            self.usedSerial = serial.Serial()
            self.usedSerial.port = portName
            self.usedSerial.baudrate = 19200  # COM-Schnittstelle des Rechners angeben
            self.usedSerial.bytesize = serial.EIGHTBITS
            self.usedSerial.parity = serial.PARITY_NONE
            self.usedSerial.stopbits = serial.STOPBITS_ONE
            self.usedSerial.timeout = 500
            self.usedSerial.writeTimeout = 500

            self.usedSerial.InputBufferSize = 512
            self.usedSerial.OutputBufferSize = 512
            self.usedSerial.open()
            if self.usedSerial.isOpen():
                logger.info('Port %s Open', self.usedSerial.portstr)

                self.ready = True
                self.status = 'Verbunden'
                logger.info('Tachy ready')
                self.setUpTachy()
                #self.closeSerial()
            else:
                logger.info('Hier darf eigentlich nix passieren ')
                self.status = 'nicht Verbunden'
                self.ready = False
        except Exception as ep:  #serial.SerialException as se:

            logger.exception(ep)
            self.status = 'nicht Verbunden'
            self.ready = False


    def setUpTachy(self):
        """
        setup the Tachy for use in our projekt


        """

        #edm_Mode = EDM_MODE.EDM_PRECISE_IR
        #edm_Mode = EDM_MODE.EDM_SINGLE_FAST
        edm_Mode = EDM_MODE.EDM_SINGLE_STANDARD
        self.sendTachy(Lgc.TMC_SetEdmMode(edm_Mode))

        self.sendTachy(Lgc.AUS_SetUserAtrState(1))  #ATR on

        self.sendTachy(Lgc.AUT_SetFineAdjustMode(AUT_ADJMODE.AUT_POINT_MODE))  #soll fuer kurze distanzen besser sein
        # self.sendTachy(Lgc.CSV_CheckPower())
        #self.sendTachy(Lgc.face)


    def closeSerial(self):
        """
        close the Serial port

        """
        self.usedSerial.close()
        if not self.usedSerial.isOpen():
            logger.info('is closed')


    def measureMultipleTargets(self, targets):
        """


        @rtype : list of Messdaten
        @type targets: (bool,list of Messdaten)
        """
        measuredTargets = []
        for i, messdaten in enumerate(targets):
            success, messwerte = self.measureAt_Hz_V(messdaten)
            if success:
                measuredTargets.append(messwerte)
            else:
                #try one more time:
                logger.warning('ziel wird nochmal versucht zu messen')
                success1, messwerte1 = self.measureAt_Hz_V(messdaten)
                if success:
                    measuredTargets.append(messwerte1)
                else:
                    logger.error('ziel konnte nicht gemessen werden ')
                    break

        if len(targets) != len(measuredTargets):
            logger.warning('Es konnten nicht alle Ziele gemessen werden')
            return False, []
        else:
            return True, measuredTargets


    def measureAt_Hz_V(self, targetMesswert):

        """

        @rtype : (bool,Messdaten)
        @type targetMesswert: Messdaten
        @param targetMesswert: Messdaten an dem sich ein Ziel befinden
        @return:
        """

        success = False
        returnMesswert = None

        findSuccsess = self.findTargetAt(targetMesswert)
        self.setPrismType(targetMesswert.PrismType)

        if findSuccsess:
            distSuccess = self.distMeasurement()
            if distSuccess:
                getMesSuccess, returnMesswert = self.getMeasurements()
                returnMesswert.TargetNo = targetMesswert.TargetNo
                returnMesswert.Type = targetMesswert.Type
                success = True
        logger.info("find and measure Target %s", success)
        if not success:
            logger.warning("can not measure Target %s", targetMesswert.TargetNo)
        return success, returnMesswert


    def findTargetAt(self, targetMesswert):
        """
        search a Target at a given Messdaten. Return true if the target was found, false if not

        @type targetMesswert: Messdaten
        @param targetMesswert:
        @rtype: bool

        """
        prismType = targetMesswert.PrismType
        if not self.setPrismType(prismType):
            logger.error('Can not set PrismType')
            return False, Messdaten()

        posMode = AUT_POSMODE.AUT_PRECISE
        atrMode = AUT_ATRMODE.AUT_TARGET
        success, response = self.sendTachy(
            Lgc.AUT_MakePositioning(targetMesswert.Hz, targetMesswert.V, posMode, atrMode))

        if success:
            if response['RC'] == 0:
                logger.info("Find Target Hz:%3.5f V:%3.5f %s", targetMesswert.Hz, targetMesswert.V, success)
                return success
        logger.info("Find Target Hz:%3.5f V:%3.5f %s", targetMesswert.Hz, targetMesswert.V, success)

        return False

    def find_and_measure_Target(self, prismType):

        """





        @param prismType:
        @rtype : (bool, Messdaten)
        @return: Messdaten
        """
        if not self.setPrismType(prismType):
            logger.error('Can not set PrismType')
            return False, Messdaten()

        searchField = 0.05  #rad 0.01 = ca.5cm
        #retMessdaten = Messdaten()
        success, response = self.sendTachy(Lgc.AUT_FineAdjust(searchField, searchField))
        if success:
            logger.info('Find Target at current position')
            success = self.distMeasurement()
            if success:
                success, messdaten = self.getMeasurements()
                if success:
                    return True, messdaten

        logger.warning('No Target found')
        return False, Messdaten()


    def distMeasurement(self):

        """
        trigger a distMeasurement with the defined dist measurement type


        @rtype : bool
        @return: the success of the dist measurement
        """
        measureCommand = TMC_MEASURE_PRG.TMC_DEF_DIST
        tmc_incline = TMC_INCLINE_PRG.TMC_AUTO_INC

        success, distResp = self.sendTachy(Lgc.TMC_DoMeasure(measureCommand, tmc_incline))
        if success:
            if distResp['RC'] == 0:
                logger.info("Dist measurement %s", success)
                return success

        logger.info("Dist measurement %s", success)
        return success

    def getMeasurements(self, messdaten=None):
        """
        request the tachy for valid measurements


        @type messdaten: Messdaten
        @param messdaten:
        @rtype : (bool,Messdaten)
        @return:

        """
        if messdaten is None:
            messdaten = Messdaten()

        tmc_incline = TMC_INCLINE_PRG.TMC_AUTO_INC

        waitTime = 1000
        tryCount = 15
        success = False
        resposeDict = {}
        while tryCount != 0:
            #logger.debug('try to get distMeasurement')
            success, resposeDict = self.sendTachy(Lgc.TMC_GetSimpleMea(waitTime, tmc_incline))
            print success
            print resposeDict
            if not success:
                break
            if resposeDict['RC'] == 0:
                break
            tryCount -= 1

        if success:
            if resposeDict['RC'] == 0:
                messdaten.Hz = resposeDict['Hz']
                messdaten.V = resposeDict["V"]
                messdaten.Dist = resposeDict["SlopeDistance"]
                messdaten.isMeasured = True
                logger.info('get measurements Point:%s Hz: %3.5f V:%3.5f Dist:%3.4f', str(messdaten.TargetNo),
                            messdaten.Hz, messdaten.V, messdaten.Dist)
                prismType = self.getPrismType()
                messdaten.PrismType = prismType
                return True, messdaten
            else:
                return False, messdaten
        else:
            logger.warning('simple measurement error PointNo:%s', messdaten.TargetNo)
        return success, messdaten

    def changeFace(self):
        """
        change to the other face and search for a target

        @return:
        """
        posMode = AUT_POSMODE.AUT_PRECISE
        atrMode = AUT_ATRMODE.AUT_TARGET
        success, me = self.sendTachy(Lgc.AUT_ChangeFace(posMode, atrMode))
        return success

    def setPrismType(self, prismType):
        """
        sets the prismType to the given one
        @param prismType:
        @return: succes (True or False)
        """
        succes, returnCode = self.sendTachy(Lgc.BAP_SetPrismType(prismType))
        return succes

    def getPrismType(self):
        """
        return the current prismtype or False

        @return: prismType
        """
        succes, responseDict = self.sendTachy(Lgc.BAP_GetPrismType())
        if succes:
            return responseDict["ePrismType"]
        else:
            return succes

    def changeToFace(self, face=0):
        """
        change to the given face
        0=0< Vz <200
        1=200 < Vz < 400

        @type face: int
        """
        success, responseDict = self.sendTachy(Lgc.TMC_GetFace())

        if success:
            print responseDict['Face']
            print face
            if responseDict['Face'] != face:
                self.changeFace()


    @staticmethod
    def getAvailablePrismTypes():
        """

        @rtype : dict
        """
        return BAP_PRISMTYPE.getPrismaDict()



