# coding=utf-8
import datetime

import numpy as np
from scipy.stats import f

from model.Epoche import Epoche



# noinspection PyCallingNonCallable,PyCallingNonCallable
class Deformationsanalyse:
    def __init__(self, Epoche1, Epoche2, signivea=0.05):

        """



        @type Epoche2: Epoche
        @type Epoche1: Epoche
        @param Epoche1: Epoche 1
        @param Epoche2: Epoche 2
        """
        self.Epoche1 = Epoche1
        self.Epoche2 = Epoche2
        self.signivea = signivea
        self.defoPoints = []  #list mit list [punktnummer, F,fisher]

        self.isDefo = None
        self.S0post_quadrat = None
        self.freiheit = None
        self.difVektor_beschreibung = None
        self.differenzVektor = None
        self.fischer_q = None
        self.F = None
        self.grundgesamtheit = False
        self.status = None

    def getQddMatrix(self, datumspunkte, aufRefPunkten=False):
        """
        Erzeugung der Kofaktorenmatrix Qdd vom Differenzvektor

        @param datumspunkte:
        @return: Qdd, differenzvektor
        """

        #[]-> Globale Defo
        #[1,2,3,4]
        Netzaus1 = self.Epoche1.netzausgleichung
        Netzaus2 = self.Epoche2.netzausgleichung
        Qxx_Epoche1 = None
        Qxx_Epoche2 = None

        unbekanntenVektor1 = self.Epoche1.netzausgleichung.ausUnbekannten
        unbekanntenVektor2 = self.Epoche2.netzausgleichung.ausUnbekannten

        if len(datumspunkte) == 0:
            Qxx_Epoche1 = Netzaus1.Qxx
            Qxx_Epoche2 = Netzaus2.Qxx
        else:
            Qxx_Epoche1, unbekanntenVektor1 = self.Epoche1.netzausgleichung.do_S_Transformation_to_date(datumspunkte)
            Qxx_Epoche2, unbekanntenVektor2 = self.Epoche2.netzausgleichung.do_S_Transformation_to_date(datumspunkte)
        np.savetxt('QxxE1', Qxx_Epoche1)
        np.savetxt('QxxE2', Qxx_Epoche2)
        differenzVektor = []
        gleichePunkte = []
        diffVektorBeschreibung = []
        if len(datumspunkte) == 0:
            for unb1 in unbekanntenVektor1:
                for unb2 in unbekanntenVektor2:
                    if unb1.pointNo == unb2.pointNo and unb1.valueType != 'omega' and unb1.valueType == unb2.valueType:
                        diffVektorBeschreibung.append(unb1)
                        gleichePunkte.append(unb1.pointNo)
                        differenzVektor.append(unb2.value - unb1.value)

        else:
            gleichePunkte = datumspunkte
            for unb1 in unbekanntenVektor1:
                for unb2 in unbekanntenVektor2:
                    if not aufRefPunkten:
                        for datumspunktNr in datumspunkte:
                            if unb1.pointNo == unb2.pointNo == datumspunktNr and unb1.valueType != 'omega' and unb1.valueType == unb2.valueType:
                                differenzVektor.append(unb2.value - unb1.value)
                                diffVektorBeschreibung.append([unb1.pointNo, unb2.value - unb1.value])
                    else:
                        if unb1.pointNo == unb2.pointNo and unb1.valueType != 'omega' and unb1.valueType == unb2.valueType:
                            differenzVektor.append(unb2.value - unb1.value)
                            diffVektorBeschreibung.append([unb1.pointNo, unb2.value - unb1.value])

        #print (diffVektorBeschreibung)
        #dublicat loeschen
        gleicheP = list(set(gleichePunkte))

        Qdd = np.zeros((3 * len(gleicheP), 3 * len(gleicheP)))

        j = 0
        for i in range(0, len(gleicheP) * 3, 3):
            j = i / 3

            qdd1 = Netzaus1.getQxxForPoint(Qxx_Epoche1, gleicheP[j])
            qdd2 = Netzaus2.getQxxForPoint(Qxx_Epoche2, gleicheP[j])
            qdd1Plus2 = qdd1 + qdd2

            Qdd[i, i] = qdd1Plus2[0, 0]
            Qdd[i + 1, i] = qdd1Plus2[1, 0]
            Qdd[i + 2, i] = qdd1Plus2[2, 0]
            Qdd[i, i + 1] = qdd1Plus2[0, 1]
            Qdd[i + 1, i + 1] = qdd1Plus2[1, 1]
            Qdd[i + 2, i + 1] = qdd1Plus2[2, 1]
            Qdd[i, i + 2] = qdd1Plus2[0, 2]
            Qdd[i + 1, i + 2] = qdd1Plus2[1, 2]
            Qdd[i + 2, i + 2] = qdd1Plus2[2, 2]
        Qdd_matrix = np.matrix(Qdd)
        np.savetxt('Qdd', Qdd_matrix)

        #self.differenzVektor = differenzVektor
        #[[PunktNummer, y],[PunktNummer, x],[PunktNummer, z]]
        if aufRefPunkten:
            #beim erstem durchlauf wir die variable überschrieben
            self.difVektor_beschreibung = diffVektorBeschreibung

        return Qdd_matrix, differenzVektor


    def checkGrundgesamtheit(self):
        """
        Überprüfung der Grundgesamtheit, ob Deformationanalyse durchführbar ist
        Berechnung der Genauigkeit und des Freiheitsgrades aus beiden Epochen

        @return:
        """
        frei1 = self.Epoche1.netzausgleichung.frei
        frei2 = self.Epoche2.netzausgleichung.frei
        S0post_E1 = self.Epoche1.netzausgleichung.S0post
        S0post_E2 = self.Epoche2.netzausgleichung.S0post
        v_E1 = self.Epoche1.netzausgleichung.v
        v_E2 = self.Epoche2.netzausgleichung.v
        P_E1 = self.Epoche1.netzausgleichung.P
        P_E2 = self.Epoche2.netzausgleichung.P

        if S0post_E1 > S0post_E2:
            S0_ver = S0post_E1 ** 2 / S0post_E2 ** 2
        else:
            S0_ver = S0post_E2 ** 2 / S0post_E1 ** 2
        f_qunatil = f.ppf(1 - self.signivea, frei1, frei2)

        freiheit = None
        S0post_quadrat = None

        if S0_ver > f_qunatil:
            print 'Die Beobachtungen stammen aus keiner Grundgesamtheit!'

        else:
            ohm1 = np.dot(np.dot(v_E1.T, P_E1), v_E1)
            ohm2 = np.dot(np.dot(v_E2.T, P_E2), v_E2)
            ohm = ohm1 + ohm2
            freiheit = frei1 + frei2
            S0post_quadrat = ohm / freiheit
            self.grundgesamtheit = True

        self.freiheit = freiheit
        self.S0post_quadrat = S0post_quadrat


    def checkLocalDefo(self, datumspunkte):
        """
        Lokalisierung welcher Punkt eine Deformation aufweist

        @type datumspunkte: list
        @rtype : (bool,float,float)
        @param datumspunkte:
        @return: isDefo, testvalue, fischer_q
        """
        Qdd, difVektor = self.getQddMatrix(datumspunkte)

        difVektor2 = np.asarray(difVektor)
        differVektor = np.matrix(difVektor2)

        #print "PPPPPP"
        Qdd_pse = np.linalg.pinv(Qdd)
        Qdd_pseudo = np.matrix(Qdd_pse)
        h = np.linalg.matrix_rank(Qdd)
        R = np.matrix(np.dot(np.dot(differVektor, Qdd_pseudo), differVektor.T))

        #Testgröße
        F = R / (self.S0post_quadrat * h)
        #print datumspunkte
        #print 'h:' + str(h)
        #print 'R'
        #print R
        #print F

        if F < 0:
            print 'F ist NEGATIV!!!!'
        F = F.item((0, 0))

        #Fischer_Quantil
        fischer_q = f.ppf(1 - self.signivea, h, self.freiheit)

        return F > fischer_q, F, fischer_q


    def defoAnalyse(self):
        """
        Globale Überprüfung, ob überhaupt eine Deformation stattgefunden hat

        @return:
        """
        defopkt1 = []
        defopkt2 = []

        for unb in self.Epoche1.netzausgleichung.ausUnbekannten:
            defopkt1.append(unb.pointNo)

        for unb in self.Epoche2.netzausgleichung.ausUnbekannten:
            defopkt2.append(unb.pointNo)

        defopkt1 = list(set(defopkt1))
        defopkt2 = list(set(defopkt2))

        pontsInBothEpochen = list(set(defopkt1).intersection(set(defopkt2)))
        Qdd, difVektor = self.getQddMatrix(pontsInBothEpochen)

        difVektor2 = np.asarray(difVektor)
        differVektor = np.matrix(difVektor2)

        Qdd_pse = np.linalg.pinv(Qdd)
        Qdd_pseudo = np.matrix(Qdd_pse)
        h = np.linalg.matrix_rank(Qdd)
        R = np.matrix(np.dot(np.dot(differVektor, Qdd_pseudo), differVektor.T))
        np.savetxt('R.txt', R)
        refPoints = []
        objectPoints = []
        for unb in self.Epoche1.netzausgleichung.ausUnbekannten:
            if unb.pointType == 0:
                #0-> Referenzpunkt
                refPoints.append(unb.pointNo)
            elif unb.pointType == 1:
                #1-> Objektpunkt
                objectPoints.append(unb.pointNo)
        refPoints = list(set(refPoints))
        objectPoints = list(set(objectPoints))
        defoPoints = []  #List mit Punktnummern
        #Damit wird der deifVektor beschreibung gespeichert, der nur auf den RefPunkten gelagert ist
        self.getQddMatrix(refPoints, True)




        #Test auf Globale Deformation
        isDefo_global, testvalue_global, fischer_global = self.checkLocalDefo(pontsInBothEpochen)

        self.F = testvalue_global
        self.fischer_q = fischer_global

        if isDefo_global:
            print ('Es liegt eine Deformation vor!')

            checkDefo, F, fisherQ = self.checkLocalDefo(refPoints)

            if checkDefo:


                print ('Referenzpunkte verschoben')

                self.status = 'Referenzpunkte verschoben!'
                for refPoint in refPoints:
                    self.defoPoints.append([refPoint, F, fisherQ])
                    #TODO:Ist das richtig so
            else:

                for objectPointNo in objectPoints:
                    refPoints.append(objectPointNo)
                    checkDefo, F, fisherQ = self.checkLocalDefo(refPoints)

                    if checkDefo:
                        #defo liegt vor

                        defoPoints.append(objectPointNo)

                        self.defoPoints.append([objectPointNo, F, fisherQ])
                        del refPoints[-1]
                self.status = 'Deformation der Objektpunkte detektiert!'
            self.isDefo = True


            #  self.defoPoints = defoPoints



        else:
            self.isDefo = False
            self.status = 'Keine Deformation detektiert!'
            print ('Es liegt keine Deformation vor!')


    def printProtokol(self, fileName=None):
        """



        @rtype : file
        @type fileName: str
        """
        if fileName is None:
            fileName = self.Epoche1.Name + "_" + self.Epoche2.Name + "_Defo"

        pName = fileName + ".txt"
        newFile = open(pName, 'w')

        newFile.write("\t\tDeformationsanalyse\n")
        newFile.write('\t\t===================\n\n')

        a = datetime.datetime.now()

        newFile.write("\tBerechnet am: " + a.strftime("%Y-%m-%d %H:%M:%S") + "\n\n")
        newFile.write("\t\t\t\t\t\t  " + self.Epoche1.Name + "\t\t\t\t\t\t\t\t\t\t\t\t\t  Genauigkeit\n")
        newFile.write(
            "\t-----------------------------------------------------\t\t\t\t-----------------------------------------\n")
        newFile.write("\tPoint-Nr.\t\t Y\t\t\t\t X\t\t\t\t Z\t\t\t\t\t\t Y\t\t\t\t X\t\t\t\t Z\n")

        ausgPunkte = self.Epoche1.netzausgleichung.getUnbekannteAsPunkte()

        for i, ausP in enumerate(ausgPunkte):
            newFile.write(
                u'\t\t{0:s}\t\t{1:9.5f}\t\t{2:9.5f}\t\t{3:9.5f}\t\t\t\t{4:9.5f}\t\t{5:9.5f}\t\t{6:9.5f}'.format(ausP.No,
                                                                                                                ausP.y,
                                                                                                                ausP.x,
                                                                                                                ausP.z,
                                                                                                                ausP.y_stAb,
                                                                                                                ausP.x_stAb,
                                                                                                                ausP.z_stAb)


                + '\n\n')

        newFile.write(
            "\n\t\t\t\t\t\t  " + self.Epoche2.Name + "\t\t\t\t\t\t\t\t\t\t\t\t\t  Genauigkeit\n")
        newFile.write(
            "\t-----------------------------------------------------\t\t\t\t-----------------------------------------\n")
        newFile.write("\tPoint-Nr.\t\t Y\t\t\t\t X\t\t\t\t Z\t\t\t\t\t\t Y\t\t\t\t X\t\t\t\t Z\n")

        ausgPunkte_E2 = self.Epoche2.netzausgleichung.getUnbekannteAsPunkte()

        for i, ausP in enumerate(ausgPunkte_E2):
            newFile.write(
                u'\t\t{0:s}\t\t{1:9.5f}\t\t{2:9.5f}\t\t{3:9.5f}\t\t       {4:9.5f}\t\t{5:9.5f}\t\t{6:9.5f}'.format(
                    ausP.No, ausP.y,
                    ausP.x, ausP.z,
                    ausP.y_stAb,
                    ausP.x_stAb,
                    ausP.z_stAb)
                + '\n\n')

        newFile.write("\n\t\t\t Differenz zu " + self.Epoche1.Name + "\t\t\t\t\t\t\t\t\t\tPunktverschiebung\n")
        newFile.write("\t-----------------------------------------------------\t\t\t\t-----------------\n")
        newFile.write("\tPoint-Nr.\t\t Y\t\t\t\t X\t\t\t\t Z\n")

        difvek = self.difVektor_beschreibung

        for i in range(0, len(difvek), 3):
            newFile.write(
                u'\t\t{0:s}\t\t{1:9.5f}\t\t{2:9.5f}\t\t{3:9.5f}\t\t\t\t  {4:9.5f}'.format(
                    difvek[i][0], difvek[i][1], difvek[i + 1][1], difvek[i + 2][1],
                    np.sqrt((difvek[i][1]) ** 2 + (difvek[i + 1][1]) ** 2 + (difvek[i + 2][1]) ** 2))
                + '\n\n')

        newFile.write("\n\tTestgroesse:\n")
        newFile.write("\t------------\n")
        newFile.write('\t %9.5f' % self.F + "\n\n")
        newFile.write("\tFischerquantil:\n")
        newFile.write("\t---------------\n")
        newFile.write('\t %9.5f' % self.fischer_q)

        if self.F < self.fischer_q:
            newFile.write("\n\n\tTestgroesse < Fischerquantil:\n")
            newFile.write("\t-----------------------------\n")
            newFile.write("\tEs liegt keine Deformation vor.")

        else:
            newFile.write("\n\n\tTestgroesse > Fischerquantil:\n")
            newFile.write("\t-----------------------------\n")
            newFile.write("\tEs liegt eine Deformation vor!\n")

        depo = self.defoPoints

        newFile.write("\n\n\tPoint-Nr\tTestgroesse\t\tFischer-Quantil\n")
        newFile.write("\t--------\t-----------\t\t---------------\n")

        for i in range(0, len(depo), 1):
            newFile.write(
                u'   {0:s}     {1:9.5f}  {2:9.5f}'.format(depo[i][0], depo[i][1], depo[i][2])
                + '\n\n')

        newFile.close()

        return newFile

        #genau_Qxx1[i]["genauy"],genau_Qxx1[i]["genaux"],genau_Qxx1[i]["genauz"])