from distutils.sysconfig import get_python_inc
from logging import basicConfig, DEBUG, info, WARNING
import os
from PySide import QtGui
import sys

from controller.MainWindowController import MainWindowController

__author__ = 'Philipp'
if __name__ == "__main__":
    basicConfig(filename='defoa.log', filemode='a', level=DEBUG,
                format='%(asctime)s - %(name)s - %(levelname)s - %(message)s - %(lineno)d')

    info("-------------------------")
    info("Start App")
    app = QtGui.QApplication.instance()  # checks if QApplication already exists

    if not app:  # create QApplication if it doesnt exist
        app = QtGui.QApplication(sys.argv)

    mySW = MainWindowController()
    icon = QtGui.QIcon('images/final.png')
    mySW.setWindowIcon(icon)
    mySW.show()
    sys.exit(app.exec_())
