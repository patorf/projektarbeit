# -*- coding: utf-8 -*-

from PySide import QtCore, QtGui
import numpy as np
import matplotlib
import sys
# specify the use of PySide
from numpy.linalg import linalg
from model.Trafo import Trafo
from view.Ui_Plotter import Ui_Plotter
from matplotlib import cm

matplotlib.rcParams['backend.qt4'] = "PySide"

# import the figure canvas for interfacing with the backend
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg \
    as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar

# import 3D plotting
from mpl_toolkits.mplot3d import Axes3D  # @UnusedImport
from matplotlib.figure import Figure
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d


class PlotterController(QtGui.QMainWindow):
    def __init__(self, PointList, parent, PointListE2=None, faktor=10):
        self.faktor = faktor

        super(PlotterController, self).__init__(parent)
        self.mainWindow = parent
        self.ui = Ui_Plotter()

        self.ui.setupUi(self)

        #self.ui.plotWidget=Mpwidget_Net(parent=self.ui.plotWidget)
        #Group Widget together
        netplot = True
        vbl = QtGui.QVBoxLayout(self.ui.plotWidget)

        qmc = Mpwidget_Net(parent=self.ui.plotWidget, PointList_E1=PointList, PointList_E2=PointListE2,
                           faktor=self.faktor)

        ntb = NavigationToolbar(qmc, self.ui.plotWidget)

        vbl.addWidget(qmc)
        vbl.addWidget(ntb)

        self.setCentralWidget(self.ui.plotWidget)

        #self.toolbar = NavigationToolbar(self.ui.plotWidget, self)

        #self.addDockWidget(QtCore.Qt.BottomDockWidgetArea,self.toolbar)


        #self.fig=   plt.Figure(figsize=(600,600), dpi=72, facecolor=(1,1,1), edgecolor=(0,0,0))

        #canvas = FigureCanvas(self.fig)
        #self.setCentralWidget(canvas)

        #self.ax = self.fig.add_subplot(111)#, projection='3d')
        #self.ax.plot([0,1])

        # self.ax.set_xlabel('x')
        #self.ax.set_ylabel('y')




        #canvas.draw()



        self.show()


        #s=self.ui.centralWidget.size()
        #self.ui.plotWidget.size()
        #self.ui.plotWidget.setGeometry(1, 1, s.width() - 2, s.height() - 2)


    @classmethod
    def addPoints(cls, ax, points):
        """

        @type points: list of Punkt
        """
        pass
        for point in points:
            pass
            #ax.plot([point.y],[point.x],[point.z])


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        FancyArrowPatch.draw(self, renderer)


class Mpwidget_Net(FigureCanvas):
    def __init__(self, PointList_E1, PointList_E2=None, parent=None, faktor=10):

        """

        @type PointList_E1: list of Punkt
        """
        self.figure = Figure(facecolor=(0, 0, 0))
        # self.figure.fr
        self.faktor = faktor
        super(Mpwidget_Net, self).__init__(self.figure)
        self.setParent(parent)

        alleYWerte = []
        alleXWerte = []
        alleZWerte = []

        # plot random 3D data
        #self.axes = self.figure.add_subplot(111, projection='3d')
        self.axes = Axes3D(self.figure)
        for point in PointList_E1:
            if PointList_E2 == None:
                alleYWerte.append(point.y)
                alleXWerte.append(point.x)
                alleZWerte.append(point.z)

                #if point.Type==0
                self.axes.scatter([point.y], [point.x], [point.z])
                self.axes.text(point.y + 0.1, point.x + 0.1, point.z, str(point.No))

                center = [point.y, point.x, point.z]

                vector1 = point.y + point.konfAxis_A[0] * self.faktor
                vector2 = point.x + point.konfAxis_A[1] * self.faktor
                vector3 = point.z + point.konfAxis_A[2] * self.faktor

                self.axes.plot([center[0], vector1], [center[1], vector2], [center[2], vector3], 'r')

                vector11 = point.y + point.konfAxis_B[0] * self.faktor
                vector22 = point.x + point.konfAxis_B[1] * self.faktor
                vector33 = point.z + point.konfAxis_B[2] * self.faktor

                self.axes.plot([center[0], vector11], [center[1], vector22], [center[2], vector33], 'b')

                vector111 = point.y + point.konfAxis_C[0] * self.faktor
                vector222 = point.x + point.konfAxis_C[1] * self.faktor
                vector333 = point.z + point.konfAxis_C[2] * self.faktor

                self.axes.plot([center[0], vector111], [center[1], vector222], [center[2], vector333], 'g')

                A = np.array(point.qxx)

                U, s, rotation = linalg.svd(A)
                #radii = 1.0/np.sqrt(s)
                radii = [point.konfA_laenge(), point.konfB_laenge(), point.konfC_laenge()]
                #radii = [linalg.norm(point.konfAxis_A), linalg.norm(point.konfAxis_B), linalg.norm(point.konfAxis_C)]

                u = np.linspace(0.0, 2.0 * np.pi, 25)
                v = np.linspace(0.0, np.pi, 25)
                x = radii[0] * np.outer(np.cos(u), np.sin(v)) * self.faktor
                y = radii[1] * np.outer(np.sin(u), np.sin(v)) * self.faktor
                z = radii[2] * np.outer(np.ones_like(u), np.cos(v)) * self.faktor
                for i in range(len(x)):
                    for j in range(len(x)):
                        [x[i, j], y[i, j], z[i, j]] = np.dot([x[i, j], y[i, j], z[i, j]], rotation) + center

                # plot

                self.axes.plot_wireframe(x, y, z, rstride=3, cstride=3, color='b', alpha=0.2)

            else:

                if point.Type == 1:
                    identityPointsForBride = []
                    for pointE2 in PointList_E2:

                        if pointE2.No in ['1', '2', '3', '4', '5', '6']:
                            identityPointsForBride.append(pointE2)

                            alleYWerte.append(pointE2.y)
                            alleXWerte.append(pointE2.x)
                            alleZWerte.append(pointE2.z)

                        if pointE2.No == point.No:


                            self.axes.scatter([point.y], [point.x], [point.z])
                            self.axes.text(point.y + 0.1, point.x + 0.1, point.z, str(point.No))

                            center = [point.y, point.x, point.z]

                            vx = point.y + (pointE2.y - point.y) * self.faktor
                            vy = point.x + (pointE2.x - point.x) * self.faktor
                            vz = point.z + (pointE2.z - point.z) * self.faktor

                            #self.axes.plot([center[0],vx],[center[1],vy],[center[2],vz],'r')

                            a = Arrow3D([center[0], vx], [center[1], vy], [center[2], vz], mutation_scale=20, lw=1,
                                        arrowstyle="-|>", color="g")
                            self.axes.add_artist(a)

                            A = np.array(point.qxx)

                            U, s, rotation = linalg.svd(A)
                            #radii = 1.0/np.sqrt(s)
                            radii = [linalg.norm(point.konfAxis_A), linalg.norm(point.konfAxis_B),
                                     linalg.norm(point.konfAxis_C)]
                            # now carry on with EOL's answer
                            u = np.linspace(0.0, 2.0 * np.pi, 50)
                            v = np.linspace(0.0, np.pi, 50)
                            x = radii[0] * np.outer(np.cos(u), np.sin(v)) * self.faktor
                            y = radii[1] * np.outer(np.sin(u), np.sin(v)) * self.faktor
                            z = radii[2] * np.outer(np.ones_like(u), np.cos(v)) * self.faktor
                            for i in range(len(x)):
                                for j in range(len(x)):
                                    [x[i, j], y[i, j], z[i, j]] = np.dot([x[i, j], y[i, j], z[i, j]], rotation) + center

                            # plot

                            self.axes.plot_wireframe(x, y, z, rstride=3, cstride=3, color='b', alpha=0.2)

                    try:
                        self.plotBridge(self.axes, identityPointsForBride)
                    except:
                        print'brücke geht nicht'

                        # self.data = np.random.random((3, 100))
                        #self.axes.plot(self.data[0, :], self.data[1, :], self.data[2, :])

        #Evtl. noch verändern
        #self.axes.auto_scale_xyz([0,3 ], [0, 3], [0, 3])

        #self.axes.set_xlim3d(-5, 5)
        #self.axes.set_ylim3d(-5,5)
        #self.axes.set_zlim3d(-5,5)

        #Hier wird jetz x mit y vertauscht
        minX = min(alleYWerte)
        maxX = max(alleYWerte)
        deltaX = maxX - minX
        minY = min(alleXWerte)
        maxY = max(alleXWerte)
        deltaY = maxY - minY
        minZ = min(alleZWerte)
        maxZ = max(alleZWerte)
        deltaZ = maxZ - minZ

        maxDelta = max([deltaX, deltaY, deltaZ])

        self.axes.auto_scale_xyz([minX, minX + maxDelta], [minY, minY + maxDelta], [minZ, minZ + maxDelta])
        self.axes.set_xlabel('Y-Achse')
        self.axes.set_ylabel('X-Achse')
        self.axes.set_zlabel('Z-Achse')
        self.axes.set_frame_on(False)


        # self.data = np.random.random((3, 50))
        # self.axes.plot(self.data[0, :], self.data[1, :], self.data[2, :],color='r')
        #ax= Plotter().create3dPlott()
        #Plotter().add_Ellipse(ax)
        #plt.show()
        #Plotter().plottEllipse()

    #PlotterController.add_Ellipse()
    def makeTransformabelList(self, xVector, yVector, zVectro):
        transforamabellist = []
        for i in range(0, len(xVector)):
            transforamabellist.append([xVector[i], yVector[i], zVectro[i]])

        return transforamabellist

    def makePlotableParameter(self, xyz_list):
        xVector = []
        yVector = []
        zVector = []
        for xyz in xyz_list:
            xVector.append(xyz[0])
            yVector.append(xyz[1])
            zVector.append(xyz[2])

        return xVector, yVector, zVector

    def plotBridge(self, ax, identitypoints=0):


        #Prismen von rechts nach links
        Objektpunktey = [0, 4, 4, 4, 4, 0]
        Objektpunktex = [2, 12, 25, 43, 57, 65.5]
        Objektpunktez = [25, 11.5, 11.5, 11.5, 11.5, 45]

        Objektpunktey = [-3, 1, 1, 1, 1, -3]
        Objektpunktex = [2, 15.5, 32.5, 51.5, 69, 82]
        Objektpunktez = [46.5, 22.5, 22.5, 22.5, 22.5, 46.6]

        objTesty = [-3, 1, -3]
        objTestx = [82, 15.5, 2]
        objTestz = [46.5, 22.5, 46.5]

        quellsystem = self.makeTransformabelList(objTesty, objTestx, objTestz)
        quellsystem = self.makeTransformabelList(Objektpunktey, Objektpunktex, Objektpunktez)

        identitypoints_withKey = {}
        zielsystem = []
        for point in identitypoints:
            identitypoints_withKey[int(point.No)] = [point.y, point.x, point.z]
        for pointWithRightKey in identitypoints_withKey:
            zielsystem.append(identitypoints_withKey[pointWithRightKey])

        trafo = Trafo(zielsystem, quellsystem)

        #Bruecke


        #Seile
        factor = 0.1

        sy1 = [2.25, 2.25, 2.25, 4.5, 2.25, 2.25, 2.25]
        sx1 = [-58.5, 0, 4.5, 42, 79.5, 84, 137.5]
        sz1 = [0, 45, 45, 19.75, 45, 45, 0]

        sy2 = [26.75, 26.75, 26.75, 24.5, 26.75, 26.75, 26.75]
        sx2 = [-58.5, 0, 4.5, 42, 79.5, 84, 137.5]
        sz2 = [0, 45, 45, 19.75, 45, 45, 0]

        #Pfeiler

        y = [0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 0, 0, 4.5, 4.5, 4.5, 4.5, 0, 0, 4.5, 4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5,
             24.5, 4.5, 4.5, 4.5]
        x = [0, 0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 0, 0, 4.5, 4.5, 4.5, 0, 0,
             0,
             4.5]
        z = [0, 0, 0, 0, 0, 45, 45, 45, 45, 45, 45, 0, 0, 45, 45, 0, 0, 18, 18, 18, 18, 18, 21.5, 21.5, 21.5, 21.5,
             21.5, 21.5]

        yy = [24.5, 29, 29, 24.5, 24.5, 24.5, 29, 29, 24.5, 24.5, 29, 29, 29, 29, 24.5, 24.5]
        xx = [0, 0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 4.5, 4.5]
        zz = [0, 0, 0, 0, 0, 45, 45, 45, 45, 45, 45, 0, 0, 45, 45, 0]

        yyy = [0, 4.5, 4.5, 0, 0, 0, 4.5, 4.5, 0, 0, 4.5, 4.5, 4.5, 4.5, 0, 0, 4.5, 4.5, 24.5, 24.5, 4.5, 4.5, 4.5,
               24.5,
               24.5, 4.5, 4.5, 4.5]
        xxx = [79.75, 79.75, 84.25, 84.25, 79.75, 79.75, 79.75, 84.25, 84.25, 79.75, 79.75, 79.75, 84.25, 84.25, 84.25,
               84.25, 84.25, 84.25,
               84.25, 79.75, 79.75, 84.25, 84.25, 84.25, 79.75, 79.75, 79.75, 84.25]
        zzz = [0, 0, 0, 0, 0, 45, 45, 45, 45, 45, 45, 0, 0, 45, 45, 0, 0, 18, 18, 18, 18, 18, 21.5, 21.5, 21.5, 21.5,
               21.5, 21.5]

        yyyy = [24.5, 29, 29, 24.5, 24.5, 24.5, 29, 29, 24.5, 24.5, 29, 29, 29, 29, 24.5, 24.5]
        xxxx = [79.75, 79.75, 84.25, 84.25, 79.75, 79.75, 79.75, 84.25, 84.25, 79.75, 79.75, 79.75, 84.25, 84.25, 84.25,
                84.25]
        zzzz = [0, 0, 0, 0, 0, 45, 45, 45, 45, 45, 45, 0, 0, 45, 45, 0]



        #Fahrbahn
        #Fahrbahn

        a1 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b1 = [5, 5, 10.25, 10.25, 5, 5, 5, 5, 10.25, 10.25, 5, 10.25, 10.25, 10.25, 10.25, 5]
        c1 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a2 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b2 = [10.75, 10.75, 16, 16, 10.75, 10.75, 10.75, 10.75, 16, 16, 10.75, 16, 16, 16, 16, 10.75]
        c2 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a3 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b3 = [16.5, 16.5, 21.75, 21.75, 16.5, 16.5, 16.5, 16.5, 21.75, 21.75, 16.5, 21.75, 21.75, 21.75, 21.75, 16.5]
        c3 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a4 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b4 = [22.25, 22.25, 27.5, 27.5, 22.25, 22.25, 22.25, 22.25, 27.5, 27.5, 22.25, 27.5, 27.5, 27.5, 27.5, 22.25]
        c4 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a5 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b5 = [28, 28, 33.25, 33.25, 28, 28, 28, 28, 33.25, 33.25, 28, 33.25, 33.25, 33.25, 33.25, 28]
        c5 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a6 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b6 = [33.75, 33.75, 39, 39, 33.75, 33.75, 33.75, 33.75, 39, 39, 33.75, 39, 39, 39, 39, 33.75]
        c6 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a7 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b7 = [39.5, 39.5, 44.75, 44.75, 39.5, 39.5, 39.5, 39.5, 44.75, 44.75, 39.5, 44.75, 44.75, 44.75, 44.75, 39.5]
        c7 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a8 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b8 = [45.25, 45.25, 50.5, 50.5, 45.25, 45.25, 45.25, 45.25, 50.5, 50.5, 45.25, 50.5, 50.5, 50.5, 50.5, 45.25]
        c8 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a9 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b9 = [51, 51, 56.25, 56.25, 51, 51, 51, 51, 56.25, 56.25, 51, 56.25, 56.25, 56.25, 56.25, 51]
        c9 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a10 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b10 = [56.75, 56.75, 62, 62, 56.75, 56.75, 56.75, 56.75, 62, 62, 56.75, 62, 62, 62, 62, 56.75]
        c10 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a11 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b11 = [62.5, 62.5, 67.75, 67.75, 62.5, 62.5, 62.5, 62.5, 67.75, 67.75, 62.5, 67.75, 67.75, 67.75, 67.75, 62.5]
        c11 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a12 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b12 = [68.25, 68.25, 73.5, 73.5, 68.25, 68.25, 68.25, 68.25, 73.5, 73.5, 68.25, 73.5, 73.5, 73.5, 73.5, 68.25]
        c12 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]

        a13 = [4.5, 24.5, 24.5, 4.5, 4.5, 4.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 4.5, 4.5, 4.5, 4.5]
        b13 = [74, 74, 79.25, 79.25, 74, 74, 74, 74, 79.25, 79.25, 74, 79.25, 79.25, 79.25, 79.25, 74]
        c13 = [21.5, 21.5, 21.5, 21.5, 21.5, 18, 18, 21.5, 21.5, 18, 18, 18, 18, 21.5, 18, 18]






        #

        Referenzpunkty = []
        Referenzpunktx = []
        Referenzpunktz = []

        alpha = 0.03

        newXYZ = trafo.transform(self.makeTransformabelList(y, x, z))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(yy, xx, zz))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(yyy, xxx, zzz))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(yyyy, xxxx, zzzz))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a1, b1, c1))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a2, b2, c2))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a3, b3, c3))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a4, b4, c4))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a5, b5, c5))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)
        newXYZ = trafo.transform(self.makeTransformabelList(a6, b6, c6))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a7, b7, c7))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a8, b8, c8))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a9, b9, c9))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a10, b10, c10))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a11, b11, c11))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a12, b12, c12))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(a13, b13, c13))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(sy1, sx1, sz1))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)

        newXYZ = trafo.transform(self.makeTransformabelList(sy2, sx2, sz2))
        x, y, z = self.makePlotableParameter(newXYZ)
        ax.plot(x, y, z, color='r', alpha=alpha)


        # plot

        #ax.scatter(Objektpunktey, Objektpunktex, Objektpunktez)
        #ax.scatter(Referenzpunkty, Referenzpunktx, Referenzpunktz)
        # ax.plot(y, x, z, color='b' ,alpha=alpha)
        # ax.plot(yy, xx, zz, color='b',alpha=alpha)
        # ax.plot(yyy, xxx, zzz, color='b',alpha=alpha)
        # ax.plot(yyyy, xxxx, zzzz, color='b',alpha=alpha)
        # ax.plot(yyyyy, xxxxx, zzzzz, color='g',alpha=alpha)
        # ax.plot(a1, b1, c1, color='r',alpha=alpha)
        # ax.plot(a2, b2, c2, color='r',alpha=alpha)
        # ax.plot(a3, b3, c3, color='r',alpha=alpha)
        # ax.plot(a4, b4, c4, color='r',alpha=alpha)
        # ax.plot(a5, b5, c5, color='r',alpha=alpha)
        # ax.plot(a6, b6, c6, color='r',alpha=alpha)
        # ax.plot(a7, b7, c7, color='r',alpha=alpha)
        # ax.plot(a8, b8, c8, color='r',alpha=alpha)
        # ax.plot(a9, b9, c9, color='r',alpha=alpha)
        # ax.plot(a10, b10, c10, color='r',alpha=alpha)
        # ax.plot(a11, b11, c11, color='r',alpha=alpha)
        # ax.plot(a12, b12, c12, color='r',alpha=alpha)
        # ax.plot(a13, b13, c13, color='r',alpha=alpha)
        # ax.plot(sy1, sx1, sz1, color='g',alpha=alpha)
        # ax.plot(sy2, sx2, sz2, color='g',alpha=alpha)
