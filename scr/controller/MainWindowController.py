# coding=utf-8
import logging
import os
import pickle
from PySide import QtGui, QtCore
import sys
import math
from controller.AddEpochDialogController import AddEpochDialogController
from controller.MeasureWindowController import MeasureWindowController
from controller.PlotterController import PlotterController
import time
from model.Epoche import Epoche
from model.Deformationsanalyse import Deformationsanalyse
from model.Project import Project
from view.Ui_MainWindow import Ui_MainWindow
import webbrowser

__author__ = 'philippatorf'
logger = logging.getLogger(__name__)


class MainWindowController(QtGui.QMainWindow):
    def __init__(self, parent=None):
        #for the consoel
        self.fs_watcher = QtCore.QFileSystemWatcher(['defoa.log'])
        self.fs_watcher.fileChanged.connect(self.openLog)

        logger.info("initMainWindow")

        super(MainWindowController, self).__init__(parent)
        self.ui = Ui_MainWindow()

        #self.ui.consoleView
        self.ui.setupUi(self)
        self.project = Project()

        self.measureWindow = None
        self.addEpochDialog = None

        self.updateUi()
        self.ui.s0priori_input.setValidator(QtGui.QDoubleValidator())
        self.ui.irtrums_input.setValidator(QtGui.QDoubleValidator())
        self.ui.faktorPlot_input.setValidator(QtGui.QIntValidator())

        self.defoEpochen = []

        #Slot for the updateName Signal
        self.project.updateName.connect(self.updateUi)
        self.project.updateEpoch.connect(self.updateEpochTable)

        self.ui.init_btn.clicked.connect(self.start_initMeasurment)
        self.ui.newEpoch_btn.clicked.connect(self.openAddEpochDialog)

        self.ui.add_btn.clicked.connect(self.addEpoch2Deof)
        self.ui.remove_btn.clicked.connect(self.removeEpochFromDefo)

        self.ui.doDefoAn.clicked.connect(self.startDefo)

        self.ui.openEpochProtokollBtn.clicked.connect(self.printNetAdjustmentProtokoll)
        self.ui.epochTabelView.itemClicked.connect(self.updateEpochInfos)

        self.ui.doAusgleichung_btn.clicked.connect(self.startAusgleichung)

        self.ui.openDefoProtokoll_btn.clicked.connect(self.printDefoProtokoll)

        self.ui.showNetPlot_btn.clicked.connect(self.showNetAdjustmentPlot)

        self.ui.openGraphic3D_btn.clicked.connect(self.showDefoPlot)

        self.ui.testBtn.clicked.connect(self.testBtn)

        self.ui.deleEpoch_btn.clicked.connect(self.deleteEpoche)

        self.ui.consoleCheck.stateChanged.connect(self.openLog)

        #self.openLog()




        #fs_watcher.connect(fs_watcher, QtCore.SIGNAL('fileChanged(QString)'), self.file_changed)


    def openLog(self):
        if self.ui.consoleCheck.isChecked():
            text = open('defoa.log').read()
            self.ui.consleView.setPlainText(text)

            self.ui.consleView.moveCursor(QtGui.QTextCursor.End)
        else:
            self.ui.consleView.clear()

    def testBtn(self):
        print 'testBtn'
        e = self.project.addEpoche()

        self.project.getDemoEpoche1(e)
        e2 = self.project.addEpoche()
        self.project.getDeomoEpcohe2(e2)

    def deleteEpoche(self):
        selecteRowIndex = self.ui.epochTabelView.selectedItems()[0].row()
        epoche = self.project.epochen[selecteRowIndex]
        self.project.removeEpoche(epoche)

        self.updateEpochTable()


    def start_initMeasurment(self):
        self.project.removeAllEpochs()
        self.project.addEpoche()
        self.openMeasureWindow()

    def openMeasureWindow(self):
        if self.measureWindow == None:
            self.measureWindow = MeasureWindowController(self)
            self.measureWindow.onWindowClose.connect(self.enableInitButton)
            #self.measureWindow.close()
        self.measureWindow.show()

        self.ui.init_btn.setEnabled(False)

    def enableInitButton(self):
        #self.measureWindow.deleteLater()
        #del self.measureWindow
        #self.measureWindow=None
        self.ui.init_btn.setEnabled(True)


    def changeProjectName(self):
        self.project.name = 'my'


    def updateUi(self):
        self.setWindowTitle(self.project.name)


    def updateEpochTable(self):

        self.ui.epochTabelView.clearContents()

        self.ui.epochTabelView.setRowCount(len(self.project.epochen))

        for i, epoch in enumerate(self.project.epochen):
            nameItem = QtGui.QTableWidgetItem(epoch.Name)
            rowText = ""
            if epoch.netzausgleichung == None:
                rowText = "noch nicht gestartet"
            elif epoch.netzausgleichung.Globaltest():
                rowText = "JA"
            else:
                rowText = 'NEIN'
            ausgleichungSuccesItem = QtGui.QTableWidgetItem(rowText)

            self.ui.epochTabelView.setItem(i, 0, nameItem)

            self.ui.epochTabelView.setItem(i, 1, ausgleichungSuccesItem)


    def openAddEpochDialog(self):
        if self.addEpochDialog == None:
            self.addEpochDialog = AddEpochDialogController(self)
            #TODO:Close abfangen
        self.addEpochDialog.show()


    def addEpoch2Deof(self):

        selecteRowIndex = self.ui.epochTabelView.selectedItems()[0].row()
        epoche = self.project.epochen[selecteRowIndex]
        #self.ui.defoList.addItem(epoche.Name)

        if len(self.defoEpochen) < 2:
            alreadyInListed = False
            for epochsInList in self.defoEpochen:
                if epochsInList['index'] == selecteRowIndex:
                    alreadyInListed = True

            if not alreadyInListed:
                self.defoEpochen.append({'index': selecteRowIndex, 'name': epoche.Name})


                #self.ui.defoList.addItem(self.defoEpochen[-1]['name'])
        self.updateDefoList()

    def removeEpochFromDefo(self):
        selectedItem = self.ui.defoList.selectedItems()[0]
        selectedRowIndex = self.ui.defoList.indexFromItem(selectedItem).row()
        #self.defoEpochen.remove(selectedRowIndex)
        del self.defoEpochen[selectedRowIndex]
        self.updateDefoList()

    def updateDefoList(self):
        self.ui.defoList.clear()
        for epoch in self.defoEpochen:
            self.ui.defoList.addItem(epoch['name'])


    def startDefo(self):
        epoche1Index = self.defoEpochen[0]['index']
        epoche2Index = self.defoEpochen[1]['index']
        epoche1 = self.project.epochen[epoche1Index]
        epoche2 = self.project.epochen[epoche2Index]

        alpha = 5.0
        alpha_str = self.ui.irtrums_input.text()
        try:

            alpha = float(alpha_str.replace(',', '.'))
        except:
            logger.warning('can not convert to float')

        self.project.defoAnalyse = Deformationsanalyse(epoche1, epoche2, alpha / 100)

        self.project.defoAnalyse.checkGrundgesamtheit()
        if self.project.defoAnalyse.grundgesamtheit:
            self.project.defoAnalyse.defoAnalyse()

            self.updateDefoTable()

        else:
            logger.info('Deformationsanalyse nicht Möglich')


    def updateEpochInfos(self):
        print self.ui.epochTabelView.selectedItems()
        selecteRowIndex = self.ui.epochTabelView.selectedItems()[0].row()
        epoche = self.project.epochen[selecteRowIndex]

        #Update StationCount

        statonCount = len(epoche.stations)
        stationCountLabel = 'Anzahl Stationen (Zielpunkte): ' + str(statonCount) + ' ('
        for station in epoche.stations:
            station.Satzauswertung()
            targetCount = len(station.stationsMittel)
            stationCountLabel += str(targetCount) + '/'
        stationCountLabel = stationCountLabel[:-1]
        stationCountLabel += ')'

        self.ui.epochInfo_label_StationCount.setText(stationCountLabel)


        #Update Label AdjustmentSuccess
        unbekanntenCount = 'Anzahl Unbekannte: '
        beobachtungsCount = 'Anzahl Beobachtungen: '
        datumsdefekt = 'Datumsdefekt: '
        globalTest = 'Globaltest: '
        epochStatus = 'Ausgleichung erfolgreich: '
        s0priori_text = 's0 a priori: '
        s0post_test = 's0 a posteriori: '
        if epoche.netzausgleichung == None:
            epochStatus += 'noch nicht gestartet'
        elif epoche.netzausgleichung.Globaltest():
            epochStatus += 'JA'

            unbekanntenCount += str(len(epoche.netzausgleichung.ausUnbekannten))
            beobachtungsCount += str(len(epoche.netzausgleichung.beobachtungen))
            datumsdefekt += str(epoche.netzausgleichung.datumsdefekt)
            globaTestSucces, T, chi = epoche.netzausgleichung.Globaltest()
            s0priori_text += "{0:.5f}".format(epoche.netzausgleichung.S0priori)
            s0post_test += "{0:.5f}".format(epoche.netzausgleichung.S0post)
            if globaTestSucces:
                t_string = "{0:.3f}".format(T)
                chi_string = "{0:.3f}".format(chi)
                globalTest += 'Erfolgreich, da ' + t_string + '<' + chi_string
            else:
                t_string = "{0:.3f}".format(T)
                chi_string = "{0:.3f}".format(chi)
                globalTest += 'nicht Erfolgreich, da ' + t_string + '>' + chi_string


        else:
            epochStatus += 'NEIN'
        self.ui.epochInfo_label_AdjustmentSuccess.setText(epochStatus)
        self.ui.epochInfo_label_UnbekanntenCount.setText(unbekanntenCount)
        self.ui.epochInfo_label_BeobachtungCount.setText(beobachtungsCount)
        self.ui.epochInfo_label_Datumsdefkt.setText(datumsdefekt)
        self.ui.epochInfo_label_Globaltest.setText(globalTest)
        self.ui.epochInfo_label_s0Pri.setText(s0priori_text)
        self.ui.epochInfo_label_S0post.setText(s0post_test)

    def startAusgleichung(self):

        selecteRowIndex = self.ui.epochTabelView.selectedItems()[0].row()
        epoche = self.project.epochen[selecteRowIndex]
        for station in epoche.stations:
            station.stationsMittelProtokoll(epoche.Name + '_' + station.No)

        s0priori = self.ui.s0priori_input.text()

        s0priori_float = 1
        try:
            s0priori_float = float(s0priori.replace(',', '.'))
        except:
            logger.warning('can not convert to float')

        winkelGenauigkeit = float(self.ui.winkelGenu_input.text())
        winkelGenauigkeit_rad = winkelGenauigkeit * math.pi / 200
        streckenGenauigkeit = float(self.ui.streckenGenau_input.text())


        #Damit alle Epochen die gleichen Näherungswerte haben muss die Epoche 0 zuerst ausgeglicehn werden
        if selecteRowIndex == 0:
            epoche.startAusgleichung(s0Priori=s0priori_float, winkel_genauigkeit=winkelGenauigkeit_rad,
                                     strecken_genauigkeit=streckenGenauigkeit)
        else:

            epoche0 = self.project.epochen[0]
            if epoche0.netzausgleichung is None:
                epoche0.startAusgleichung(s0Priori=s0priori_float, winkel_genauigkeit=winkelGenauigkeit_rad,
                                          strecken_genauigkeit=streckenGenauigkeit)
            aproxUnbe = epoche0.getKoordinaten()
            epoche.startAusgleichung(NaeherungswerteDesErstenEpoche=aproxUnbe, s0Priori=s0priori_float,
                                     winkel_genauigkeit=winkelGenauigkeit_rad,
                                     strecken_genauigkeit=streckenGenauigkeit)

        self.ui.epochTabelView.selectRow(selecteRowIndex)
        self.updateEpochInfos()


    def printNetAdjustmentProtokoll(self):
        selecteRowIndex = self.ui.epochTabelView.selectedItems()[0].row()
        epoche = self.project.epochen[selecteRowIndex]

        file = epoche.netzausgleichung.printProtokol(epoche.Name + "_Ausgleichung")

        path = os.path.abspath(file.name)

        webbrowser.open_new_tab('file://' + path)


    def printDefoProtokoll(self):

        file = self.project.defoAnalyse.printProtokol()
        path = os.path.abspath(file.name)

        webbrowser.open_new_tab('file://' + path)


    def updateDefoTable(self):

        self.ui.DefoInfoTabel.clearContents()

        defo = self.project.defoAnalyse
        difVectorBeschreibung = defo.difVektor_beschreibung  #yxz

        self.ui.defoStatus_label.setText(defo.status)
        self.ui.DefoInfoTabel.setRowCount(len(difVectorBeschreibung) / 3)

        j = 0
        for i in range(0, len(difVectorBeschreibung), 3):

            pointNo = QtGui.QTableWidgetItem(difVectorBeschreibung[i][0])

            deltaY = QtGui.QTableWidgetItem('%.4f' % difVectorBeschreibung[i][1])
            deltaX = QtGui.QTableWidgetItem('%.4f' % difVectorBeschreibung[i + 1][1])
            deltaZ = QtGui.QTableWidgetItem('%.4f' % difVectorBeschreibung[i + 2][1])

            delta_S = math.sqrt(
                difVectorBeschreibung[i][1] ** 2 + difVectorBeschreibung[i + 1][1] ** 2 + difVectorBeschreibung[i + 2][
                    1] ** 2)
            deltaS_item = QtGui.QTableWidgetItem('%.4f' % delta_S)

            defoVorhanden = QtGui.QTableWidgetItem('Nein')
            defoVorhanden.setBackground(QtGui.QColor('green').lighter())

            for defopoint in defo.defoPoints:
                if difVectorBeschreibung[i][0] == defopoint[0]:
                    testgro = defopoint[1]
                    fisher = defopoint[2]

                    test_item = QtGui.QTableWidgetItem('%.4f' % testgro)
                    fisher_item = QtGui.QTableWidgetItem('%.4f' % fisher)

                    self.ui.DefoInfoTabel.setItem(j, 5, test_item)
                    self.ui.DefoInfoTabel.setItem(j, 6, fisher_item)

                    defoVorhanden = QtGui.QTableWidgetItem('Ja')

                    defoVorhanden.setBackground(QtGui.QColor('red').lighter())

            self.ui.DefoInfoTabel.setItem(j, 0, pointNo)
            self.ui.DefoInfoTabel.setItem(j, 1, deltaY)
            self.ui.DefoInfoTabel.setItem(j, 2, deltaX)
            self.ui.DefoInfoTabel.setItem(j, 3, deltaZ)
            self.ui.DefoInfoTabel.setItem(j, 4, deltaS_item)

            self.ui.DefoInfoTabel.setItem(j, 7, defoVorhanden)

            j += 1


    def showNetAdjustmentPlot(self):
        selecteRowIndex = self.ui.epochTabelView.selectedItems()[0].row()
        epoche = self.project.epochen[selecteRowIndex]
        #epoche.netzausgleichung.plot3dMap(self)
        pointList = epoche.netzausgleichung.getUnbekannteAsPunkte()

        faktor = 100
        try:
            faktor = int(self.ui.faktorPlot_input.text())
        except:
            pass

        plotter = PlotterController(parent=self, PointList=pointList, faktor=faktor)
        plotter.show()


    def showDefoPlot(self):

        defoAn = self.project.defoAnalyse

        pointList1 = defoAn.Epoche1.netzausgleichung.getUnbekannteAsPunkte()
        pointList2 = defoAn.Epoche2.netzausgleichung.getUnbekannteAsPunkte()

        faktor = 100
        try:
            faktor = int(self.ui.faktorPlot_input.text())
        except:
            pass
        plotter = PlotterController(parent=self, PointList=pointList1, PointListE2=pointList2, faktor=faktor)
        plotter.show()






