from PySide.QtCore import QThread
from model.Epoche import Epoche
from model.Station import Station

__author__ = 'philippatorf'
from PySide import QtGui, QtCore
import logging
from view.Ui_AddEpochDialog import Ui_addEpochDialog

logger = logging.getLogger(__name__)


class AddEpochDialogController(QtGui.QMainWindow):
    def __init__(self, parent):

        super(AddEpochDialogController, self).__init__(parent)
        self.ui = Ui_addEpochDialog()
        self.mainWindow = parent

        self.ui.setupUi(self)

        self.ui.measure_btn.clicked.connect(self.measureNewSets_withCount)
        self.ui.cancel_Btn.clicked.connect(self.close)
        self.threads = []

    def getThread(self):
        newThread = QThread()
        newThread.finished.connect(lambda: self.deletThread(newThread))
        self.threads.append(newThread)
        return newThread


    def deletThread(self, thread):
        if thread in self.threads:
            print "delete Thread"

            self.threads.remove(thread)


    def startMeasurment(self):

        setCount = self.ui.SetCount_spinBox.value()
        #fullSet = self.ui.FullSet_checkBox.isChecked()
        self.ui.progressBar.setMaximum(0)
        referenceEpoch = self.mainWindow.project.epochen[0]
        project = self.mainWindow.project
        newEpoch = project.addNewEpochwithSetUpFromOtherEpoch(referenceEpoch)
        assert isinstance(newEpoch, Epoche)
        for i in range(0, setCount):

            thisThreads = []
            for station in newEpoch.stations:
                mythread = self.getThread()
                mythread.run = lambda: station.measureAdditionalSet()
                mythread.start()
                thisThreads.append(mythread);

            for thread in thisThreads:
                thread.wait()
                self.ui.progressBar.setMaximum(100)


    def measureNewSets_withCount(self):


        numberOfSets = self.ui.SetCount_spinBox.value()
        referenceEpoch = self.mainWindow.project.epochen[0]
        project = self.mainWindow.project
        project.addNewEpochwithSetUpFromOtherEpoch(referenceEpoch)

        mythread = self.getThread()
        mythread.run = lambda: self.measureNewSets(numberOfSets)
        mythread.start()

    def measureNewSets(self, numberOfSets):
        #recursive Funktion

        if numberOfSets != 0:
            th1 = self.getThread()
            th2 = self.getThread()

            th1.run = lambda: self.mainWindow.project.getCurrentEpoch().stations[0].measureAdditionalSet()
            th2.run = lambda: self.mainWindow.project.getCurrentEpoch().stations[1].measureAdditionalSet()
            self.ui.progressBar.setMaximum(0)
            th2.start()
            th1.start()

            th1.wait()
            th2.wait()
            self.ui.progressBar.setMaximum(100)

            self.measureNewSets(numberOfSets - 1)





