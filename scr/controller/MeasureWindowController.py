# coding=utf-8
from PySide import QtGui, QtCore
import logging
import sys
from PySide.QtCore import QThread
from model.Messdaten import Messdaten
from model.Tachy import Tachy

from view.Ui_MeasureWindow import Ui_measureWindow

import time

__author__ = 'philippatorf'

logger = logging.getLogger(__name__)


class MeasureWindowController(QtGui.QMainWindow):
    onWindowClose = QtCore.Signal()
    incresPktNo = QtCore.Signal(int)  #StationNo

    def __init__(self, parent):
        """


        @type parent: MainWindowController
        @param parent:
        """
        super(MeasureWindowController, self).__init__(parent)

        self.ui = Ui_measureWindow()
        self.mainWindow = parent

        self.tachy1 = Tachy()
        self.tachy2 = Tachy()

        self.ui.setupUi(self)

        self.tachy1.updateStatus.connect(self.updateTachy_1_StatusLabel)
        self.tachy2.updateStatus.connect(self.updateTachy_2_StatusLabel)

        self.ui.connect_btn_1.clicked.connect(lambda: self.connectTachy(1))
        self.ui.connect_btn_2.clicked.connect(lambda: self.connectTachy(2))

        self.ui.measure_btn_1.clicked.connect(lambda: self.measurePointinParallel(1))
        self.ui.measure_btn_2.clicked.connect(lambda: self.measurePointinParallel(2))

        self.ui.completeSet_btn_1.clicked.connect(lambda: self.runCompleteSetInParallel(1))
        self.ui.completeSet_btn_2.clicked.connect(lambda: self.runCompleteSetInParallel(2))

        self.ui.identifyTachy_1.clicked.connect(lambda: self.itentifyTachy(1))
        self.ui.identifyTachy_2.clicked.connect(lambda: self.itentifyTachy(2))

        self.ui.measureAdditionsSet.clicked.connect(self.measureNewSets_withCount)

        self.incresPktNo.connect(self.increasPointNo)

        self.setUpComPortCmb()
        self.setUpPrismCmb()
        self.setUpPointTypeCombo()


        #Thread test
        self.parallelTestArray1 = []
        self.parallelTestArray2 = []

        self.threads = []


    def getThread(self):
        newThread = QThread()
        newThread.finished.connect(lambda: self.deletThread(newThread))
        self.threads.append(newThread)
        return newThread


    def deletThread(self, thread):
        if thread in self.threads:
            print "delete Thread"
            self.threads.remove(thread)


    def closeEvent(self, *args, **kwargs):
        self.onWindowClose.emit()


    def setUpComPortCmb(self):

        """
        create 2 new Station with 2 new Tachys
        fill the ComPort Combo Boxes


        """

        currentEpoch = self.mainWindow.project.getCurrentEpoch()
        station1 = currentEpoch.addNewStation()
        station2 = currentEpoch.addNewStation()

        station1.pointsUpdated.connect(lambda: self.updateTable(1))
        station2.pointsUpdated.connect(lambda: self.updateTable(2))

        station1.finishedLearning.connect(self.learningFinished)

        station1.tachy = self.tachy1
        station2.tachy = self.tachy2

        self.ui.prism_cmb_1.clear()
        self.ui.prism_cmb_2.clear()
        print self.tachy1.availablePorts
        for ports in self.tachy1.availablePorts:
            self.ui.portSelect_cmb_1.addItem(ports[0])

        for ports in self.tachy2.availablePorts:
            self.ui.portSelect_cmb_2.addItem(ports[0])

    def increasPointNo(self, stationNo):
        pktInput = None
        if stationNo == 1:

            pktInput = self.ui.pktNo_input_1

        elif stationNo == 2:
            pktInput = self.ui.pktNo_input_2
        if pktInput:
            pointNo = pktInput.toPlainText()

            if pointNo.isdigit():
                pointAsInt = int(pointNo)
                pointAsInt += 1
                pointNo = str(pointAsInt)
                pktInput.setPlainText(pointNo)

    def setUpPrismCmb(self):
        prismTypes = self.tachy1.getAvailablePrismTypes()

        self.ui.prism_cmb_1.clear()
        self.ui.prism_cmb_2.clear()

        for prismNom, prismName in prismTypes.iteritems():
            self.ui.prism_cmb_1.addItem(prismName)
            self.ui.prism_cmb_2.addItem(prismName)

    def setUpPointTypeCombo(self):

        pointTypes = self.mainWindow.project.getAvailablePointTypes()

        self.ui.pointType_cmb_1.clear()
        self.ui.pointType_cmb_2.clear()

        for typeNo, typeName in pointTypes.iteritems():
            self.ui.pointType_cmb_1.addItem(typeName)
            self.ui.pointType_cmb_2.addItem(typeName)


    def connectTachy(self, tachyNo):

        if tachyNo == 1:
            portName = self.ui.portSelect_cmb_1.currentText()
            self.tachy1.initSerial(portName)
            self.tachy1.changeToFace(0)

        if tachyNo == 2:
            portName = self.ui.portSelect_cmb_2.currentText()
            self.tachy2.initSerial(portName)
            self.tachy2.changeToFace(0)

    def updateTachy_1_StatusLabel(self, status):

        self.ui.status_lb_1.setText(status)

    def updateTachy_2_StatusLabel(self, status):
        self.ui.status_lb_2.setText(status)

    def measurePointinParallel(self, stationNo):
        mythread = self.getThread()
        mythread.run = lambda: self.measurePoint(stationNo)
        mythread.start()

    def measurePoint(self, stationNo):

        print(stationNo)
        if stationNo == 1 and self.tachy1.ready:
            print ('drin')
            self.setProgressBarBusy(1)
            self.ui.measure_btn_1.setDisabled(True)
            pointNo = self.ui.pktNo_input_1.toPlainText()
            pointTypeName = self.ui.pointType_cmb_1.currentText()
            currentPrismName = self.ui.prism_cmb_1.currentText()


            #get the pointTypeNo for  the PointType  in the Cmb
            pointTypes = self.mainWindow.project.getAvailablePointTypes()
            pointTypeNo = None
            for typeNo, typeName in pointTypes.iteritems():
                if typeName == pointTypeName:
                    pointTypeNo = typeNo

            #get the prismTypeNo for the PrismTypeName in the Cmb
            prismTypes = self.tachy1.getAvailablePrismTypes()
            currentPrismNo = None
            for prismNo, prismName in prismTypes.iteritems():
                if currentPrismName == prismName:
                    currentPrismNo = prismNo

            station1 = self.mainWindow.project.getCurrentEpoch().stations[0]
            #station2 = self.mainWindow.project.getCurrentEpoch().stations[1]
            station1.defineTarget(pointNo, currentPrismNo, pointTypeNo)

            self.incresPktNo.emit(stationNo)
            self.ui.measure_btn_1.setDisabled(False)

            self.setProgressBarBusy(1, False)


        elif stationNo == 2 and self.tachy2.ready:
            self.setProgressBarBusy(2)
            self.ui.measure_btn_2.setDisabled(True)

            pointNo = self.ui.pktNo_input_2.toPlainText()
            pointTypeName = self.ui.pointType_cmb_2.currentText()
            currentPrismName = self.ui.prism_cmb_2.currentText()


            #get the pointTypeNo for  the PointType  in the Cmb
            pointTypes = self.mainWindow.project.getAvailablePointTypes()
            pointTypeNo = None
            for typeNo, typeName in pointTypes.iteritems():
                if typeName == pointTypeName:
                    pointTypeNo = typeNo

            #get the prismTypeNo for the PrismTypeName in the Cmb
            prismTypes = self.tachy2.getAvailablePrismTypes()
            currentPrismNo = None
            for prismNo, prismName in prismTypes.iteritems():
                if currentPrismName == prismName:
                    currentPrismNo = prismNo

            #station1 = self.mainWindow.project.getCurrentEpoch().stations[0]
            station2 = self.mainWindow.project.getCurrentEpoch().stations[1]

            station2.defineTarget(pointNo, currentPrismNo, pointTypeNo)
            self.incresPktNo.emit(stationNo)

            self.setProgressBarBusy(2, False)

            self.ui.measure_btn_2.setDisabled(False)


    def updateTable(self, stationNo):

        currentEpoch = self.mainWindow.project.getCurrentEpoch()
        station = None
        tabel = None
        if stationNo == 1:
            station = currentEpoch.stations[0]
            tabel = self.ui.pointTable1

        elif stationNo == 2:
            station = currentEpoch.stations[1]
            tabel = self.ui.pointTable2

        tabel.clearContents()
        firstSet = station.getFirstVollsatz()

        #Prism Hash Table
        prismTypes = self.tachy1.getAvailablePrismTypes()
        #Point Types HashTabel
        pointTypes = self.mainWindow.project.getAvailablePointTypes()

        measuremntCounts = station.getCoutOfMeasurements()

        tabel.setRowCount(len(firstSet.Halbsaetze[1]))
        #self.ui.pointTable1.setSortingEnabled()
        for j, messdaten in enumerate(firstSet.Halbsaetze[1]):


            nameItem = QtGui.QTableWidgetItem(str(messdaten.TargetNo))




            #todo:Noch nicht getestet
            typeItem = QtGui.QTableWidgetItem(str(pointTypes[messdaten.Type]))
            prismItem = QtGui.QTableWidgetItem(str(prismTypes[messdaten.PrismType]))

            firstCircel = u"\u25D0"
            fullCircle = u"\u25CF"
            #
            count = measuremntCounts[messdaten.TargetNo]['count']
            fullSet = measuremntCounts[messdaten.TargetNo]['vollsatz']
            #
            setCount = u""
            if fullSet:
                if count % 2 != 0:
                    coutOfFullSets = int(count / 2)
                    for i in range(0, coutOfFullSets):
                        setCount += fullCircle

                    setCount += firstCircel
                else:
                    for i in range(0, count / 2):
                        setCount += fullCircle
            else:
                for i in range(0, count):
                    setCount += firstCircel

            #setCount=u"\u25CF"+u"\u25CF"+u"\u25CF"+u"\u25CF"
            #print messdaten
            #print count

            circle = QtGui.QTableWidgetItem(setCount)

            tabel.setItem(j, 0, nameItem)
            tabel.setItem(j, 1, typeItem)
            tabel.setItem(j, 2, prismItem)
            tabel.setItem(j, 3, circle)


            #circle = QtGui.QTableWidgetItem("hallo")
            #font = QtGui.QFont()
            #font.setFamily("LucidaSansUnicode")
            #font.setPointSize(14)

            #circle.setFont(font)


    def runCompleteSetInParallel(self, stationNo):

        myThread = self.getThread()
        myThread.run = lambda: self.completeSet(stationNo)
        myThread.start()

        if stationNo == 1:
            self.ui.measure_btn_1.setDisabled(True)
            self.ui.completeSet_btn_1.setDisabled(True)
        else:
            self.ui.measure_btn_2.setDisabled(True)
            self.ui.completeSet_btn_2.setDisabled(True)

    def completeSet(self, stationNo):


        currentEpoch = self.mainWindow.project.getCurrentEpoch()
        station = None
        if stationNo == 1:
            #TO show a buyi progressbar

            station = currentEpoch.stations[0]

        elif stationNo == 2:
            #self.ui.progressBar_1.setMaximum(0)
            station = currentEpoch.stations[1]

        #reply = QtGui.QMessageBox.question(self,
        #                                   "Hinweis", u"Wollen Sie den Satz mit einem zweiten Halbsatz abschließen?",
        #                                   QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)
        reply = QtGui.QMessageBox.Yes
        if reply == QtGui.QMessageBox.Yes:
            self.setProgressBarBusy(stationNo)
            time.sleep(3)
            try:
                station.completeFirstSet()
            except:
                logger.error('Satz konnte nicht beendet werden')
                print sys.exc_info()[0]

        else:
            station.learningFinished = True

        self.setProgressBarBusy(stationNo, False)

    def setProgressBarBusy(self, stationNo, busy=True):
        if stationNo == 1:
            if busy:
                self.ui.progressBar_1.setMaximum(0)
            else:
                self.ui.progressBar_1.setMaximum(100)
        elif stationNo == 2:
            if busy:
                self.ui.progressBar_2.setMaximum(0)
            else:
                self.ui.progressBar_2.setMaximum(100)


    def measureNewSets_withCount(self):


        numberOfSets = self.ui.umberOfSets_spinBox.value()
        mythread = self.getThread()
        mythread.run = lambda: self.measureNewSets(numberOfSets)
        mythread.start()


    def measureNewSets(self, numberOfSets):
        #recursive Funktion

        if numberOfSets != 0:
            th1 = self.getThread()
            th2 = self.getThread()
            th1.run = lambda: self.mainWindow.project.getCurrentEpoch().stations[0].measureAdditionalSet()
            th2.run = lambda: self.mainWindow.project.getCurrentEpoch().stations[1].measureAdditionalSet()
            self.setProgressBarBusy(1)
            self.setProgressBarBusy(2)
            th2.start()
            th1.start()

            th1.wait()
            th2.wait()
            self.setProgressBarBusy(1, False)
            self.setProgressBarBusy(2, False)

            self.measureNewSets(numberOfSets - 1)


    def learningFinished(self):
        #todo: disabel station elements

        print "kajsdfjöj"


    def itentifyTachy(self, stationNo):

        if stationNo == 1:
            self.mainWindow.project.getCurrentEpoch().stations[0].tachy.changeFace()
        else:
            self.mainWindow.project.getCurrentEpoch().stations[1].tachy.changeFace()







