# coding=utf-8
from PySide import QtCore, QtGui
import sys

from PySide import QtCore, QtGui

from PySide import QtCore, QtGui

from PySide import QtCore, QtGui

from PySide import QtCore, QtGui


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(985, 721)
        self.centralWidget = QtGui.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.newEpoch_btn = QtGui.QPushButton(self.centralWidget)
        self.newEpoch_btn.setGeometry(QtCore.QRect(250, 10, 114, 32))
        self.newEpoch_btn.setObjectName("newEpoch_btn")
        self.defoList = QtGui.QListWidget(self.centralWidget)
        self.defoList.setGeometry(QtCore.QRect(450, 50, 121, 41))
        self.defoList.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.defoList.setObjectName("defoList")
        self.init_btn = QtGui.QPushButton(self.centralWidget)
        self.init_btn.setGeometry(QtCore.QRect(20, 10, 114, 32))
        self.init_btn.setObjectName("init_btn")
        self.doDefoAn = QtGui.QPushButton(self.centralWidget)
        self.doDefoAn.setGeometry(QtCore.QRect(450, 100, 131, 32))
        self.doDefoAn.setObjectName("doDefoAn")
        self.add_btn = QtGui.QPushButton(self.centralWidget)
        self.add_btn.setGeometry(QtCore.QRect(390, 50, 41, 31))
        self.add_btn.setObjectName("add_btn")
        self.remove_btn = QtGui.QPushButton(self.centralWidget)
        self.remove_btn.setGeometry(QtCore.QRect(390, 90, 41, 31))
        self.remove_btn.setObjectName("remove_btn")
        self.epochTabelView = QtGui.QTableWidget(self.centralWidget)
        self.epochTabelView.setGeometry(QtCore.QRect(20, 50, 341, 161))
        self.epochTabelView.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.epochTabelView.setAlternatingRowColors(True)
        self.epochTabelView.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.epochTabelView.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.epochTabelView.setObjectName("epochTabelView")
        self.epochTabelView.setColumnCount(2)
        self.epochTabelView.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.epochTabelView.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.epochTabelView.setHorizontalHeaderItem(1, item)
        self.epochTabelView.horizontalHeader().setDefaultSectionSize(140)
        self.epochTabelView.horizontalHeader().setStretchLastSection(True)
        self.DefoInfoTabel = QtGui.QTableWidget(self.centralWidget)
        self.DefoInfoTabel.setGeometry(QtCore.QRect(400, 160, 561, 321))
        self.DefoInfoTabel.setObjectName("DefoInfoTabel")
        self.DefoInfoTabel.setColumnCount(8)
        self.DefoInfoTabel.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.DefoInfoTabel.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.DefoInfoTabel.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.DefoInfoTabel.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.DefoInfoTabel.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.DefoInfoTabel.setHorizontalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.DefoInfoTabel.setHorizontalHeaderItem(5, item)
        item = QtGui.QTableWidgetItem()
        self.DefoInfoTabel.setHorizontalHeaderItem(6, item)
        item = QtGui.QTableWidgetItem()
        self.DefoInfoTabel.setHorizontalHeaderItem(7, item)
        self.DefoInfoTabel.horizontalHeader().setDefaultSectionSize(67)
        self.DefoInfoTabel.horizontalHeader().setStretchLastSection(True)
        self.EpochInfoGroupBox = QtGui.QGroupBox(self.centralWidget)
        self.EpochInfoGroupBox.setGeometry(QtCore.QRect(20, 220, 341, 211))
        self.EpochInfoGroupBox.setObjectName("EpochInfoGroupBox")
        self.epochInfo_label_StationCount = QtGui.QLabel(self.EpochInfoGroupBox)
        self.epochInfo_label_StationCount.setGeometry(QtCore.QRect(20, 30, 301, 16))
        self.epochInfo_label_StationCount.setObjectName("epochInfo_label_StationCount")
        self.epochInfo_label_AdjustmentSuccess = QtGui.QLabel(self.EpochInfoGroupBox)
        self.epochInfo_label_AdjustmentSuccess.setGeometry(QtCore.QRect(20, 70, 301, 16))
        self.epochInfo_label_AdjustmentSuccess.setObjectName("epochInfo_label_AdjustmentSuccess")
        self.epochInfo_label_UnbekanntenCount = QtGui.QLabel(self.EpochInfoGroupBox)
        self.epochInfo_label_UnbekanntenCount.setGeometry(QtCore.QRect(20, 90, 301, 16))
        self.epochInfo_label_UnbekanntenCount.setObjectName("epochInfo_label_UnbekanntenCount")
        self.epochInfo_label_Datumsdefkt = QtGui.QLabel(self.EpochInfoGroupBox)
        self.epochInfo_label_Datumsdefkt.setGeometry(QtCore.QRect(20, 130, 301, 16))
        self.epochInfo_label_Datumsdefkt.setObjectName("epochInfo_label_Datumsdefkt")
        self.epochInfo_label_BeobachtungCount = QtGui.QLabel(self.EpochInfoGroupBox)
        self.epochInfo_label_BeobachtungCount.setGeometry(QtCore.QRect(20, 110, 301, 16))
        self.epochInfo_label_BeobachtungCount.setObjectName("epochInfo_label_BeobachtungCount")
        self.epochInfo_label_Globaltest = QtGui.QLabel(self.EpochInfoGroupBox)
        self.epochInfo_label_Globaltest.setGeometry(QtCore.QRect(20, 150, 301, 16))
        self.epochInfo_label_Globaltest.setObjectName("epochInfo_label_Globaltest")
        self.label_7 = QtGui.QLabel(self.EpochInfoGroupBox)
        self.label_7.setGeometry(QtCore.QRect(20, 50, 301, 16))
        self.label_7.setObjectName("label_7")
        self.epochInfo_label_s0Pri = QtGui.QLabel(self.EpochInfoGroupBox)
        self.epochInfo_label_s0Pri.setGeometry(QtCore.QRect(20, 170, 301, 16))
        self.epochInfo_label_s0Pri.setObjectName("epochInfo_label_s0Pri")
        self.epochInfo_label_S0post = QtGui.QLabel(self.EpochInfoGroupBox)
        self.epochInfo_label_S0post.setGeometry(QtCore.QRect(20, 190, 301, 16))
        self.epochInfo_label_S0post.setObjectName("epochInfo_label_S0post")
        self.line = QtGui.QFrame(self.centralWidget)
        self.line.setGeometry(QtCore.QRect(370, 0, 20, 541))
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")
        self.line_2 = QtGui.QFrame(self.centralWidget)
        self.line_2.setGeometry(QtCore.QRect(0, 530, 1001, 16))
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.openGraphic3D_btn = QtGui.QPushButton(self.centralWidget)
        self.openGraphic3D_btn.setGeometry(QtCore.QRect(400, 490, 114, 32))
        self.openGraphic3D_btn.setObjectName("openGraphic3D_btn")
        self.openDefoProtokoll_btn = QtGui.QPushButton(self.centralWidget)
        self.openDefoProtokoll_btn.setGeometry(QtCore.QRect(520, 490, 131, 32))
        self.openDefoProtokoll_btn.setObjectName("openDefoProtokoll_btn")
        self.openEpochProtokollBtn = QtGui.QPushButton(self.centralWidget)
        self.openEpochProtokollBtn.setGeometry(QtCore.QRect(240, 430, 131, 32))
        self.openEpochProtokollBtn.setObjectName("openEpochProtokollBtn")
        self.consleView = QtGui.QPlainTextEdit(self.centralWidget)
        self.consleView.setGeometry(QtCore.QRect(10, 550, 821, 101))
        font = QtGui.QFont()
        font.setFamily("Courier")
        font.setPointSize(11)
        self.consleView.setFont(font)
        self.consleView.setLineWrapMode(QtGui.QPlainTextEdit.NoWrap)
        self.consleView.setReadOnly(True)
        self.consleView.setCenterOnScroll(True)
        self.consleView.setObjectName("consleView")
        self.doAusgleichung_btn = QtGui.QPushButton(self.centralWidget)
        self.doAusgleichung_btn.setGeometry(QtCore.QRect(210, 500, 161, 32))
        self.doAusgleichung_btn.setObjectName("doAusgleichung_btn")
        self.showNetPlot_btn = QtGui.QPushButton(self.centralWidget)
        self.showNetPlot_btn.setGeometry(QtCore.QRect(240, 460, 131, 32))
        self.showNetPlot_btn.setObjectName("showNetPlot_btn")
        self.testBtn = QtGui.QPushButton(self.centralWidget)
        self.testBtn.setGeometry(QtCore.QRect(960, 640, 20, 20))
        self.testBtn.setText("")
        self.testBtn.setObjectName("testBtn")
        self.defoStatus_label = QtGui.QLabel(self.centralWidget)
        self.defoStatus_label.setGeometry(QtCore.QRect(400, 140, 511, 16))
        self.defoStatus_label.setText("")
        self.defoStatus_label.setObjectName("defoStatus_label")
        self.irtrums_input = QtGui.QLineEdit(self.centralWidget)
        self.irtrums_input.setGeometry(QtCore.QRect(590, 50, 51, 21))
        self.irtrums_input.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.irtrums_input.setObjectName("irtrums_input")
        self.label = QtGui.QLabel(self.centralWidget)
        self.label.setGeometry(QtCore.QRect(660, 50, 181, 16))
        self.label.setObjectName("label")
        self.deleEpoch_btn = QtGui.QPushButton(self.centralWidget)
        self.deleEpoch_btn.setGeometry(QtCore.QRect(243, 210, 121, 32))
        self.deleEpoch_btn.setObjectName("deleEpoch_btn")
        self.faktorPlot_input = QtGui.QLineEdit(self.centralWidget)
        self.faktorPlot_input.setGeometry(QtCore.QRect(740, 490, 51, 21))
        self.faktorPlot_input.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.faktorPlot_input.setObjectName("faktorPlot_input")
        self.label_2 = QtGui.QLabel(self.centralWidget)
        self.label_2.setGeometry(QtCore.QRect(800, 490, 181, 21))
        self.label_2.setObjectName("label_2")
        self.s0priori_input = QtGui.QLineEdit(self.centralWidget)
        self.s0priori_input.setGeometry(QtCore.QRect(120, 440, 61, 21))
        self.s0priori_input.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.s0priori_input.setObjectName("s0priori_input")
        self.epochInfo_label_Globaltest_4 = QtGui.QLabel(self.centralWidget)
        self.epochInfo_label_Globaltest_4.setGeometry(QtCore.QRect(10, 440, 81, 20))
        font = QtGui.QFont()
        font.setFamily("Lucida Grande")
        font.setPointSize(11)
        self.epochInfo_label_Globaltest_4.setFont(font)
        self.epochInfo_label_Globaltest_4.setObjectName("epochInfo_label_Globaltest_4")
        self.label_3 = QtGui.QLabel(self.centralWidget)
        self.label_3.setGeometry(QtCore.QRect(540, 10, 231, 21))
        font = QtGui.QFont()
        font.setFamily("Lucida Grande")
        font.setPointSize(17)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.consoleCheck = QtGui.QCheckBox(self.centralWidget)
        self.consoleCheck.setGeometry(QtCore.QRect(840, 550, 141, 20))
        self.consoleCheck.setObjectName("consoleCheck")
        self.epochInfo_label_Globaltest_5 = QtGui.QLabel(self.centralWidget)
        self.epochInfo_label_Globaltest_5.setGeometry(QtCore.QRect(10, 470, 111, 20))
        font = QtGui.QFont()
        font.setFamily("Lucida Grande")
        font.setPointSize(11)
        self.epochInfo_label_Globaltest_5.setFont(font)
        self.epochInfo_label_Globaltest_5.setObjectName("epochInfo_label_Globaltest_5")
        self.epochInfo_label_Globaltest_6 = QtGui.QLabel(self.centralWidget)
        self.epochInfo_label_Globaltest_6.setGeometry(QtCore.QRect(10, 490, 121, 20))
        font = QtGui.QFont()
        font.setFamily("Lucida Grande")
        font.setPointSize(11)
        self.epochInfo_label_Globaltest_6.setFont(font)
        self.epochInfo_label_Globaltest_6.setObjectName("epochInfo_label_Globaltest_6")
        self.winkelGenu_input = QtGui.QLineEdit(self.centralWidget)
        self.winkelGenu_input.setGeometry(QtCore.QRect(120, 470, 61, 21))
        self.winkelGenu_input.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.winkelGenu_input.setObjectName("winkelGenu_input")
        self.streckenGenau_input = QtGui.QLineEdit(self.centralWidget)
        self.streckenGenau_input.setGeometry(QtCore.QRect(120, 490, 61, 21))
        self.streckenGenau_input.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.streckenGenau_input.setObjectName("streckenGenau_input")
        self.epochInfo_label_Globaltest_7 = QtGui.QLabel(self.centralWidget)
        self.epochInfo_label_Globaltest_7.setGeometry(QtCore.QRect(180, 470, 41, 20))
        font = QtGui.QFont()
        font.setFamily("Lucida Grande")
        font.setPointSize(11)
        self.epochInfo_label_Globaltest_7.setFont(font)
        self.epochInfo_label_Globaltest_7.setObjectName("epochInfo_label_Globaltest_7")
        self.epochInfo_label_Globaltest_8 = QtGui.QLabel(self.centralWidget)
        self.epochInfo_label_Globaltest_8.setGeometry(QtCore.QRect(180, 490, 31, 20))
        font = QtGui.QFont()
        font.setFamily("Lucida Grande")
        font.setPointSize(11)
        self.epochInfo_label_Globaltest_8.setFont(font)
        self.epochInfo_label_Globaltest_8.setObjectName("epochInfo_label_Globaltest_8")
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtGui.QMenuBar()
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 985, 22))
        self.menuBar.setObjectName("menuBar")
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QtGui.QToolBar(MainWindow)
        self.mainToolBar.setObjectName("mainToolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtGui.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)
        self.actionAsdfa = QtGui.QAction(MainWindow)
        self.actionAsdfa.setObjectName("actionAsdfa")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(
            QtGui.QApplication.translate("MainWindow", "defomaMainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.newEpoch_btn.setText(
            QtGui.QApplication.translate("MainWindow", "neue Epoche", None, QtGui.QApplication.UnicodeUTF8))
        self.init_btn.setText(
            QtGui.QApplication.translate("MainWindow", "Initialmessung", None, QtGui.QApplication.UnicodeUTF8))
        self.doDefoAn.setText(
            QtGui.QApplication.translate("MainWindow", "Analyse starten", None, QtGui.QApplication.UnicodeUTF8))
        self.add_btn.setText(QtGui.QApplication.translate("MainWindow", ">", None, QtGui.QApplication.UnicodeUTF8))
        self.remove_btn.setText(QtGui.QApplication.translate("MainWindow", "<", None, QtGui.QApplication.UnicodeUTF8))
        self.epochTabelView.horizontalHeaderItem(0).setText(
            QtGui.QApplication.translate("MainWindow", "Epoche", None, QtGui.QApplication.UnicodeUTF8))
        self.epochTabelView.horizontalHeaderItem(1).setText(
            QtGui.QApplication.translate("MainWindow", "Ausgleichung  erfolgreich", None,
                                         QtGui.QApplication.UnicodeUTF8))
        self.DefoInfoTabel.horizontalHeaderItem(0).setText(
            QtGui.QApplication.translate("MainWindow", "Punkt Nr. ", None, QtGui.QApplication.UnicodeUTF8))
        self.DefoInfoTabel.horizontalHeaderItem(1).setText(
            QtGui.QApplication.translate("MainWindow", "delta Y", None, QtGui.QApplication.UnicodeUTF8))
        self.DefoInfoTabel.horizontalHeaderItem(2).setText(
            QtGui.QApplication.translate("MainWindow", "delta X", None, QtGui.QApplication.UnicodeUTF8))
        self.DefoInfoTabel.horizontalHeaderItem(3).setText(
            QtGui.QApplication.translate("MainWindow", "delta Z", None, QtGui.QApplication.UnicodeUTF8))
        self.DefoInfoTabel.horizontalHeaderItem(4).setText(
            QtGui.QApplication.translate("MainWindow", "delta s", None, QtGui.QApplication.UnicodeUTF8))
        self.DefoInfoTabel.horizontalHeaderItem(5).setText(
            QtGui.QApplication.translate("MainWindow", "Testgröße", None, QtGui.QApplication.UnicodeUTF8))
        self.DefoInfoTabel.horizontalHeaderItem(6).setText(
            QtGui.QApplication.translate("MainWindow", "Quantil F", None, QtGui.QApplication.UnicodeUTF8))
        self.DefoInfoTabel.horizontalHeaderItem(7).setText(
            QtGui.QApplication.translate("MainWindow", "Deformation", None, QtGui.QApplication.UnicodeUTF8))
        self.EpochInfoGroupBox.setTitle(
            QtGui.QApplication.translate("MainWindow", "Epchen Informationen", None, QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_StationCount.setText(
            QtGui.QApplication.translate("MainWindow", "Anzahl Stationen (Zielpunkte): ", None,
                                         QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_AdjustmentSuccess.setText(
            QtGui.QApplication.translate("MainWindow", "Ausgleichung erfolgreich:", None,
                                         QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_UnbekanntenCount.setText(
            QtGui.QApplication.translate("MainWindow", "Anzahl Unbekannte:", None, QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_Datumsdefkt.setText(
            QtGui.QApplication.translate("MainWindow", "Datumsdefek:", None, QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_BeobachtungCount.setText(
            QtGui.QApplication.translate("MainWindow", "Anzahl Beobachtungen:", None, QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_Globaltest.setText(
            QtGui.QApplication.translate("MainWindow", "Globaltest: ", None, QtGui.QApplication.UnicodeUTF8))
        self.label_7.setText(
            QtGui.QApplication.translate("MainWindow", "Ausgleichungsart: Freie 3D-Netzausgleichung", None,
                                         QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_s0Pri.setText(
            QtGui.QApplication.translate("MainWindow", "s0 a priori:", None, QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_S0post.setText(
            QtGui.QApplication.translate("MainWindow", "s0 a posteriori:", None, QtGui.QApplication.UnicodeUTF8))
        self.openGraphic3D_btn.setText(
            QtGui.QApplication.translate("MainWindow", "Plot öffnen", None, QtGui.QApplication.UnicodeUTF8))
        self.openDefoProtokoll_btn.setText(
            QtGui.QApplication.translate("MainWindow", "Protokoll öffnen", None, QtGui.QApplication.UnicodeUTF8))
        self.openEpochProtokollBtn.setText(
            QtGui.QApplication.translate("MainWindow", "Protokoll öffnen", None, QtGui.QApplication.UnicodeUTF8))
        self.doAusgleichung_btn.setText(
            QtGui.QApplication.translate("MainWindow", "Epoche ausgleichen", None, QtGui.QApplication.UnicodeUTF8))
        self.showNetPlot_btn.setText(
            QtGui.QApplication.translate("MainWindow", "Plot öffnen", None, QtGui.QApplication.UnicodeUTF8))
        self.irtrums_input.setText(
            QtGui.QApplication.translate("MainWindow", "5", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("MainWindow", "Irrtumswahrscheinlichkeit [%]", None,
                                                        QtGui.QApplication.UnicodeUTF8))
        self.deleEpoch_btn.setText(
            QtGui.QApplication.translate("MainWindow", "Epoche löschen", None, QtGui.QApplication.UnicodeUTF8))
        self.faktorPlot_input.setText(
            QtGui.QApplication.translate("MainWindow", "10", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("MainWindow", "Überzeichnungsfaktor (Plot)", None,
                                                          QtGui.QApplication.UnicodeUTF8))
        self.s0priori_input.setText(
            QtGui.QApplication.translate("MainWindow", "1", None, QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_Globaltest_4.setText(
            QtGui.QApplication.translate("MainWindow", "s0 a priori:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(
            QtGui.QApplication.translate("MainWindow", "Deformationsanalyse", None, QtGui.QApplication.UnicodeUTF8))
        self.consoleCheck.setText(
            QtGui.QApplication.translate("MainWindow", "Konsole", None, QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_Globaltest_5.setText(
            QtGui.QApplication.translate("MainWindow", "Winkelgenauigkeit", None, QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_Globaltest_6.setText(
            QtGui.QApplication.translate("MainWindow", "Streckengenauigkeit", None, QtGui.QApplication.UnicodeUTF8))
        self.winkelGenu_input.setText(
            QtGui.QApplication.translate("MainWindow", "0.008", None, QtGui.QApplication.UnicodeUTF8))
        self.streckenGenau_input.setText(
            QtGui.QApplication.translate("MainWindow", "0.001", None, QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_Globaltest_7.setText(
            QtGui.QApplication.translate("MainWindow", " gon", None, QtGui.QApplication.UnicodeUTF8))
        self.epochInfo_label_Globaltest_8.setText(
            QtGui.QApplication.translate("MainWindow", " m", None, QtGui.QApplication.UnicodeUTF8))
        self.actionAsdfa.setText(
            QtGui.QApplication.translate("MainWindow", "asdfa", None, QtGui.QApplication.UnicodeUTF8))
