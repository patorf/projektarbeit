# coding=utf-8
__author__ = 'philippatorf'

from PySide import QtCore, QtGui
import sys
from PySide import QtCore, QtGui

from PySide import QtCore, QtGui


class Ui_measureWindow(object):
    def setupUi(self, measureWindow):
        measureWindow.setObjectName("measureWindow")
        measureWindow.resize(941, 593)
        self.groupBox = QtGui.QGroupBox(measureWindow)
        self.groupBox.setGeometry(QtCore.QRect(20, 10, 421, 501))
        self.groupBox.setObjectName("groupBox")
        self.portSelect_cmb_1 = QtGui.QComboBox(self.groupBox)
        self.portSelect_cmb_1.setGeometry(QtCore.QRect(100, 30, 141, 26))
        self.portSelect_cmb_1.setObjectName("portSelect_cmb_1")
        self.label = QtGui.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(20, 30, 61, 31))
        self.label.setObjectName("label")
        self.label_2 = QtGui.QLabel(self.groupBox)
        self.label_2.setGeometry(QtCore.QRect(20, 60, 62, 16))
        self.label_2.setObjectName("label_2")
        self.status_lb_1 = QtGui.QLabel(self.groupBox)
        self.status_lb_1.setGeometry(QtCore.QRect(100, 60, 151, 21))
        self.status_lb_1.setObjectName("status_lb_1")
        self.pktNo_input_1 = QtGui.QTextEdit(self.groupBox)
        self.pktNo_input_1.setGeometry(QtCore.QRect(120, 120, 71, 31))
        self.pktNo_input_1.setObjectName("pktNo_input_1")
        self.measure_btn_1 = QtGui.QPushButton(self.groupBox)
        self.measure_btn_1.setGeometry(QtCore.QRect(10, 240, 114, 32))
        self.measure_btn_1.setAutoRepeatDelay(306)
        self.measure_btn_1.setAutoDefault(False)
        self.measure_btn_1.setObjectName("measure_btn_1")
        self.label_4 = QtGui.QLabel(self.groupBox)
        self.label_4.setGeometry(QtCore.QRect(20, 130, 62, 16))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtGui.QLabel(self.groupBox)
        self.label_5.setGeometry(QtCore.QRect(20, 165, 62, 21))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtGui.QLabel(self.groupBox)
        self.label_6.setGeometry(QtCore.QRect(20, 200, 81, 21))
        self.label_6.setObjectName("label_6")
        self.pointType_cmb_1 = QtGui.QComboBox(self.groupBox)
        self.pointType_cmb_1.setGeometry(QtCore.QRect(120, 160, 191, 26))
        self.pointType_cmb_1.setObjectName("pointType_cmb_1")
        self.prism_cmb_1 = QtGui.QComboBox(self.groupBox)
        self.prism_cmb_1.setGeometry(QtCore.QRect(120, 200, 191, 26))
        self.prism_cmb_1.setObjectName("prism_cmb_1")
        self.connect_btn_1 = QtGui.QPushButton(self.groupBox)
        self.connect_btn_1.setGeometry(QtCore.QRect(240, 30, 91, 32))
        self.connect_btn_1.setObjectName("connect_btn_1")
        self.pointTable1 = QtGui.QTableWidget(self.groupBox)
        self.pointTable1.setGeometry(QtCore.QRect(10, 270, 401, 191))
        font = QtGui.QFont()
        font.setFamily("Lucida Sans Unicode")
        self.pointTable1.setFont(font)
        self.pointTable1.setObjectName("pointTable1")
        self.pointTable1.setColumnCount(4)
        self.pointTable1.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.pointTable1.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.pointTable1.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.pointTable1.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.pointTable1.setHorizontalHeaderItem(3, item)
        self.pointTable1.horizontalHeader().setDefaultSectionSize(93)
        self.pointTable1.horizontalHeader().setStretchLastSection(True)
        self.completeSet_btn_1 = QtGui.QPushButton(self.groupBox)
        self.completeSet_btn_1.setGeometry(QtCore.QRect(240, 240, 171, 32))
        self.completeSet_btn_1.setObjectName("completeSet_btn_1")
        self.progressBar_1 = QtGui.QProgressBar(self.groupBox)
        self.progressBar_1.setEnabled(True)
        self.progressBar_1.setGeometry(QtCore.QRect(10, 470, 401, 23))
        self.progressBar_1.setProperty("value", 0)
        self.progressBar_1.setOrientation(QtCore.Qt.Horizontal)
        self.progressBar_1.setInvertedAppearance(False)
        self.progressBar_1.setObjectName("progressBar_1")
        self.identifyTachy_1 = QtGui.QPushButton(self.groupBox)
        self.identifyTachy_1.setGeometry(QtCore.QRect(10, 80, 114, 32))
        self.identifyTachy_1.setObjectName("identifyTachy_1")
        self.groupBox_2 = QtGui.QGroupBox(measureWindow)
        self.groupBox_2.setGeometry(QtCore.QRect(470, 10, 421, 501))
        self.groupBox_2.setObjectName("groupBox_2")
        self.portSelect_cmb_2 = QtGui.QComboBox(self.groupBox_2)
        self.portSelect_cmb_2.setGeometry(QtCore.QRect(110, 30, 141, 26))
        self.portSelect_cmb_2.setObjectName("portSelect_cmb_2")
        self.label_7 = QtGui.QLabel(self.groupBox_2)
        self.label_7.setGeometry(QtCore.QRect(30, 30, 61, 31))
        self.label_7.setObjectName("label_7")
        self.label_8 = QtGui.QLabel(self.groupBox_2)
        self.label_8.setGeometry(QtCore.QRect(30, 60, 62, 16))
        self.label_8.setObjectName("label_8")
        self.status_lb_2 = QtGui.QLabel(self.groupBox_2)
        self.status_lb_2.setGeometry(QtCore.QRect(110, 60, 151, 21))
        self.status_lb_2.setObjectName("status_lb_2")
        self.pktNo_input_2 = QtGui.QTextEdit(self.groupBox_2)
        self.pktNo_input_2.setGeometry(QtCore.QRect(130, 120, 71, 31))
        self.pktNo_input_2.setObjectName("pktNo_input_2")
        self.measure_btn_2 = QtGui.QPushButton(self.groupBox_2)
        self.measure_btn_2.setGeometry(QtCore.QRect(10, 240, 114, 32))
        self.measure_btn_2.setObjectName("measure_btn_2")
        self.label_10 = QtGui.QLabel(self.groupBox_2)
        self.label_10.setGeometry(QtCore.QRect(30, 130, 62, 16))
        self.label_10.setObjectName("label_10")
        self.label_11 = QtGui.QLabel(self.groupBox_2)
        self.label_11.setGeometry(QtCore.QRect(30, 160, 62, 21))
        self.label_11.setObjectName("label_11")
        self.label_12 = QtGui.QLabel(self.groupBox_2)
        self.label_12.setGeometry(QtCore.QRect(30, 200, 81, 21))
        self.label_12.setObjectName("label_12")
        self.pointType_cmb_2 = QtGui.QComboBox(self.groupBox_2)
        self.pointType_cmb_2.setGeometry(QtCore.QRect(130, 160, 191, 26))
        self.pointType_cmb_2.setObjectName("pointType_cmb_2")
        self.prism_cmb_2 = QtGui.QComboBox(self.groupBox_2)
        self.prism_cmb_2.setGeometry(QtCore.QRect(130, 200, 191, 26))
        self.prism_cmb_2.setObjectName("prism_cmb_2")
        self.connect_btn_2 = QtGui.QPushButton(self.groupBox_2)
        self.connect_btn_2.setGeometry(QtCore.QRect(250, 30, 91, 32))
        self.connect_btn_2.setObjectName("connect_btn_2")
        self.completeSet_btn_2 = QtGui.QPushButton(self.groupBox_2)
        self.completeSet_btn_2.setGeometry(QtCore.QRect(240, 240, 171, 32))
        self.completeSet_btn_2.setObjectName("completeSet_btn_2")
        self.progressBar_2 = QtGui.QProgressBar(self.groupBox_2)
        self.progressBar_2.setEnabled(True)
        self.progressBar_2.setGeometry(QtCore.QRect(10, 470, 401, 23))
        self.progressBar_2.setProperty("value", 0)
        self.progressBar_2.setOrientation(QtCore.Qt.Horizontal)
        self.progressBar_2.setInvertedAppearance(False)
        self.progressBar_2.setObjectName("progressBar_2")
        self.identifyTachy_2 = QtGui.QPushButton(self.groupBox_2)
        self.identifyTachy_2.setGeometry(QtCore.QRect(20, 80, 114, 32))
        self.identifyTachy_2.setObjectName("identifyTachy_2")
        self.pointTable2 = QtGui.QTableWidget(self.groupBox_2)
        self.pointTable2.setGeometry(QtCore.QRect(10, 270, 401, 191))
        font = QtGui.QFont()
        font.setFamily("Lucida Sans Unicode")
        self.pointTable2.setFont(font)
        self.pointTable2.setObjectName("pointTable2")
        self.pointTable2.setColumnCount(4)
        self.pointTable2.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.pointTable2.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.pointTable2.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.pointTable2.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.pointTable2.setHorizontalHeaderItem(3, item)
        self.pointTable2.horizontalHeader().setDefaultSectionSize(93)
        self.pointTable2.horizontalHeader().setStretchLastSection(True)
        self.umberOfSets_spinBox = QtGui.QSpinBox(measureWindow)
        self.umberOfSets_spinBox.setGeometry(QtCore.QRect(30, 530, 49, 24))
        self.umberOfSets_spinBox.setMinimum(1)
        self.umberOfSets_spinBox.setMaximum(4)
        self.umberOfSets_spinBox.setObjectName("umberOfSets_spinBox")
        self.measureAdditionsSet = QtGui.QPushButton(measureWindow)
        self.measureAdditionsSet.setGeometry(QtCore.QRect(80, 530, 181, 32))
        self.measureAdditionsSet.setObjectName("measureAdditionsSet")

        self.retranslateUi(measureWindow)
        QtCore.QMetaObject.connectSlotsByName(measureWindow)

    def retranslateUi(self, measureWindow):
        measureWindow.setWindowTitle(
            QtGui.QApplication.translate("measureWindow", "Initialmessung", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(
            QtGui.QApplication.translate("measureWindow", "Standpunkt 1", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("measureWindow", "Port:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(
            QtGui.QApplication.translate("measureWindow", "Status:", None, QtGui.QApplication.UnicodeUTF8))
        self.status_lb_1.setText(
            QtGui.QApplication.translate("measureWindow", "nicht Verbunden", None, QtGui.QApplication.UnicodeUTF8))
        self.measure_btn_1.setText(
            QtGui.QApplication.translate("measureWindow", "Punkt messen", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(
            QtGui.QApplication.translate("measureWindow", "Pkt. Nr.", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("measureWindow", "Typ", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(
            QtGui.QApplication.translate("measureWindow", "Prismatyp", None, QtGui.QApplication.UnicodeUTF8))
        self.connect_btn_1.setText(
            QtGui.QApplication.translate("measureWindow", "verbinden", None, QtGui.QApplication.UnicodeUTF8))
        self.pointTable1.setSortingEnabled(False)
        self.pointTable1.horizontalHeaderItem(0).setText(
            QtGui.QApplication.translate("measureWindow", "Nr", None, QtGui.QApplication.UnicodeUTF8))
        self.pointTable1.horizontalHeaderItem(1).setText(
            QtGui.QApplication.translate("measureWindow", "Typ", None, QtGui.QApplication.UnicodeUTF8))
        self.pointTable1.horizontalHeaderItem(2).setText(
            QtGui.QApplication.translate("measureWindow", "Prismatyp", None, QtGui.QApplication.UnicodeUTF8))
        self.pointTable1.horizontalHeaderItem(3).setText(
            QtGui.QApplication.translate("measureWindow", "Anzahl Messungen", None, QtGui.QApplication.UnicodeUTF8))
        self.completeSet_btn_1.setText(
            QtGui.QApplication.translate("measureWindow", "Satz abschließen", None, QtGui.QApplication.UnicodeUTF8))
        self.identifyTachy_1.setText(
            QtGui.QApplication.translate("measureWindow", "identifizieren", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(
            QtGui.QApplication.translate("measureWindow", "Standpunkt 2", None, QtGui.QApplication.UnicodeUTF8))
        self.label_7.setText(
            QtGui.QApplication.translate("measureWindow", "Port:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_8.setText(
            QtGui.QApplication.translate("measureWindow", "Status:", None, QtGui.QApplication.UnicodeUTF8))
        self.status_lb_2.setText(
            QtGui.QApplication.translate("measureWindow", "nicht Verbunden", None, QtGui.QApplication.UnicodeUTF8))
        self.measure_btn_2.setText(
            QtGui.QApplication.translate("measureWindow", "Punkt messen", None, QtGui.QApplication.UnicodeUTF8))
        self.label_10.setText(
            QtGui.QApplication.translate("measureWindow", "Pkt. Nr.", None, QtGui.QApplication.UnicodeUTF8))
        self.label_11.setText(
            QtGui.QApplication.translate("measureWindow", "Typ", None, QtGui.QApplication.UnicodeUTF8))
        self.label_12.setText(
            QtGui.QApplication.translate("measureWindow", "Prismatyp", None, QtGui.QApplication.UnicodeUTF8))
        self.connect_btn_2.setText(
            QtGui.QApplication.translate("measureWindow", "verbinden", None, QtGui.QApplication.UnicodeUTF8))
        self.completeSet_btn_2.setText(
            QtGui.QApplication.translate("measureWindow", "Satz abschließen", None, QtGui.QApplication.UnicodeUTF8))
        self.identifyTachy_2.setText(
            QtGui.QApplication.translate("measureWindow", "identifizieren", None, QtGui.QApplication.UnicodeUTF8))
        self.pointTable2.setSortingEnabled(False)
        self.pointTable2.horizontalHeaderItem(0).setText(
            QtGui.QApplication.translate("measureWindow", "Nr", None, QtGui.QApplication.UnicodeUTF8))
        self.pointTable2.horizontalHeaderItem(1).setText(
            QtGui.QApplication.translate("measureWindow", "Typ", None, QtGui.QApplication.UnicodeUTF8))
        self.pointTable2.horizontalHeaderItem(2).setText(
            QtGui.QApplication.translate("measureWindow", "Prismatyp", None, QtGui.QApplication.UnicodeUTF8))
        self.pointTable2.horizontalHeaderItem(3).setText(
            QtGui.QApplication.translate("measureWindow", "Anzahl Messungen", None, QtGui.QApplication.UnicodeUTF8))
        self.measureAdditionsSet.setText(
            QtGui.QApplication.translate("measureWindow", "weitere Sätze messen", None, QtGui.QApplication.UnicodeUTF8))

