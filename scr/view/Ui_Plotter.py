from PySide import QtGui, QtCore


class Ui_Plotter(object):
    def setupUi(self, Ui_Plotter):
        Ui_Plotter.setObjectName("Ui_Plotter")
        Ui_Plotter.resize(631, 631)
        self.centralWidget = QtGui.QWidget(Ui_Plotter)
        self.centralWidget.setObjectName("centralWidget")
        self.plotWidget = QtGui.QWidget(self.centralWidget)
        self.plotWidget.setEnabled(True)
        self.plotWidget.setGeometry(QtCore.QRect(100, 70, 400, 400))
        self.plotWidget.setObjectName("plotWidget")
        Ui_Plotter.setCentralWidget(self.centralWidget)
        self.menuBar = QtGui.QMenuBar()
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 631, 22))
        self.menuBar.setObjectName("menuBar")
        Ui_Plotter.setMenuBar(self.menuBar)
        self.mainToolBar = QtGui.QToolBar(Ui_Plotter)
        self.mainToolBar.setObjectName("mainToolBar")
        Ui_Plotter.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtGui.QStatusBar(Ui_Plotter)
        self.statusBar.setObjectName("statusBar")
        Ui_Plotter.setStatusBar(self.statusBar)

        self.retranslateUi(Ui_Plotter)
        QtCore.QMetaObject.connectSlotsByName(Ui_Plotter)

    def retranslateUi(self, Ui_Plotter):
        Ui_Plotter.setWindowTitle(
            QtGui.QApplication.translate("Ui_Plotter", "Ui_Plotter", None, QtGui.QApplication.UnicodeUTF8))



        #ax= Plotter().create3dPlott()
        #Plotter().add_Ellipse(ax)
        #plt.show()
        #Plotter().plottEllipse()