# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'AddEpochDialog.ui'
#
# Created: Tue Jan 28 10:22:12 2014
#      by: pyside-uic 0.2.13 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui


class Ui_addEpochDialog(object):
    def setupUi(self, addEpochDialog):
        addEpochDialog.setObjectName("addEpochDialog")
        addEpochDialog.resize(296, 195)
        self.layoutWidget = QtGui.QWidget(addEpochDialog)
        self.layoutWidget.setGeometry(QtCore.QRect(20, 120, 252, 32))
        self.layoutWidget.setObjectName("layoutWidget")
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.cancel_Btn = QtGui.QPushButton(self.layoutWidget)
        self.cancel_Btn.setObjectName("cancel_Btn")
        self.horizontalLayout_2.addWidget(self.cancel_Btn)
        self.measure_btn = QtGui.QPushButton(self.layoutWidget)
        self.measure_btn.setObjectName("measure_btn")
        self.horizontalLayout_2.addWidget(self.measure_btn)
        self.layoutWidget1 = QtGui.QWidget(addEpochDialog)
        self.layoutWidget1.setGeometry(QtCore.QRect(30, 40, 181, 54))
        self.layoutWidget1.setObjectName("layoutWidget1")
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget1)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtGui.QLabel(self.layoutWidget1)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.SetCount_spinBox = QtGui.QSpinBox(self.layoutWidget1)
        self.SetCount_spinBox.setObjectName("SetCount_spinBox")
        self.horizontalLayout.addWidget(self.SetCount_spinBox)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.progressBar = QtGui.QProgressBar(addEpochDialog)
        self.progressBar.setGeometry(QtCore.QRect(20, 160, 261, 23))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName("progressBar")

        self.retranslateUi(addEpochDialog)
        QtCore.QMetaObject.connectSlotsByName(addEpochDialog)

    def retranslateUi(self, addEpochDialog):
        addEpochDialog.setWindowTitle(
            QtGui.QApplication.translate("addEpochDialog", "addEpochDialog", None, QtGui.QApplication.UnicodeUTF8))
        self.cancel_Btn.setText(
            QtGui.QApplication.translate("addEpochDialog", "Abbrechen", None, QtGui.QApplication.UnicodeUTF8))
        self.measure_btn.setText(
            QtGui.QApplication.translate("addEpochDialog", "Epoche messen", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(
            QtGui.QApplication.translate("addEpochDialog", "Anzahl der Sätze:", None, QtGui.QApplication.UnicodeUTF8))